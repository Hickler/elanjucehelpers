#pragma once

#include "JuceHeader.h"

class Translator
{
public:
    Translator(juce::String& s);
    Translator() {}

    std::map<String, juce::String> dict;
    juce::String def;

private:
    // members
};
