#include "TypeConversion.h"

namespace Convert {
vector<String> Convert::toVector(const StringArray& x) {
    vector<String> ret;
    for (const auto& obj : x)
        ret.push_back(obj);

    return std::move(ret);
}

vector<String> toVector(const String& x, const juce_wchar& c) {
    StringIterator iter(x);
    vector<String> tokens;

    while (!iter.atEnd()) {
        tokens.push_back(iter.consumeToChar(c));
        iter.move();
    }

    return std::move(tokens);
}

vector<String> fromLines(const String& x) {
    StringIterator iter(x);
    vector<String> tokens;

    while (!iter.atEnd()) {
        tokens.push_back(iter.consumeToNewLine());
        iter.move();
    }

    return std::move(tokens);
}

void appendFromLines(vector<String>& v, const String& x) {
    VectorHelper::append<String>(v, fromLines(x));
}

Array<var> Convert::toVar(const Array<String>& x) {
    Array<var> ret;
    for (const auto& obj : x)
        ret.add(obj);

    return std::move(ret);
}

Array<var> Convert::toVar(const Array<File>& x) {
    Array<var> ret;
    for (const auto& obj : x)
        ret.add(obj.getFullPathName());

    return ret;
}

Array<var> Convert::toVar(const vector<String>& x) {
    Array<var> ret;
    for (const auto& obj : x)
        ret.add(obj);

    return ret;
}

Array<var> Convert::toVar(const vector<double>& x) {
    Array<var> ret;
    for (const auto& obj : x)
        ret.add(obj);

    return ret;
}

StringArray Convert::toStringArray(const vector<String>& x) {
    StringArray ret;

    for (const auto& obj : x)
        ret.add(obj);

    return ret;
}

StringArray Convert::toStringArray(const var& x) {
    StringArray ret;

    if (!x.isArray())
        ret.add(x.toString());
    else
        for (const auto& obj : *x.getArray())
            ret.add(obj);

    return ret;
}

String Convert::toString(const vector<String>& x, juce::StringRef separator, int start, int numberToJoin) {
    if (x.empty())
        return "";

    auto last = (numberToJoin < 0) ? x.size() : std::min<int>(int(x.size()), start + numberToJoin);

    if (start < 0)
        start = 0;

    if (start >= last)
        return {};

    if (start == last - 1)
        return x[start];

    auto separatorBytes = separator.text.sizeInBytes() - sizeof(String::CharPointerType::CharType);
    auto bytesNeeded    = (size_t)(last - start - 1) * separatorBytes;

    for (int i = start; i < last; ++i)
        bytesNeeded += x[i].getCharPointer().sizeInBytes() - sizeof(String::CharPointerType::CharType);

    String result;
    result.preallocateBytes(bytesNeeded);

    auto dest = result.getCharPointer();

    while (start < last) {
        auto& s = x[start];

        if (!s.isEmpty())
            dest.writeAll(s.getCharPointer());

        if (++start < last && separatorBytes > 0)
            dest.writeAll(separator.text);
    }

    dest.writeNull();
    return std::move(result);
}

String Convert::toString(const vector<juce_wchar>& x) {
    String result;
    result.preallocateBytes(x.size() - sizeof(String::CharPointerType::CharType));

    auto dest = result.getCharPointer();
    std::for_each(x.begin(), x.end(), [&](const juce_wchar& c) { result += c; });
    dest.writeNull();

    return std::move(result);
}

String Convert::toString(const Array<int>& x, juce::StringRef separator) {
    StringArray result;
    result.ensureStorageAllocated(x.size());

    for (auto n : x)
        result.add(String(n));

    return std::move(result.joinIntoString(separator));
}

String toString(const StringArray& x, juce::StringRef separator, int start, int numberToJoin) {
    return x.joinIntoString(separator, start, numberToJoin);
}

}; // namespace Convert

std::default_random_engine VectorHelper::getRandomEngine() {
    return std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());
}

void DEBUG_PRINT(const ValueTree& x) {
    DBG(x.toXmlString());
}

void DEBUG_PRINT(const vector<String>& x) {
    DBG(Convert::toString(x));
}

void DEBUG_PRINT(const var& x) {
    DBG(JSON::toString(x));
}

void DEBUG_PRINT(const Array<var>* x) {
    DBG(JSON::toString(*x));
}

void DEBUG_PRINT(DynamicObject* x) {
    DBG(JSON::toString(var(x)));
}
