#pragma once

#include "3rdParty/Helmholtz/Helmholtz.h"

#include <algorithm>
#include <vector>

typedef double FREQ;
typedef double DATA;
typedef double RATIO;
typedef double TIME;
typedef int POW2;
typedef int BLOCK;
struct int_double { int u; double f; };

class HelmholtzPitchDetector
{
public:
	HelmholtzPitchDetector(DATA* RawWav, int nsamples, int samplerate);
	~HelmholtzPitchDetector();

	struct samp_freq { std::vector<int> s; std::vector<double> f; };
	struct time_amp { double t; double a; };
	struct samp_amp { size_t s; double a; };

	//mode == 1 to prevent harmonic jumps
	FREQ GetAvgFreq();
	samp_freq GetFreqData();
	std::vector<double> getFrequencies();

	samp_freq FreqData;
	std::vector<double> frequencies;
	int samplerate = 0;
	double pitchRange = 2; // in semitones, how far above or below measured frequenc
	FREQ basefreq = 0; // set by getting average frequency, happens automatically if no user or base freq
	FREQ userfreq = 0; // if userfreq is not set, basefreq is used.
	double fidelity = 0.95; // normalized value where 0 is 'use least reliable frequency measurements'
	POW2 framesize = 0;
	POW2 overlap = 1;
	int nsamples = 0;
	bool prevent_harmonic_jump = 1;
	std::vector<double> normalizedAudio;

protected:
	//stuff for helmholtz
	double* hhout; // can we move this? 
	Helmholtz* hel;

	double LinearInterpolate(double a, double b, size_t size, size_t index);
	void processBlock(DATA* dataBlock, int blocksize, int samp_index_for_freq);
	void process();
	void solve_framesize();
	int appear_most();
};
