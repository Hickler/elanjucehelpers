#pragma once

#include "JuceHeader.h"

juce::String toS(const juce::juce_wchar& c); // convert char to string
juce::String toS(int i);                     // convert int to string

struct toN {
    juce::String s;
    toN(juce::String const& s)
      : s(s) {}
    operator double() {
        return s.getDoubleValue();
    }
    operator int() {
        return s.getIntValue();
    }
    operator juce::String() {
        return s;
    }
};

namespace STR {
// check if string is a correctly-formatted integer
bool isInt(const juce::String& s);
// check if string is a corrrectly-formatted integer or floating point number
bool isFloat(const juce::String& s);
// check if string is a correctly-formatted int or floating point number, returns 1 if int, 2 if float, 0 if netiher. Decimal position is -1 if dot not found.
int isIntOrFloat(const juce::String& s, int* dotPosition = nullptr, int* signPosition = nullptr);
/* If string is a floating point number (must have dot), will return new string with limited decimal places otherwise returns original string

if decimalPlaces == 0
1.0 => 1
0.1 => 0
1.  => 1
.0  => 0
1.5 => 2
0.5 => 1

if decimalPlaces >= 1
1.0 => 1.0
0.1 => 0.1
1.  => 1.0
.0  => 0.0
1.5 => 1.5
0.5 => 0.5
*/
juce::String limitDecimals(const juce::String& s, int decimalPlaces);
juce::String limitDecimals(double v, int decimalPlaces);
// delimiters are single characters long, multiple chars can be used as delimiters, delimiters are discarded
std::vector<juce::String> splitToVector(const juce::String& s, const juce::String& delims);
// delimiters are single characters long, multiple chars can be used as delimiters, delimiters are discarded
juce::Array<juce::var> splitToVarArray(const juce::String& s, const juce::String& delims);

std::vector<juce::String> splitPast(const juce::String& s, const juce::String& delim); // split by any delimiter but ignore delimiters encountered subsequently
std::vector<juce::String> split(const juce::String& s, const juce::String& delim, size_t min_size, const juce::String& defval);

juce::String repeat(const juce::String& s, int numRepeats);

juce::String getRandomAlphaNumericString(int numChars);

static const std::vector<juce::juce_wchar> alphanumeric = {
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E',
    'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
};
} // namespace STR

namespace CHAr {
bool is(const juce::juce_wchar& c1, const juce::juce_wchar& c2);   // case-sensitive compare two characters
bool is_i(const juce::juce_wchar& c1, const juce::juce_wchar& c2); // case-insensitive compare two characters
bool isWhiteSpace(const juce::juce_wchar& c);                      // check if character is white space
bool isNewLine(const juce::juce_wchar& c);                         // check if character is new line character
bool isDigit(const juce::juce_wchar& c);                           // check if character is digit
bool isAlphabet(const juce::juce_wchar& c);                        // check if character exists in the english alphabet alphabetal character
bool isAny(const juce::juce_wchar& c, const juce::String& list);   // case sensitive check if character is in string
bool isNotAny(const juce::juce_wchar& c, const juce::String& list);   // case sensitive check if character is in string
bool isAny_i(const juce::juce_wchar& c, const juce::String& list); // case-insensitive check if character is in string
bool isNotAny_i(const juce::juce_wchar& c, const juce::String& list);   // case sensitive check if character is in string
bool isUpper(const juce::juce_wchar& c);                           // check if character is upper case
bool isLower(const juce::juce_wchar& c);                           // check if character is lower case
juce::juce_wchar toUpper(const juce::juce_wchar& c);               // convert character to upper case
juce::juce_wchar toLower(const juce::juce_wchar& c);               // convert character to lower case
int getValue(const juce::juce_wchar& c);
} // namespace CHAr

/* Give StringIterator a string to look at. You can use = operator or constructor.

'Consume' means iterate over string and return it based on a stop point.

'Inc' means increment end pointer and return character (does not check for being at end of string)

'Skip' means increment end pointer and do not return character

'Move' is like skip except does not check for being at end of string.

'Back' moves the start pointer backwards

'Get' returns the string that is between p1 and p2 string pointers.

'Get Memory' returns what is saved via Remember function.

'Remember' adds string between p1 and p2 to memory.
*/
class StringIterator {
  public:
    static juce::String convertToWhiteSpace(const juce::String& s);

    static juce::String replaceAll(const juce::String& s, juce::juce_wchar c);

    StringIterator(const juce::String& str)
      : in_str(str)
      , p1(str.getCharPointer())
      , p2(str.getCharPointer()) {
        restart();
    }

    // operator
    juce::juce_wchar operator++() {
        return inc();
    }
    juce::juce_wchar operator[](int i) const {
        return in_str[i];
    }
    // void operator=(const juce::String& s) { in_str = s; restart(); }

    // conversion
    operator const int() const {
        return pos;
    }
    operator const char() const {
        return (char)*p2;
    }
    operator const juce::juce_wchar() const {
        return *p2;
    }
    operator const juce::String() const {
        return in_str;
    }

    // comparison
    bool operator!=(char c) const {
        return *p2 != (unsigned)c;
    }
    bool operator==(char c) const {
        return *p2 == (unsigned)c;
    }
    bool operator<(char c) const {
        return *p2 < (unsigned)c;
    }
    bool operator>(char c) const {
        return *p2 > (unsigned)c;
    }

    bool operator!=(int i) const {
        return pos != i;
    }
    bool operator==(int i) const {
        return pos == i;
    }
    bool operator<(int i) const {
        return pos < i;
    }
    bool operator>(int i) const {
        return pos > i;
    }

    bool operator!=(juce::juce_wchar c) const {
        return *p2 != c;
    }
    bool operator==(juce::juce_wchar c) const {
        return *p2 == c;
    }
    bool operator<(juce::juce_wchar c) const {
        return *p2 < (juce::juce_wchar)c;
    }
    bool operator>(juce::juce_wchar c) const {
        return *p2 > (juce::juce_wchar)c;
    }

    juce::CharPointer_UTF8 p2;
    juce::CharPointer_UTF8 p1;

    // Add a substring to memory
    void remember();

    // Add a substring to memory, skip next char, and have p1 = p2 which essentially removes previous characters from string. This is used to efficiently remove a single character (the next character)
    // from the substring, especially useful for when trying to find and remove escape characters
    void rememberAndSkipOver();

    // Return substring from p1 to p2
    juce::String getSubstring() {
        return String(p1, p2);
    }

    // Return all remembered substrings as a single string and clear memory.
    juce::String getMemory();

    // have p1 = p2, used before doing a series of p2 increments
    void start() {
        p1 = p2;
    }
    // Swap p1 and p2, used before doing a series of p2 increments
    void swap() {
        juce::CharPointer_UTF8 temp{ p1 };
        p1 = p2;
        p2 = temp;
    }

    // increment p2 iterator if not at end
    void move() {
        if (!atEnd()) {
            pos += dir;
            p2 += dir;
        }
    }
    void move(int steps) {
        for (int i = 0; i < steps; ++i) {
            if (atEnd()) {
                return;
            }
            move();
        }
    }

    // move() function and all other movement functions will increment by +1
    void setMoveForward() {
        dir = +1;
    }
    // move() function and all other movement functions will increment by -1
    void setMoveReverse() {
        dir = -1;
    }

    void skipToEnd() {
        pos = len;
        p2  = in_str.end();
    }

    // increment p2 iterator and return character before increment
    char inc() {
        ++pos;
        ++p2;
        return (char)*p2;
    }

    char dec() {
        --pos;
        --p2;
        return (char)*p2;
    }

    juce::juce_wchar lookAhead(int amount = 1) {
        if (pos < len - 1 - amount)
            return *(p2 + amount);
        return '\0';
    }

    // get length of string
    int length() {
        return len;
    }

    // check if position is at the last character or later (later could result in undefined behavior)
    bool atLast() const {
        return dir == 1 ? pos >= len - 1 : pos <= 0;
    }

     // true if position is at the first character or ealier (earlier could result in undefined behavior)
    bool atFirst() {
        return dir == 1 ? pos <= 0 : pos >= len - 1;
    }

    // check if iterator is at end
    bool atEnd() const {
        return dir == 1 ? pos >= len : pos <= 0;
    }

    // check if current character is equal to given character, case sensitive
    bool is(const juce::juce_wchar& c) const {
        return *p2 == c;
    }
    // check if current character is not equal to given character, case sensitive
    bool isNot(const juce::juce_wchar& c) const {
        return *p2 != c;
    }

    // check if current character is equal to any of characters in string, case sensitive
    bool isAny(const juce::String& s) const {
        return CHAr::isAny(*p2, s);
    }
    // check if current character is not equal to any of characters in string, case sensitive
    bool isNotAny(const juce::String& s) const {
        return !isAny(s);
    }

    bool isDigit() const {
        return CHAr::isDigit(*p2);
    }

    bool isNonZeroDigit() const {
        return CHAr::isDigit(*p2) && isNot('0');
    }

    // restart iterator back to beginning of string
    void restart();

    // increments until the last new line character
    void skipNewLines();

    // increments until encountering new line character
    void skipToNewLine();

    void skipPastSingleNewLine() {
        while (!atEnd() && isNotAny("\r\n")) {
            move();
        }
        if (is('\r')) {
            move();
        }
        if (is('\n')) {
            move();
        }
    }

    // increment until encountering character
    void skipToChar(const juce::juce_wchar& c);

    // increment until encountering any of characters in string
    void skipToChar(const juce::String& s);

    // increment until encountering char and then increments until char is not encountered
    void skipPastChar(const juce::juce_wchar& c);

    // increments until the last white space character
    void skipToSpace();

    // increments until encountering char or white space
    void skipToSpaceOrChar(const juce::juce_wchar& c);

    // increments until the last non white space character
    void skipSpaces();

    // increment until not encountering char
    void skipChar(const juce::juce_wchar& c);

    // increment until not encountering char
    void skipChar(const juce::String& s);

    // increments until encountering a new line character and returns the substring
    juce::String consumeToNewLine();

    // increments past the next full new line (\r\n or just \n)
    juce::String consumePastNewLine();

    // increments until the last white space character and returns the substring
    juce::String consumePastWhiteSpace();

    // increments until the last white space character and returns the substring
    juce::String consumeToNonSpace();

    // increments until the last digit character and returns the substring
    juce::String consumeInt();

    juce::String consumeToSequenceAndSkip(const std::vector<juce::String>& sequences, int* sequenceFound = nullptr);

    juce::String consumeToSequence(const std::vector<juce::String>& sequences, int* sequenceFound = nullptr);

    juce::String consumePastSequence(const std::vector<juce::String>& sequences, int* sequenceFound = nullptr);

    juce::String consumePastSequence(const juce::String& sequence);

    bool lookAheadForSequence(const juce::String& sequence);

    juce::String prepareForNaturalSort();

    // increments to end and returns the substring
    juce::String consumeToEnd();

    // increments until encountering char and returns the substring
    juce::String consumeToChar(const juce::juce_wchar& c);

    // increments until encountering any char in string and returns the substring
    juce::String consumeToChar(const juce::String& s);

    // increments until encountering any char in string, then increments until char is not encountered, and returns the substring
    juce::String consumePastChar(const juce::String& s);

    // increments until encountering char or white space and returns the substring
    juce::String consumeToSpaceOrChar(const juce::juce_wchar& c);

    // increments until the last new line character and returns the substring without pre and post white space
    juce::String consumeToNewLineTRIM();

    // increments until encounting char and returns the substring without pre and post white space
    juce::String consumeToCharTRIM(const juce::juce_wchar& c);

    // increments until encounting char or newline and returns the substring without pre and post white space
    juce::String consumeToNewlineOrCharTRIM(const juce::juce_wchar& c);

    juce::String consumeToChar_IgnoreEscapes(const juce::String& s, const juce::juce_wchar& escape);

    juce::String getCharsAhead(int length) {
        return in_str.substring(pos, pos + length);
    }

  private:
    // private members

    // used to tell if we are at end of string or not;
    int pos = 0;
    int len = 0;
    int dir = 1;
    const juce::String& in_str;

    struct sub_mem {
        juce::CharPointer_UTF8 a, b;
    };

    std::vector<sub_mem> memory;

    // private functions

    // get substring of from iterators p1 and p2
    juce::String get() {
        return juce::String(p1, p2);
    }
};

/* unit tests

jassert(STR::limitDecimals("1.0", 1) == "1.0");
jassert(STR::limitDecimals("0.1", 1) == "0.1");
jassert(STR::limitDecimals(".1", 1) == "0.1");
jassert(STR::limitDecimals("1.", 1) == "1.0");
jassert(STR::limitDecimals("1.5", 1) == "1.5");
jassert(STR::limitDecimals(".15", 1) == "0.2");
jassert(STR::limitDecimals("0.15", 1) == "0.2");
jassert(STR::limitDecimals("51.", 1) == "51.0");
jassert(STR::limitDecimals(".51", 1) == "0.5");
jassert(STR::limitDecimals("0.51", 1) == "0.5");
jassert(STR::limitDecimals("1.66699999", 4) == "1.667");
jassert(STR::limitDecimals("1.9999", 1) == "2.0");
jassert(STR::limitDecimals("999.9999", 1) == "1000.0");
jassert(STR::limitDecimals("0.9999", 1) == "1.0");
jassert(STR::limitDecimals(".9999", 1) == "1.0");
jassert(STR::limitDecimals("-40.53", 1) == "-40.5");
jassert(STR::limitDecimals("-99.99", 1) == "-100.0");
jassert(STR::limitDecimals("-1.999", 1) == "-2.0");
jassert(STR::limitDecimals("-.5333", 1) == "-0.5");
jassert(STR::limitDecimals("-.5599", 1) == "-0.6");
jassert(STR::limitDecimals("-.9999", 1) == "-1.0");
jassert(STR::limitDecimals("0.9999", 0) == "1");
jassert(STR::limitDecimals("-40.53", 0) == "-41");
jassert(STR::limitDecimals("-99.99", 0) == "-100");
jassert(STR::limitDecimals("-1.999", 0) == "-2");
jassert(STR::limitDecimals("-.5333", 0) == "-1");
jassert(STR::limitDecimals("-.4499", 0) == "0");
jassert(STR::limitDecimals("-.5599", 0) == "-1");
jassert(STR::limitDecimals("-.9999", 0) == "-1");
jassert(STR::limitDecimals("1.0", 0) == "1");
jassert(STR::limitDecimals("0.1", 0) == "0");
jassert(STR::limitDecimals(".1", 0) == "0");
jassert(STR::limitDecimals("1.", 0) == "1");
jassert(STR::limitDecimals("1.5", 0) == "2");
jassert(STR::limitDecimals("0.51", 0) == "1");
jassert(STR::limitDecimals("51.", 0) == "51");
jassert(STR::limitDecimals(".15", 0) == "0");
jassert(STR::limitDecimals("0.15", 0) == "0");
jassert(STR::limitDecimals("1.9999", 0) == "2");
jassert(STR::limitDecimals("0.9999", 0) == "1");
jassert(STR::limitDecimals(".9999", 0) == "1
*/
