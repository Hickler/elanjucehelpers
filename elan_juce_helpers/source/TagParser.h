#pragma once

#include "JuceHeader.h"
#include <vector>
#include <map>

class TagParser {
  public:
    TagParser(const juce::String& in_str);

    struct tag_properties {
        juce::String property_name;
        juce::String prefix;
        juce::String suffix;
        bool use_value;
        bool is_optional;
    };

    std::vector<tag_properties> tags;

    template<class T>
    juce::String buildString(T& object) {
        juce::StringArray s;
        for (auto& t : tags) {
            if (t.property_name.isEmpty()) {
                s.add(t.prefix);
                continue;
            }

            juce::String temp = object.GetPropertyStringFromKey(t.property_name, t.use_value);

            if (temp.isEmpty() && t.is_optional) {
                continue;
            }
            s.add(t.prefix + temp + t.suffix);
        }

        return s.joinIntoString("");
    }

    juce::String buildString(std::function<juce::String(const juce::String&, bool)> getPropertyFunction) {
        juce::StringArray s;
        for (auto& t : tags) {
            if (t.property_name.isEmpty()) {
                s.add(t.prefix);
                continue;
            }

            juce::String temp = getPropertyFunction(t.property_name, t.use_value);

            if (temp.isEmpty() && t.is_optional)
                continue;
            s.add(t.prefix + temp + t.suffix);
        }

        return s.joinIntoString("");
    }

    size_t size() {
        return tags.size();
    }

  private:
    enum Mode {
        Begin,
        Escape,
        InsideBracket,
        InsideExpression,
        Literal,
        SaveResult
    };
    Mode mode = Begin;

    juce::String property_name;
    juce::String prefix;
    juce::String suffix;
    bool use_value   = false;
    bool is_optional = false;
};

/*
Tags
a  =  ~  articulation           legato
i  =  ~  interval and direction up12
n  = #/@ note                   C3 / 36
sn = #/@ starting note          C3 / 36
en = #/@ ending note            C4 / 48
v  = #/@ velocity layer         3  / MF
rr = #/@ repeitition            4  / d
*/

// order that tag string will be constructed.
static std::vector<juce::String> define_order({ "ls", "le", "a", "no", "d", "i", "n", "ns", "ne", "lv", "hv", "vl", "rr" });
// tags not considered for rr number
static std::vector<juce::String> define_uniques({ "ls", "le", "f", "r", "clip", "so", "sn", "v", "pp" });

class Tagger {
  public:
    Tagger() {}
    Tagger(juce::String input);

    std::map<juce::String, juce::String>::iterator begin() {
        return tagmap.begin();
    }
    std::map<juce::String, juce::String>::iterator end() {
        return tagmap.end();
    }
    std::map<juce::String, juce::String>::const_iterator begin() const {
        return tagmap.cbegin();
    }
    std::map<juce::String, juce::String>::const_iterator end() const {
        return tagmap.cend();
    }

    void reset() {
        tagmap.clear();
        name = "";
    }

    // Return string with tags.
    juce::String getString() const;
    // Returns string without tags
    juce::String getNameString() const;
    // Return only tags.
    juce::String getTagString() const;
    // Returns a string based on tag script
    juce::String fromTagScript(const juce::String& tagScript) const;
    // Returns non-unique tags as string
    juce::String getImportantTagString() const;
    // Get tag string or value, returns "" on error
    juce::String getTag(const juce::String& tag, char typechar = '@') const;

    // Returns a string of values based on given tags.
    juce::String getTagSet(std::vector<juce::String> tags) {
        std::vector<juce::String> outStr;
        for (const auto& t : tags)
            outStr.push_back(getTag(t));

        return Convert::toString(outStr);
    }
    juce::String getTagSet(juce::String tagsCommaSeparated) {
        return getTagSet(STR::splitToVector(tagsCommaSeparated, ","));
    }

    // Move tag value to a new tag value. Will overwrite existing tag.
    void renameTag(const juce::String& tag, const juce::String& newName);

    // Set the full string
    void setString(const juce::String& input);
    // Set the non-tag string
    void setNameString(const juce::String& input);
    // Set the full tag string.
    void setTagString(const juce::String& input);

    // Set tag with value using int/double/string
    void setTag(const juce::String& tag, const juce::var& val) {
        tagmap[tag] = val.toString();
    }
    // Remove tag
    void removeTag(const juce::String& tag);
    // Remove a set of tags
    void removeTags(const std::vector<juce::String>& TagList);
    // Remove all tags
    void removeAllTags();

    /* Boolean */
    // Check if string is properly formatted tag script
    bool isTagScript(const juce::String& v);
    // Check if tag exists
    bool hasTag(const juce::String& tag) const;
    bool isTagDefined(const juce::String& tag) const;

    ///* Specific tag functions */

    // juce::String GetNoteTag(bool search_tags = 1, bool use_regex = 1, int search_count = 1);
    ////Mode 0: Search for tags w,f,n,ne,ns, or use regex to find note
    ////Mode 1: Search for w,f tags
    ////Mode 2: Search for n,ne,ns tags
    // double GetFreqTag(int mode = 0, int search_count = 1);
    // void GetLegatoTags(int* start_out = nullptr, int* end_out = nullptr, int* interval_out = nullptr, int* dir_out = nullptr, bool shorthand = true);
    // int TransposeLegatoTags(int i);
    // void SetLegatoTags(int ns, int dir, int ne, bool shorthand = true);

  private:
    juce::String name;
    std::map<juce::String, juce::String> tagmap;
    std::vector<juce::String> order{ define_order };
    std::vector<juce::String> uniques{ define_uniques };
    juce::String KeySort(std::map<juce::String, juce::String> m) const;
    void setup(juce::String input);
};