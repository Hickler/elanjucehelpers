#pragma once

#include "JuceHeader.h"

class PluginFileHelper {
  public:
    PluginFileHelper() = default;

    PluginFileHelper(juce::String vendor, juce::String module, juce::String version, juce::String vendorWebsite = "", juce::String moduleWebsite = "", juce::String downloadLink = "") {
        setupPaths(vendor, module, version, vendorWebsite, moduleWebsite, downloadLink);
    }

    ~PluginFileHelper() = default;

    /*
    Example inputs:
            JucePlugin_Manufacturer,
            JucePlugin_Name,
            JucePlugin_VersionString,
            JucePlugin_ManufacturerWebsite,
            "https://www.website.com/page",
            "http://www.website.com/file.zip"
    */
    void setupPaths(juce::String vendor, juce::String module, juce::String version, juce::String vendorWebsite = "", juce::String moduleWebsite = "", juce::String downloadLink = "");

    // Example: C:\Program Files\Vendor
    juce::File getModuleProgramFolder();

    // Example: C:\Program Files\Vendor
    juce::File getVendorProgramFolder();

    // Example: C:\Users\Name\Documents\Vendor
    juce::File getVendorDocumentsFolder();

    // Example: C:\Users\Name\Documents\Vendor\Module
    juce::File getModuleDocumentsFolder();

    // Example: C:\Users\Name\Documents\Vendor\Module\Presets
    juce::File getPresetsFolder();

    // Example: C:\Users\Name\Documents\Vendor\Module\Presets\Factory
    juce::File getFactoryPresetsFolder();

    // Example: C:\Users\Name\Documents\Vendor\Module\Presets\User
    juce::File getUserPresetsFolder();

    // Example: Module 1.0.0 DEBUG (if in debug mode, 'DEBUG' will be added)
    juce::String getModuleNameWithVersion();

    juce::String getDownloadLink();

    juce::String getVendorWebsite();

    juce::String getModuleWebsite();

    juce::String getVersionString();

  protected:
    juce::String vendor;
    juce::String moduleName;
    juce::String moduleVersionString;
    juce::String vendorWebsite;
    juce::String moduleWebsite;
    juce::String moduleDownloadLink;

    juce::File programFolder;
    juce::File documentsFolder;
    juce::File presetFolder;
};
