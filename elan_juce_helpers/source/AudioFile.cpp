#include <algorithm>
#include "AudioFile.h"
#include "maths.h"

using std::min;
using std::max;

static const String idCueLabel{ "CueLabel" };
static const String idCueRegion{ "CueRegion" };
static const String idCue{ "Cue" };
static const String idLoop{ "Loop" };

const Identifier Envelope::POINTS  = "points";
const Identifier Envelope::TIME    = "time";
const Identifier Envelope::VALUE   = "value";
const Identifier Envelope::SHAPE   = "shape";
const Identifier Envelope::TENSION = "tension";

WavFile::WavFile(const File& sourceFile)
{
	path = sourceFile;

	audioFormatManager.registerBasicFormats();
	reader.reset(audioFormatManager.createReaderFor(path));

	if (!reader)
	{		
		errorString = path.getFullPathName() + " -> Corrupted file or not an audio file.";
		jassertfalse;
		return;
	}

	sampleRate  = static_cast<int>(reader->sampleRate);
	bitDepth    = reader->bitsPerSample;
	numFrames   = int(reader->lengthInSamples);
	numChannels = reader->numChannels;

	metadata = reader->metadataValues;
	loadMetaData();
}

StringPairArray WavFile::getMetadata() const
{
	return newMetadata;
}

int WavFile::getNumSampleLoops() const
{
	return metadata["NumSampleLoops"].getIntValue();
}

int WavFile::getNumCueRegions() const
{
	return metadata["NumCueRegions"].getIntValue();
}

int WavFile::getNumCueLabels() const
{
	return metadata["NumCueLabels"].getIntValue();
}

int WavFile::getNumCuePoints() const
{
	return metadata["NumCuePoints"].getIntValue();
}

String WavFile::getStringValue(const String& prefix, int num, const String& postfix) const
{
	return metadata[prefix + String(num) + postfix];
}

int64 WavFile::getIntValue(const String& prefix, int num, const String& postfix) const
{
	return metadata[prefix + String(num) + postfix].getLargeIntValue();
}

void WavFile::loadMetaData()
{
	for (int i = 0; i < getNumSampleLoops(); ++i)
		loops.push_back(getSampleLoop(i));

	for (int i = 0; i < getNumCueRegions(); ++i)
		regions.push_back(getCueRegion(i));

	for (int i = 0; i < getNumCuePoints(); ++i)
	{
		bool alreadyPresent{ false };

		auto cuePoint = getCuePoint(i);

		for (auto &r : regions)
			if (r.label == cuePoint.label && r.offset == cuePoint.offset)
				alreadyPresent = true;

		if (!alreadyPresent)
			cuePoints.push_back(getCuePoint(i));
	}

	for (int i = 0; i < getNumSampleLoops(); ++i)
		loops.push_back(getSampleLoop(i));
}

void WavFile::createNewRegions()
{
	int regionId{0};

	for (auto& r : regions)
	{
		CuePoint cue;
		cue.offset = r.offset;
		cue.label = r.label;
		newCues.push_back(cue);

		newMetadata.set(idCueRegion + String(regionId) + "Identifier", String(newCues.size()));
		newMetadata.set(idCueRegion + String(regionId) + "SampleLength", String(r.length));
		newMetadata.set(idCueRegion + String(regionId) + "SampleLength", String(r.length));

		regionId++;
	}

	newMetadata.set("NumCueRegions", String(regionId));
}

void WavFile::addAdditionalCuePoints()
{
	for (auto & cue : cuePoints)
		newCues.push_back(cue);
}

void WavFile::createNewCuePoints()
{
	int cueIndex{ 0 };

	for (auto & r : newCues)
	{
		String baseCue = idCue + String(cueIndex);

		/* JUCE applies default values for data we don't provide here, e.g. ChunkID */
		newMetadata.set(baseCue + "Identifier", String(cueIndex + 1));
		newMetadata.set(baseCue + "Offset", String(r.offset));
		newMetadata.set(baseCue + "Order", String(r.offset));

		String baseLabel = idCueLabel + String(cueIndex);

		newMetadata.set(baseLabel + "Identifier", String(cueIndex + 1));
		newMetadata.set(baseLabel + "Text", r.label);

		cueIndex++;
	}

	newMetadata.set("NumCuePoints", String(cueIndex));
	newMetadata.set("NumCueLabels", String(cueIndex));
}

void WavFile::createNewLoops()
{
	int sampleLoopId{0};

	for (auto& l : loops)
	{
		newCues.push_back({ l.label, l.start });

		newMetadata.set(idLoop + String(sampleLoopId) + "Identifier", String(newCues.size()));
		newMetadata.set(idLoop + String(sampleLoopId) + "PlayCount", String(l.playcount));
		newMetadata.set(idLoop + String(sampleLoopId) + "Fraction", String(l.fraction));
		newMetadata.set(idLoop + String(sampleLoopId) + "Type", String(int(l.type)));
		newMetadata.set(idLoop + String(sampleLoopId) + "Start", String(l.start));
		newMetadata.set(idLoop + String(sampleLoopId) + "End", String(l.end));

		sampleLoopId++;
	}

	newMetadata.set("NumSampleLoops", String(sampleLoopId));
}

void WavFile::createMetaDataFromArrays()
{
	newMetadata.clear();
	newCues.clear();

	createNewRegions(); // creates regions based on vector<Region>
	createNewLoops(); // creates loops based on vector<Loop>, and this requires creating cue points
	addAdditionalCuePoints(); // adds required cue points from loops
	createNewCuePoints(); // creates cue points based on vector<CuePoint>
}

WavFile::CuePoint WavFile::getCuePoint(int index) const
{
	auto base = String("Cue") + String(index);

	CuePoint r;
	r.offset = getIntValue(idCue, index, "Offset");
	r.identifier = (int)getIntValue(idCue, index, "Identifier");

	// we also have ChunkID which should normally, it seems, be 'data'.
	jassert(getIntValue(idCue, index, "ChunkID") == 1635017060L);

	// appears to be zero ... need to check this one...
	jassert(getIntValue(idCue, index, "ChunkStart") == 0L);

	// The dwBlockStart field specifies the byte offset of the start of the block containing the 
	// position. This offset is relative to the start of the waveform data within the 'data' or 'slnt' chunk.
	jassert(getIntValue(idCue, index, "BlockStart") == 0L);

	r.label = getCueLabel(r.identifier);

	return r;
}

WavFile::CuePoint WavFile::getCuePointByIdentifier(int identifier) const
{
	for (int i = 0; i < getNumCuePoints(); ++i)
	{
		auto cp = getCuePoint(i);

		if (cp.identifier == identifier)
			return cp;
	}

	jassertfalse; // cue point not found
	return {};
}

String WavFile::getCueLabel(int identifier) const
{
	auto numLabels = getNumCueLabels();

	for (int i = 0; i < numLabels; ++i)
		if (getIntValue(idCueLabel, i, "Identifier") == identifier)
			return getStringValue(idCueLabel, i, "Text");

	return String();
}

WavFile::Region WavFile::getCueRegion(int index) const
{
	Region r;
	r.identifier = (int)getIntValue(idCueRegion, index, "Identifier");
	r.length = getIntValue(idCueRegion, index, "SampleLength");

	auto cuePoint = getCuePointByIdentifier(r.identifier);

	r.offset = cuePoint.offset;
	r.label = cuePoint.label;
	return r;
}

WavFile::Loop WavFile::getSampleLoop(int index) const
{
	Loop r;
	r.start = (int)getIntValue(idLoop, index, "Start");
	r.type = Loop::LoopType(getIntValue(idLoop, index, "Type"));
	r.end = (int)getIntValue(idLoop, index, "End");
	r.fraction = (int)getIntValue(idLoop, index, "Fraction");
	r.playcount = (int)getIntValue(idLoop, index, "PlayCount");
	r.cueIdentifier = (int)getIntValue(idLoop, index, "Identifier");
	return r;
}

const float* MetaAudioFile::Child::getReadPointer() const
{
	return audio.getReadPointer(0);
}

float* MetaAudioFile::Child::getWritePointer()
{
	return audio.getWritePointer(0);
}

void MetaAudioFile::Child::mixInfo(Child& child)
{
	sampleRate = std::max(sampleRate, child.sampleRate);
	bitDepth = std::max(bitDepth, child.bitDepth);
	clearInfoCache();
}

void MetaAudioFile::setAudioFilePath(const File& fileForAudio)
{
	audioFilePath = fileForAudio;
}

File MetaAudioFile::getAudioFilePath() const
{
	return audioFilePath;
}

bool MetaAudioFile::ensureAudioLoaded()
{
	if (audioIsLoaded)
		return true;

	WavFile wavAudioFile(audioFilePath);
	
	audioChildren.clear();
	audioChildren.resize(wavAudioFile.getNumChannels());	

	AudioBuffer<float> buffer = RenderBuffer::buffer(wavAudioFile);
	int n = wavAudioFile.getNumFrames();
	for (int ch = wavAudioFile.getNumChannels(); ch--; )
	{
		audioChildren[ch].sampleRate = wavAudioFile.getSampleRate();
		audioChildren[ch].bitDepth = wavAudioFile.getBitDepth();
		audioChildren[ch].numFrames = n;

		audioChildren[ch].audio.setSize(1, n);
		audioChildren[ch].audio.copyFrom(0, 0, buffer.getReadPointer(ch), n);
		buffer.setSize(ch, n, true, true); // discard last channel, don't need it after copying into audioChildren[ch]
	}

	updateEnabledChannels();

	audioIsLoaded = true;
	infoIsLoaded = true;

	return true;
}

bool MetaAudioFile::ensureInfoLoaded()
{
	if (infoIsLoaded)
		return true;

	if (audioFilePath.getFileName().isEmpty())
		wavFile.reset(new WavFile());
	else
		wavFile.reset(new WavFile(audioFilePath));

	audioChildren.clear();
	audioChildren.resize(wavFile->getNumChannels());

	for (int ch = wavFile->getNumChannels(); ch--; )
	{
		audioChildren[ch].sampleRate = wavFile->getSampleRate();
		audioChildren[ch].bitDepth   = wavFile->getBitDepth();
		audioChildren[ch].numFrames  = wavFile->getNumFrames();
	}

	updateEnabledChannels();

	infoIsLoaded = true;

	return true;
}

void MetaAudioFile::unload()
{
	audioChildren.clear();
	enabledChildren.clear();
	infoIsLoaded = false;
	audioIsLoaded = false;
}

void MetaAudioFile::disableChannel(int channel)
{
	if (!isPositiveAndBelow(channel, int(audioChildren.size())))
	{
		jassertfalse;
		return;
	}

	audioChildren[channel].disable();

	updateEnabledChannels();
}

void MetaAudioFile::enableChannel(int channel)
{
	if (!isPositiveAndBelow(channel, int(audioChildren.size())))
	{
		jassertfalse;
		return;
	}

	audioChildren[channel].enable();

	updateEnabledChannels();
}

void MetaAudioFile::disableAllChannels()
{
	for (auto& ch : audioChildren)
		ch.disable();

	enabledChildren.clear();
}

void MetaAudioFile::enableAllChannels()
{
	for (auto& c : audioChildren)
		c.enable();

	updateEnabledChannels();
}

void MetaAudioFile::setNumChannels(int v)
{
	if (isPositiveAndBelow(v, maxChannels))
	{
		audioChildren.resize(v);
		updateEnabledChannels();
		return;
	}

	jassertfalse;
	return;
}

int MetaAudioFile::getNumChannels() const
{
	jassert(infoIsLoaded);

	return int(audioChildren.size());
}

int MetaAudioFile::getNumEnabledChannels() const
{
	jassert(infoIsLoaded);

	return int(enabledChildren.size());
}

int MetaAudioFile::getNumFrames() const
{
	jassert(infoIsLoaded);

	int value = 0;

	for (const auto& c : audioChildren)
		value = std::max(value, c.getNumFrames());

	return value;
}

int MetaAudioFile::getNumEnabledFrames() const
{
	jassert(infoIsLoaded);

	int value = 0;

	for (const auto& c : enabledChildren)
		value = std::max<int>(value, c->getNumFrames());

	return value;
}

int MetaAudioFile::getNumFrames(int channel) const
{
	jassert(infoIsLoaded);

	if (!isPositiveAndBelow(channel, int(audioChildren.size())))
		return audioChildren[channel].getNumFrames();
	
	jassertfalse;
	return 0;
}

int MetaAudioFile::getSampleRate() const
{
	jassert(infoIsLoaded);

	int value = 0;
	for (const auto& c : audioChildren)
		value = std::max<int>(value, c.sampleRate);

	return value;
}

int MetaAudioFile::getEnabledSampleRate() const
{
	jassert(infoIsLoaded);

	int value = 0;
	for (const auto& c : enabledChildren)
		value = std::max<int>(value, c->sampleRate);

	return value;
}

int MetaAudioFile::getSampleRate(int channel) const
{
	jassert(channel >= 0 && channel < audioChildren.size());

	return audioChildren[channel].sampleRate;
}

int MetaAudioFile::getBitDepth() const
{
	jassert(infoIsLoaded);

	int value = 0;
	for (const auto& c : audioChildren)
		value = std::max<int>(value, c.bitDepth);

	return value;
}

int MetaAudioFile::getEnabledBitDepth() const
{
	jassert(infoIsLoaded);

	int value = 0;
	for (const auto& c : enabledChildren)
		value = std::max<int>(value, c->bitDepth);

	return value;
}

int MetaAudioFile::getBitDepth(int channel) const
{
	jassert(infoIsLoaded);

	if (isPositiveAndBelow(channel, int(audioChildren.size())))
		return audioChildren[channel].bitDepth;
	
	jassertfalse;
	return 0;
}

double MetaAudioFile::getLength()
{
	ensureInfoLoaded();
	return double(getNumFrames()) / double(getSampleRate());
}

double MetaAudioFile::getEnabledLength()
{
	ensureInfoLoaded();
	return double(getNumEnabledFrames()) / double(getEnabledSampleRate());
}

double MetaAudioFile::getLength(int channel)
{
	ensureInfoLoaded();
	return double(getNumFrames(channel)) / double(getSampleRate(channel));
}

bool MetaAudioFile::isChannelEnabled(int channel) const
{
	if (isPositiveAndBelow(channel, int(audioChildren.size())))
		return audioChildren[channel].isEnabled();

	jassertfalse;
	return false;
}

bool MetaAudioFile::isLoaded() const
{
	return audioIsLoaded;
}

const float* MetaAudioFile::getReadPointer(int channel) const
{
	jassert(audioIsLoaded);

	if (isPositiveAndBelow(channel, int(audioChildren.size())))
		return audioChildren[channel].audio.getReadPointer(0);
	
	jassertfalse;
	return nullptr;
}

const float* MetaAudioFile::getEnabledReadPointer(int channel) const
{
	jassert(audioIsLoaded);

	if (isPositiveAndBelow(channel, int(enabledChildren.size())))
		return enabledChildren[channel]->audio.getReadPointer(0);

	jassertfalse;
	return nullptr;
}

float* MetaAudioFile::getWritePointer(int channel)
{
	if (isPositiveAndBelow(channel, int(audioChildren.size())))
		return audioChildren[channel].getWritePointer();

	jassertfalse;
	return nullptr;
}

float* MetaAudioFile::getEnabledWritePointer(int channel)
{
	if (isPositiveAndBelow(channel, int(enabledChildren.size())))
		return enabledChildren[channel]->getWritePointer();

	jassertfalse;
	return nullptr;
}

bool MetaAudioFile::isValidChannel(int channel) const
{
	return isPositiveAndBelow(channel, getNumChannels());
}

void AudioFx::addChannel(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio, int sourceChannel)
{
	destAudio.ensureAudioLoaded();
	sourceAudio.ensureAudioLoaded();

	if (!isPositiveAndBelow(sourceChannel, sourceAudio.getNumChannels()))
	{
		jassertfalse;
		return;
	}
	if (destAudio.getNumChannels() >= maxChannels)
	{
		jassertfalse;
		return;
	}

	destAudio.audioChildren.resize(destAudio.audioChildren.size() + 1);

	destAudio.audioChildren.back() = sourceAudio.audioChildren[sourceChannel];

	destAudio.updateEnabledChannels();
}

void AudioFx::moveAudio(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio, int sourceChannel)
{
	destAudio.ensureAudioLoaded();
	sourceAudio.ensureAudioLoaded();

	if (!isPositiveAndBelow(sourceChannel, sourceAudio.getNumChannels()))
	{
		jassertfalse;
		return;
	}
	if (destAudio.getNumChannels() >= maxChannels)
	{
		jassertfalse;
		return;
	}

	destAudio.audioChildren.resize(destAudio.audioChildren.size() + 1);

	destAudio.audioChildren.back() = std::move(sourceAudio.audioChildren[sourceChannel]);
	sourceAudio.audioChildren.erase(sourceAudio.audioChildren.begin() + sourceChannel);

	destAudio.updateEnabledChannels();
}

void AudioFx::moveAudio(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio, const Array<var>& arr)
{
	destAudio.ensureAudioLoaded();
	sourceAudio.ensureAudioLoaded();

	auto arrSorted = arr;

	class Comp
	{
	public:
		static int compareElements(const var& first, const var& second)
		{
			int a = int(first);
			int b = int(second);
			return (a < b) ? -1 : ((b < a) ? 1 : 0);
		}
	} comp;

	arrSorted.sort(comp, true);

	if (!sourceAudio.isValidChannel(arrSorted.getLast()))
	{
		jassertfalse;
		return;
	}

	if (destAudio.getNumChannels() + arr.size() > maxChannels)
	{
		jassertfalse;
		return;
	}

	size_t channelToEdit = destAudio.audioChildren.size();

	destAudio.audioChildren.resize(destAudio.audioChildren.size() + arr.size());

	std::map<int, int> channelAppearanceCount;

	for (int c : arr)
		++channelAppearanceCount[c];

	for (int c : arr)
	{
		if (--channelAppearanceCount[c] > 0) // Allow for specifying the same channel more than once by only moving when applicable.
			destAudio.audioChildren[channelToEdit] = sourceAudio.audioChildren[c];
		else
			destAudio.audioChildren[channelToEdit] = std::move(sourceAudio.audioChildren[c]);

		++channelToEdit;
	}

	for (auto& [key, value] : channelAppearanceCount)
		sourceAudio.audioChildren.erase(sourceAudio.audioChildren.begin() + key);

	destAudio.updateEnabledChannels();
}

void AudioFx::removeChannel(MetaAudioFile& audio, int channel)
{
	audio.ensureAudioLoaded();

	if (!isPositiveAndBelow(channel, audio.getNumChannels()))
	{
		jassertfalse;
		return;
	}

	audio.audioChildren.erase(audio.audioChildren.begin() + channel);

	audio.updateEnabledChannels();
}

void AudioFx::mix(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio)
{
	destAudio.ensureAudioLoaded();
	sourceAudio.ensureAudioLoaded();

	int minChs = std::min(destAudio.getNumChannels(), sourceAudio.getNumChannels());
	int maxChs = std::max(destAudio.getNumChannels(), sourceAudio.getNumChannels()) - minChs;

	destAudio.setNumChannels(maxChs);

	for (int i = 0; i < sourceAudio.getNumChannels(); ++i)
		mix(destAudio.audioChildren[i], sourceAudio.audioChildren[i]);
}

void AudioFx::mix(MetaAudioFile& destAudio, int destChannel, MetaAudioFile& sourceAudio, int sourceChannel)
{
	destAudio.ensureAudioLoaded();
	sourceAudio.ensureAudioLoaded();

	if (!isPositiveAndBelow(sourceChannel, sourceAudio.getNumChannels()))
	{
		jassertfalse;
		return;
	}
	if (!isPositiveAndBelow(destChannel, destAudio.getNumChannels()))
	{
		jassertfalse;
		return;
	}

	mix(sourceAudio.audioChildren[sourceChannel], destAudio.audioChildren[destChannel]);
}

void AudioFx::mix(MetaAudioFile& destAudio, int destChannel, int sourceChannel)
{
	destAudio.ensureAudioLoaded();

	if (!isPositiveAndBelow(destChannel, destAudio.getNumChannels()))
	{
		jassertfalse;
		return;
	}

	mix(destAudio.audioChildren[sourceChannel], destAudio.audioChildren[destChannel]);
}

void AudioFx::mix(MetaAudioFile::Child& dstAudioCh, MetaAudioFile::Child& srcAudioCh)
{
	int maxSampleRate = std::max(srcAudioCh.sampleRate, dstAudioCh.sampleRate);

	if (dstAudioCh.sampleRate < maxSampleRate)
		setSampleRate(dstAudioCh, maxSampleRate);

	MetaAudioFile::Child newSrcObj;
	int numSamples;
	const float* readPtr;

	if (srcAudioCh.sampleRate < maxSampleRate)
	{
		newSrcObj = srcAudioCh;
		setSampleRate(newSrcObj, maxSampleRate);
		numSamples = newSrcObj.getNumFrames();
		readPtr = newSrcObj.getReadPointer();
	}
	else
	{
		numSamples = srcAudioCh.getNumFrames();
		readPtr = srcAudioCh.getReadPointer();
	}

	if (numSamples < dstAudioCh.getNumFrames())
		dstAudioCh.audio.setSize(1, numSamples, true, true);

	auto destPtr = dstAudioCh.getWritePointer();
	for (int s = 0; s < numSamples; ++s)
		destPtr[s] += readPtr[s];

	dstAudioCh.mixInfo(srcAudioCh);
}

void AudioFx::setSampleRate(MetaAudioFile::Child& child, int sampleRate)
{
	if (sampleRate <= 0)
		jassertfalse;

	if (child.sampleRate <= 0 || child.sampleRate == sampleRate || child.getNumFrames() <= 1)
	{
		child.sampleRate = sampleRate;
		return;
	}

	MetaAudioFile::Child newAudioChild;
	newAudioChild.sampleRate = sampleRate;
	newAudioChild.bitDepth = child.bitDepth;

	double resampleFactor = double(child.sampleRate) / double(sampleRate);
	int numSamples = child.getNumFrames();
	int newSampleSize = int(ceil(numSamples * 1.0 / resampleFactor));

	newAudioChild.audio.setSize(1, newSampleSize);

	RAPT::rsResampler<float, float>::transposeSinc(child.getReadPointer(), numSamples, newAudioChild.getWritePointer(), newSampleSize, float(resampleFactor));

	child = std::move(newAudioChild);
}

void AudioFx::setSampleRate(MetaAudioFile& audio, int sampleRate)
{
	// sampleRate is stored inside the audio child. If there are no children, nothing can be given a samplerate.
	jassert(audio.enabledChildren.size() > 0);

	for (auto& c : audio.enabledChildren)
		setSampleRate(*c, sampleRate);
}

void AudioFx::setBitDepth(MetaAudioFile::Child& child, int bitDepth)
{
	child.bitDepth = bitDepth;
}

void AudioFx::setBitDepth(MetaAudioFile& audio, int bitDepth)
{
	// bitDepth is stored inside the audio child. If there are no children, nothing can be given a bitdepth.
	jassert(audio.enabledChildren.size() > 0);

	for (auto& c : audio.enabledChildren)
		setBitDepth(*c, bitDepth);
}

void AudioFx::setSize(MetaAudioFile& audio, int numChannels, int numFrames)
{
	audio.setNumChannels(numChannels);

	for (auto& c : audio.audioChildren)
			c.setSize(numFrames);
}

// Sets the number of channels and frames for all channels. Data will be lost when setting to a smaller size.
void AudioFx::setNumFramesForEnabledChannels(MetaAudioFile& audio, int numFrames)
{	
	for (auto c : audio.enabledChildren)
		c->setSize(numFrames);
}

void AudioFx::replaceChannel(MetaAudioFile& destAudio, int destChannel, MetaAudioFile& sourceAudio, int sourceChannel)
{
	destAudio.ensureAudioLoaded();
	sourceAudio.ensureAudioLoaded();

	if (!isPositiveAndBelow(sourceChannel, sourceAudio.getNumChannels()))
	{
		jassertfalse;
		return;
	}
	if (!isPositiveAndBelow(destChannel, destAudio.getNumChannels()))
	{
		jassertfalse;
		return;
	}

	destAudio.audioChildren.back() = sourceAudio.audioChildren[sourceChannel];
}

double AudioFx::getPeakAmplitude(MetaAudioFile& audio)
{
	audio.ensureAudioLoaded();

	double maxValue = 0;

	for (auto& c : audio.enabledChildren)
	{
		double channelValue = 0;

		if (!c->peakAmplitudeWasStored)
		{
			auto ptr = c->getReadPointer();

			for (int s = 0; s < c->audio.getNumSamples(); ++s)
			{
				channelValue = std::max<double>(channelValue, abs(ptr[s]));

				if (channelValue >= 1.f)
					break;
			}

			c->peakAmplitudeWasStored = true;
			c->peakAmplitude = channelValue;
		}

		c->peakAmplitude = channelValue;

		maxValue = std::max<double>(maxValue, channelValue);
	}

	return maxValue;
}

double AudioFx::getPeakRMS(MetaAudioFile& audio)
{
	audio.ensureAudioLoaded();

	double maxValue = 0;

	for (auto& c : audio.enabledChildren)
	{
		double channelValue = 0;

		if (!c->peakRMSWasStored)
		{
			// seems to be giving a value that is half as much as it should be
			// auto ptr = c->getReadPointer();
			//channelValue = RAPT::rsArrayTools::meanSquare<float>(ptr, c->getNumFrames());

			channelValue = c->audio.getRMSLevel(0, 0, c->getNumFrames());

			c->peakRMSWasStored = true;
			c->peakRMS = channelValue;
		}

		maxValue = std::max<double>(maxValue, channelValue);
	}

	return maxValue;
}

void AudioFx::applyFadeIn(MetaAudioFile& audio, double time, int shape, double tension)
{
	audio.ensureAudioLoaded();

	Envelope env;

	for (auto& c : audio.enabledChildren)
	{
		auto ptr = c->getWritePointer();
		int n = c->audio.getNumSamples();
		int sr = c->getSampleRate();

		env.addNode(0, 0);
		env.addNode(time, 1, shape, tension);

		for (int s = 0; s < n; ++s)
			ptr[s] = ptr[s] * float(env.getValue(s, sr));
	}
}

void AudioFx::applyFadeOut(MetaAudioFile& audio, double time, int shape, double tension)
{
	audio.ensureAudioLoaded();

	Envelope env;

	for (auto& c : audio.enabledChildren)
	{
		auto ptr = c->getWritePointer();
		int n = c->getNumFrames();
		int sr = c->getSampleRate();

		env.addNode(float(c->getLength()), 0);
		env.addNode(float(c->getLength()) - time, 1, shape, float(tension));

		for (int s = 0; s < n; ++s)
			ptr[s] = ptr[s] * float(env.getValue(s, sr));
	}
}

void AudioFx::flattenPitch(MetaAudioFile& audio, double targetFrequency)
{
	jassert(targetFrequency > 0);

	audio.ensureAudioLoaded();

	for (int c = 0; c < audio.getNumEnabledChannels(); ++c)
	{
		auto child = audio.enabledChildren[c];
		auto sampleRate = double(child->getSampleRate());
		auto numFrames = int(child->getNumFrames());

		vector<double> origSignal = Convert::toVector<double, float>(child->getReadPointer(), numFrames);
		vector<double> newSignal = RAPT::rsFlattenPitch<double>(origSignal.data(), numFrames, sampleRate, targetFrequency);

		audio.replaceEnabledChannel(c, newSignal);
	}
}

void AudioFx::applyRateEnvelope(MetaAudioFile& audio, Envelope rateEnvelope)
{
	audio.ensureAudioLoaded();

	for (int c = 0; c < audio.getNumEnabledChannels(); ++c)
	{
		auto child = audio.enabledChildren[c];
		int sampleRate = child->getSampleRate();
		int numFrames = child->getNumFrames();

		vector<double> warpMap = rateEnvelope.getSamples(sampleRate, 0, numFrames);

		int newSignalSize = RAPT::rsTimeWarper<double, double>::getPitchModulatedLength(warpMap.data(), static_cast<int>(warpMap.size()));

		vector<double> origSignal = Convert::toVector<double, float>(child->getReadPointer(), numFrames);
		vector<double> newSignal(newSignalSize, 0);

		RAPT::rsTimeWarper<double, double>::applyPitchModulation(origSignal.data(), warpMap.data(), static_cast<int>(origSignal.size()), newSignal.data(), 16.0, 4.0, true);

		audio.replaceEnabledChannel(c, newSignal);
	}
}

void AudioFx::setStart(MetaAudioFile& audio, double time)
{
	audio.ensureAudioLoaded();

	for (auto& c : audio.enabledChildren)
	{
		int totalFrames = c->getNumFrames();
		int frameStart = juce::roundToInt(double(c->getSampleRate()) * time);
		int newTotalFrames = totalFrames - frameStart;

		// ensure frameStart is positive after we know if we are removing or adding frames
		frameStart = abs(frameStart);

		if (newTotalFrames < 0)
		{
			jassertfalse;
			return;
		}

		auto ptr = c->getWritePointer();

		// shift backwards
		if (time > 0)
		{
			for (int p = 0; p < newTotalFrames; ++p)
				ptr[p] = ptr[p + frameStart];
		}

		c->setSize(newTotalFrames, true);
		ptr = c->getWritePointer();

		// shift forward only after setting size
		if (time < 0)
		{
			// shift forward with reverse for loop
			for (int p = totalFrames; p--; )
				ptr[p + frameStart] = ptr[p];

			// silence the duplicate portion
			for (int p = 0; p < frameStart; ++p)
				ptr[p] = 0;
		}		
	}
}

void AudioFx::setEnd(MetaAudioFile& audio, double time)
{
	audio.ensureAudioLoaded();

	for (auto& c : audio.enabledChildren)
	{
		int newTotalFrames = static_cast<int>(round(double(c->getSampleRate()) * time));

		c->setSize(newTotalFrames, true, true);
	}
}

void AudioFx::trimZeros(MetaAudioFile& audio)
{
	audio.ensureAudioLoaded();

	for (auto& c : audio.enabledChildren)
	{
		auto read = c->getReadPointer();

		for (int i = c->getNumFrames(); i--> 0; )
		{
			if (read[i] != 0.0)
			{
				c->setSize(i + 1);
				break;
			}
		}
	}
}

void AudioFx::trimSilenceViaDecibels(MetaAudioFile& audio, double threshold_dB, double fadeTime) {
    audio.ensureAudioLoaded();

    double ampThreshold   = dbToAmpAccurate(threshold_dB);
    int numSamples        = audio.getNumEnabledFrames();
    int blockSize         = std::min(numSamples, 4096);
    int maxThresholdPoint = 0;

    for (auto& c : audio.enabledChildren) {
        c->setSize(numSamples, true, true);

        auto writePointer = c->getWritePointer();
        for (int b = 0; b < numSamples; b += blockSize) {
            double blocksizeAmp = 0.0;

            for (int s = 0; b + s < numSamples && s < blockSize; ++s)
                blocksizeAmp = std::max<double>(blocksizeAmp, fabs(writePointer[b + s]));

            if (blocksizeAmp < ampThreshold) {
                maxThresholdPoint = std::max(maxThresholdPoint, b);
                break;
            }
        }
    }

    // ramp up from end/fade point to fade start
    for (auto& c : audio.enabledChildren) {
        // each channel may be different in samplerate, so calculate fade time here
        int fadeOutSamples = std::min<int>(numSamples, static_cast<int>(round(double(c->getSampleRate())) * fadeTime));
        auto writePointer  = c->getWritePointer();
        for (int i = 0; i < fadeOutSamples; ++i) {
            writePointer[maxThresholdPoint - i] *= static_cast<float>(i) / static_cast<float>(fadeOutSamples);
        }

        c->setSize(maxThresholdPoint);
    }

    trimZeros(audio); // restore original lengths
}

AudioBuffer<float> RenderBuffer::buffer(const File& path, int* sampleRateOut, int* bitDepthOut)
{
    WavFile wavFile(path);
    *sampleRateOut = wavFile.getSampleRate();
    *bitDepthOut   = wavFile.getBitDepth();

    jassert(wavFile.reader != nullptr);

    AudioBuffer<float> audiobuffer(wavFile.getNumChannels(), wavFile.getNumFrames());
    wavFile.reader->read(&audiobuffer, 0, wavFile.getNumFrames(), 0, true, true);
    wavFile.reader = nullptr;

    return std::move(audiobuffer);
}

AudioBuffer<float> RenderBuffer::buffer(WavFile& wavFile)
{
	jassert(wavFile.reader != nullptr);

	AudioBuffer<float> audiobuffer(wavFile.getNumChannels(), wavFile.getNumFrames());
	wavFile.reader->read(&audiobuffer, 0, wavFile.getNumFrames(), 0, true, true);
	wavFile.reader = nullptr;

	return std::move(audiobuffer);
}

AudioBuffer<float> RenderBuffer::buffer(MetaAudioFile& metaAudioFile)
{
	int highestSampleRate = metaAudioFile.getEnabledSampleRate();

	AudioBuffer<float> buffer;
	buffer.setSize(metaAudioFile.getNumEnabledChannels(), metaAudioFile.getNumEnabledFrames(), false, true);

	int channel = 0;
	for (auto& c : metaAudioFile.enabledChildren)
	{
		if (c->getNumFrames() == 0)
			continue;

		if (c->getSampleRate() != highestSampleRate)
			AudioFx::setSampleRate(*c, highestSampleRate);

		buffer.copyFrom(channel, 0, c->audio.getReadPointer(0), c->getNumFrames());

		++channel;
	}

	return std::move(buffer);
}

SimpleAudio::SimpleAudio(const File& path)
{
	buffer = RenderBuffer::buffer(path, &sampleRate, &bitDepth);
	numFrames = buffer.getNumSamples();
	numChannels = buffer.getNumChannels();
}

Envelope Envelope::copy()
{
	Envelope e;
	e.nodes = this->nodes;
	return e;
}

double Envelope::getValue(int currentSample, int sampleRate)
{
	return nodes.getValue(double(currentSample) / double(sampleRate));
}

double Envelope::getValue(double time)
{
	return nodes.getValue(time);
}

void Envelope::addNode(double time, double value, int shape, double tension)
{
	nodes.addNode(time, value);
	nodes.setNodeShapeType(nodes.getNodes().size() - 1, saltShapeToNodeShape(shape));
	nodes.setNodeShapeParameter(nodes.getNodes().size() - 1, tension);
}

 vector<double> Envelope::getSamples(double sampleRate, int startFrame, int endFrame)
{
	jassert(startFrame <= endFrame);
	jassert(startFrame >= 0);
	jassert(endFrame > 0);

	vector<double> graph(endFrame, nodes.getNodeY(nodes.getNodes().size() - 1));

	for (int i = startFrame; i < endFrame; ++i)
		graph[i] = nodes.getValue(double(i) / double(sampleRate));

	return std::move(graph);
}

void Envelope::removePointsOutsideValue(double minValue, double maxValue)
{
	auto& points = nodes.getNodes();

	for (size_t i = points.size(); i--; )
		if (points[i].getY() < minValue || points[i].getY() > maxValue)
			nodes.removeNode(i);
}

void Envelope::removePointsOutsideTime(double minTime, double maxTime)
{
	auto& points = nodes.getNodes();

	for (size_t i = points.size(); i--; )
		if (points[i].getX() < minTime || points[i].getX() > maxTime)
			nodes.removeNode(i);
}

double Envelope::getAverage()
{
	vector<double> v = getSamples(100);

	return std::accumulate(v.begin(), v.end(), 0.0) / v.size();
}

double Envelope::getAverage(double startTime, double endTime)
{
	Envelope envelope = this->copy();
	envelope.removePointsOutsideTime(startTime, endTime);
	return envelope.getAverage();
}

double Envelope::getAverage(double startTime, double endTime, double minVal, double maxVal)
{
	Envelope envelope = this->copy();
	envelope.removePointsOutsideValue(minVal, maxVal);
	envelope.removePointsOutsideTime(startTime, endTime);
	return envelope.getAverage();
}
