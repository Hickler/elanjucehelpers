#include "PluginFileHelper.h"

/*
Example inputs:
JucePlugin_Manufacturer,
JucePlugin_Name,
JucePlugin_VersionString,
JucePlugin_ManufacturerWebsite,
"https://www.soundemote.com/labs/flower-child",
"http://www.elanhickler.com/Soundemote/Soundemote_-_Flower_Child_Filter.zip"
*/

void PluginFileHelper::setupPaths(String vendor, String module, String version, String vendorWebsite, String moduleWebsite, String downloadLink)
{
	this->vendor = vendor;
	this->moduleName = module;
	this->moduleVersionString = version;
	this->vendorWebsite = vendorWebsite;
	this->moduleWebsite = moduleWebsite;
	this->moduleDownloadLink = downloadLink;

#ifdef _WIN32
	programFolder = File::getSpecialLocation(File::SpecialLocationType::globalApplicationsDirectory).getFullPathName() + "/" + vendor;
	documentsFolder = File::getSpecialLocation(File::SpecialLocationType::userDocumentsDirectory).getFullPathName() + "/" + vendor;
#elif __APPLE__
	programFolder = "/Library/Audio/Presets/" + vendor;
	documentsFolder = juce::File("~/Documents").getChildFile(vendor);
#elif __linux__ 
	programFolder = File::getSpecialLocation(File::SpecialLocationType::globalApplicationsDirectory).getFullPathName() + "/" + vendor;
	documentsFolder = File::getSpecialLocation(File::SpecialLocationType::userDocumentsDirectory).getFullPathName() + "/" + vendor;
#endif
}

File PluginFileHelper::getVendorDocumentsFolder()
{
	return documentsFolder;
}

File PluginFileHelper::getVendorProgramFolder()
{
	return programFolder;
}

File PluginFileHelper::getModuleDocumentsFolder()
{
	return documentsFolder.getChildFile(moduleName);
}

File PluginFileHelper::getModuleProgramFolder()
{
	return programFolder.getChildFile(moduleName);
}

File PluginFileHelper::getPresetsFolder()
{
	return documentsFolder.getChildFile(moduleName).getChildFile("Presets/");
}

File PluginFileHelper::getFactoryPresetsFolder()
{
	return documentsFolder.getChildFile(moduleName).getChildFile("Presets/Factory/");
}

File PluginFileHelper::getUserPresetsFolder()
{
	return documentsFolder.getChildFile(moduleName).getChildFile("Presets/User/");
}

String PluginFileHelper::getModuleNameWithVersion()
{
#if JUCE_DEBUG		
	return moduleName + " " + moduleVersionString + " DEBUG";
#else	
	return moduleName + " " + moduleVersionString;
#endif
}

String PluginFileHelper::getDownloadLink()
{
	return moduleDownloadLink;
}

String PluginFileHelper::getVendorWebsite()
{
	return vendorWebsite;
}

String PluginFileHelper::getModuleWebsite()
{
	return moduleWebsite;
}

String PluginFileHelper::getVersionString()
{
	#if JUCE_DEBUG		
	return  moduleVersionString + " DEBUG";
	#else	
	return moduleVersionString;
	#endif
}
