#pragma once

#include "JuceHeader.h"
#include "sehelper.hpp"
#include <memory>
#include <vector>

#include <juce_audio_formats/juce_audio_formats.h>

#include "TypeConversion.h"

class AudioInfo {
  public:
    virtual int getSampleRate() const {
        return sampleRate;
    }
    virtual int getBitDepth() const {
        return bitDepth;
    }
    virtual int getNumFrames() const {
        return numFrames;
    }
    virtual int getNumChannels() const {
        return numChannels;
    }
    virtual int getNumSamples() const {
        return getNumFrames() * getNumChannels();
    }
    virtual int getLength() const {
        return getNumFrames() / getSampleRate();
    }

    juce::File getPath() const {
        return path;
    }

    // Sets the path used in audio functions. Path will be equal to File() if invalid.
    void setPath(const juce::File& f) {
        path = f;
    }

  protected:
    int sampleRate  = 0;
    int bitDepth    = 0;
    int numFrames   = 0;
    int numChannels = 0;
    juce::File path;
};

/* Easy class wrapper for rsNodeBasedFunction. */
class Envelope {
  public:
    static const juce::Identifier POINTS;
    static const juce::Identifier TIME;
    static const juce::Identifier VALUE;
    static const juce::Identifier SHAPE;
    static const juce::Identifier TENSION;

    RAPT::rsNodeBasedFunction<double> nodes;

    enum SHAPE {
        RAT,
        EXP
    };

    Envelope copy();

    double getValue(int currentSample, int sampleRate);
    double getValue(double time);

    //std::vector<double> getYValues();

    void addNode(double time, double value, int shape = 0, double tension = 0);

    std::vector<double> getSamples(double sampleRate, int startFrame = 0, int endFrame = 0);

    void removePointsOutsideValue(double minValue, double maxValue);
    void removePointsOutsideTime(double minTime, double maxTime);

    double getAverage();
    double getAverage(double startTime, double endTime);
    double getAverage(double startTime, double endTime, double minVal, double maxVal);

  protected:
    static int saltShapeToNodeShape(int v) {
        switch (v) {
        case Envelope::RAT:
            return RAPT::rsFunctionNode<double>::RATIONAL;
        default:
        case Envelope::EXP:
            return RAPT::rsFunctionNode<double>::EXPONENTIAL;
        }
    }
};

/**
 * WAV file meta-data decoder.
 *
 * @notes
 * This object could be expanded to preserve or allow editing of other types of WAV metadata
 */
class WavFile : public AudioInfo {
  public:
    WavFile() = default;

    WavFile(const juce::File& sourceFile);

    WavFile& operator=(const WavFile& other) {
        metadata    = other.metadata;
        newMetadata = other.newMetadata;
        newCues     = other.newCues;
        reader.reset(other.reader.get());

        sampleRate  = other.sampleRate;
        bitDepth    = other.bitDepth;
        numFrames   = other.numFrames;
        numChannels = other.numChannels;
        path        = other.path;

        cuePoints = other.cuePoints;
        loops     = other.loops;
        regions   = other.regions;

        return *this;
    }

    struct CuePoint {
        CuePoint(juce::String label, juce::int64 offset)
          : label(label)
          , offset(offset) {}
        CuePoint(){};
        juce::String label;
        juce::int64 offset = 0; // in samples

      private:
        friend class WavFile;
        int identifier = -1; // internal wav file use for linking labels with regions
    };

    // Using LTXT chunk data.
    struct Region {
        Region(juce::String label, juce::int64 offset, juce::int64 length)
          : label(label)
          , offset(offset)
          , length(length) {}
        Region(){};
        juce::String label;
        juce::int64 offset = 0; // samples
        juce::int64 length = 0; // samples

      private:
        friend class WavFile;
        int identifier = -1; // internal wav file use for linking regions with cue points
    };

    struct Loop {
        enum LoopType {
            kForward,
            kPingPong,
            kBackward
        } type{ kForward };

        Loop(juce::String label, int start, int end, LoopType type = LoopType::kForward, int fraction = 0, int playcount = 0)
          : label(label)
          , start(start)
          , end(end)
          , type(type)
          , fraction(fraction)
          , playcount(playcount) {}
        Loop(){};

        juce::String label;

        int start     = 0; // samples
        int end       = 0; // samples
        int fraction  = 0;
        int playcount = 0;

      private:
        friend class WavFile;
        int cueIdentifier = -1; // internal wav file use for linking regions with cue points
    };

    /**
     * Remove all loops, cuepoints and regions.
     */
    void clearAllMetadata() {
        loops.clear();
        regions.clear();
        cuePoints.clear();
    }

    /** Contains cue point, region and label information for all complete regions. */
    std::vector<Region> regions;

    /** Contains all the loops from the loaded sample file. */
    std::vector<Loop> loops;

    /** Contains all stand-alone cue points and labels (i.e. those that aren't associated with a region. */
    std::vector<CuePoint> cuePoints;

    /** Returns a copy of the metadata as read from the original file.  Used for debugging. */
    juce::StringPairArray getMetadata() const;

    juce::AudioFormatManager audioFormatManager;
    std::unique_ptr<juce::AudioFormatReader> reader;

    bool isValid() {
        return errorString.isEmpty();
    }
    juce::String getErrorString() {
        return errorString;
    }

  private:
    juce::String errorString;

    /* input buffer ... */
    juce::StringPairArray metadata;

    /* output buffers... */
    juce::StringPairArray newMetadata;
    std::vector<CuePoint> newCues;

    juce::String getStringValue(const juce::String& prefix, int num, const juce::String& postfix) const;
    juce::int64 getIntValue(const juce::String& prefix, int num, const juce::String& postfix) const;

    // Populates the various arrays based on the given wav file metadata.
    void loadMetaData();

    // Creates regions based on std::vector<Region>
    void createNewRegions();
    // Creates loops based on std::vector<Loop>, and this requires creating cue points
    void createNewLoops();
    // Adds required cue points from loops
    void addAdditionalCuePoints();
    // Creates cue points based on std::vector<CuePoint>
    void createNewCuePoints();

  public:
    // Calls various functions to translate vectors to actual wav file specced metadata.
    void createMetaDataFromArrays();

  private:
    // Queries wav metadata for loops
    int getNumSampleLoops() const;
    // Queries wav metadata for loops
    Loop getSampleLoop(int index) const;
    // Queries wav metadata for regions
    int getNumCueRegions() const;
    // Queries wav metadata for regions
    Region getCueRegion(int index) const;
    // Queries wav metadata for cue labels
    int getNumCueLabels() const;
    // Queries wav metadata for cue labels
    juce::String getCueLabel(int index) const;
    // Queries wav metadata for cue points
    int getNumCuePoints() const;
    // Queries wav metadata for cue points
    CuePoint getCuePoint(int index) const;
    // Queries wav metadata for cue points
    CuePoint getCuePointByIdentifier(int identifier) const;
};

/* Dynamic multichannel audio class that allows for disabling/enabling/removing/adding/insterting audio
where the audio buffers keep track of individual channel sample rates, bit depths, number of frames, and other info.
This class allows you to loop through enabled-only channels in order to selectively process FX.
*/
class MetaAudioFile {
  public:
    MetaAudioFile()  = default;
    ~MetaAudioFile() = default;

    MetaAudioFile(const juce::File& path) {
        audioFilePath = path;

        ensureInfoLoaded();
    }

    MetaAudioFile(const MetaAudioFile& other) {
        operator=(other);
    }

    MetaAudioFile& operator=(const MetaAudioFile& other) {
        audioChildren   = other.audioChildren;
        enabledChildren = other.enabledChildren;
        audioFilePath   = other.audioFilePath;
        wavFile.reset(other.wavFile.get());

        infoIsLoaded  = other.infoIsLoaded;
        audioIsLoaded = other.audioIsLoaded;

        return *this;
    }

    // Store path to an audio file for later use. See ensureAudioLoaded, ensureInfoLoaded
    void setAudioFilePath(const juce::File& fileForAudio);

    // Get the stored path to an audio file.
    juce::File getAudioFilePath() const;

    // When loading audio from disk, this transfers the audio to the meta audio buffers where each child has one buffer and one channel of data.
    bool ensureAudioLoaded();

    // Similar to ensureAudioLoaded except does not read audio into memory and only pulls info like channels, length, bitrate, etc.
    bool ensureInfoLoaded();

    // Call this when creating a blank metaAudioFile.
    void setAudioLoadedTrue() {
        infoIsLoaded = audioIsLoaded = true;
        wavFile.reset(new WavFile());
    }

    // Check if channel exists
    bool isValidChannel(int channel) const;

    // Returns true if meta audio buffer or audio file object has data.
    bool isLoaded() const;

    // Clears all memory allocation
    void unload();

    // Disable a channel. This channel is effectively muted and will not be rendered when calling writeToAudioBuffer.
    void disableChannel(int channel);

    // Enable a channel. This channel will render when calling writeToAudioBuffer.
    void enableChannel(int channel);

    // Disable all channels. Nothing will be written when calling writeToAudioBuffer.
    void disableAllChannels();

    // Enable all channels. All channels will be written when calling writeToAudioBuffer.
    void enableAllChannels();

    // Set number of channels. This will remove data for unused channels.
    void setNumChannels(int v);

    // Total number of channels counting disabled channels.
    int getNumChannels() const;

    // Number of channels not counting disabled channels.
    int getNumEnabledChannels() const;

    // Returns the number of samples from the channel with the most samples.
    int getNumFrames() const;

    // Returns the number of samples from the channel with the most samples, but only considers enabled channels.
    int getNumEnabledFrames() const;

    // Returns the number of samples from a specific channel.
    int getNumFrames(int channel) const;

    // Returns the highest samplerate considering all channels.
    int getSampleRate() const;

    // Returns the highest samplerate only enabled channels.
    int getEnabledSampleRate() const;

    // Returns the samplerate for a specific channel.
    int getSampleRate(int channel) const;

    // Returns the highest bit depth considering all channels.
    int getBitDepth() const;

    // Returns the highest bit depth considering only enabled channels.
    int getEnabledBitDepth() const;

    // Returns the bit depth for a specific channel.
    int getBitDepth(int channel) const;

    // Returns the longest length in time considering all channels.
    double getLength();

    // Returns the longest length in seconds considering only enabled channels.
    double getEnabledLength();

    // Returns the length in seconds for a specific channel.
    double getLength(int channel);

    // Return true if channel is enabled.
    bool isChannelEnabled(int channel) const;

    // Returns readable array pointer for given channel.
    const float* getReadPointer(int channel) const;

    // Returns readable array pointer for given enabled channel.
    const float* getEnabledReadPointer(int channel) const;

    // Returns writable array pointer for given channel.
    float* getWritePointer(int channel);

    // Returns writable array pointer for given enabled channel.
    float* getEnabledWritePointer(int channel);

    template<typename t>
    void replaceEnabledChannel(int channel, std::vector<t> signal) {
        auto audioChild = enabledChildren[channel];
        audioChild->setSize(toInt(signal.size())); // updates numFrames

        auto ptr = audioChild->getWritePointer();
        for (size_t i = 0; i < signal.size(); ++i)
            ptr[i] = toFloat(signal[i]);
    }

    // Holds audio data and info for as single channel
    class Child : public AudioInfo {
      public:
        Child() {
            setSize(0);
            numChannels = 1;
        }
        ~Child() = default;

        Child& operator=(const Child& other) {
            audio = other.audio;

            sampleRate = other.sampleRate;
            bitDepth   = other.bitDepth;
            numFrames  = other.numFrames;

            path = other.path;

            enabled = other.enabled;

            peakAmplitude          = other.peakAmplitude;
            peakRMS                = other.peakRMS;
            peakAmplitudeWasStored = other.peakAmplitudeWasStored;
            peakRMSWasStored       = other.peakRMSWasStored;

            return *this;
        }

        void enable() {
            enabled = true;
        }
        void disable() {
            enabled = false;
        }
        bool isEnabled() const {
            return enabled;
        }
        bool isDisabled() const {
            return !enabled;
        }

        juce::AudioBuffer<float> audio;

        const float* getReadPointer() const;
        float* getWritePointer();
        void setSize(int frames, bool keepExistingContent = true, bool clearExtraSpace = true, bool avoidReallocating = false) {
            audio.setSize(1, frames, keepExistingContent, clearExtraSpace, avoidReallocating);
            numFrames = frames;
        }

        // Find highest samplerate and bithdepth between itself and given child.
        void mixInfo(Child& child);

      protected:
        double peakAmplitude = 0;
        double peakRMS       = 0;

        bool enabled                = true;
        bool peakAmplitudeWasStored = false;
        bool peakRMSWasStored       = false;

        void clearInfoCache() {
            peakAmplitude          = 0;
            peakRMS                = 0;
            peakAmplitudeWasStored = false;
            peakRMSWasStored       = false;
        }

        friend class MetaAudioFile;
        friend class AudioFx;
    };

    std::vector<Child> audioChildren;
    std::vector<Child*> enabledChildren;

    std::unique_ptr<WavFile> wavFile; // to hold wav cues and loop meta data

  protected:
    juce::File audioFilePath;
    bool infoIsLoaded  = false;
    bool audioIsLoaded = false;

    void updateEnabledChannels() {
        enabledChildren.clear();

        for (auto& c : audioChildren)
            if (c.isEnabled())
                enabledChildren.push_back(&c);
    }

    const int maxChannels = 64;

    friend class AudioFx;
};

class AudioFx {
  public:
    AudioFx()  = default;
    ~AudioFx() = default;

    // Modifies given audio object with the new sampleRate. No conversion will happen if audio is already the given samplerate.
    static void setSampleRate(MetaAudioFile::Child& child, int sampleRate);

    // Modifies given audio object with the new sampleRate. No conversion will happen if audio is already the given samplerate.
    static void setSampleRate(MetaAudioFile& audio, int sampleRate);

    // Sets the bit depth for given audio channel.
    static void setBitDepth(MetaAudioFile::Child& child, int bitDepth);

    // Sets the bit depth for enabled channels.
    static void setBitDepth(MetaAudioFile& audio, int bitDepth);

    // Sets the number of channels and frames for all channels. Data will be lost when setting to a smaller size.
    static void setSize(MetaAudioFile& audio, int numChannels, int numFrames);

    // Sets the number of channels and frames for all channels. Data will be lost when setting to a smaller size.
    static void setNumFramesForEnabledChannels(MetaAudioFile& audio, int numFrames);

    // Add a new audio channel from source to the end of dest.
    static void addChannel(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio, int sourceChannel);

    // Add a new audio channel from source to the end of dest.
    static void moveAudio(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio, int sourceChannel);

    // Add a new audio channel from source to the end of dest.
    static void moveAudio(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio, const juce::Array<juce::var>& arr);

    // Remove an audio channel.
    static void removeChannel(MetaAudioFile& audio, int channel);

    // Mix source audio into dest audio
    // Will upsample the dest obj signal destructively to match highest samplerate if needed. Will upsample source non-destructively if needed.
    static void mix(MetaAudioFile& destAudio, MetaAudioFile& sourceAudio);

    // Mix source audio of source channel into dest audio of dest channel.
    // Will upsample the dest obj signal destructively to match highest samplerate if needed. Will upsample source non-destructively if needed.
    static void mix(MetaAudioFile& destAudio, int destChannel, MetaAudioFile& sourceAudio, int sourceChannel);

    // Mix audio of source channel into dest channel. Will upsample the dest obj signal destructively to match highest samplerate if needed.
    // Will upsample source audio non-destructively if needed.
    static void mix(MetaAudioFile& destAudio, int destChannel, int sourceChannel);

    // Mix audio source into dest. Will upsample the dest obj signal destructively to match highest samplerate if needed.
    // Will upsample source non-destructively if needed.
    static void mix(MetaAudioFile::Child& dstAudioCh, MetaAudioFile::Child& srcAudioCh);

    // Replace a channel of dest using source
    static void replaceChannel(MetaAudioFile& destAudio, int destChannel, MetaAudioFile& sourceAudio, int sourceChannel);

    // Replace a channel of dest using source
    template<typename t>
    static void replaceChannel(MetaAudioFile& destAudio, int destChannel, const std::vector<t>& sourceAudio, int sourceNumFrames) {
        destAudio.ensureAudioLoaded();

        if (!isPositiveAndBelow(destChannel, destAudio.getNumChannels())) {
            jassertfalse;
            return;
        }

        destAudio
          .enabledChildren

          for (int i = 0; i < sourceNumFrames; ++i) destAudio.getWritePointer(destChannel)[i] = sourceAudio[i];
    }

    // Get peak amplitude considering only enabled channels of source.
    static double getPeakAmplitude(MetaAudioFile& audio);

    // Sum audio to mono and return a std::vector. Amplitude is divided by number of channels.
    static std::vector<double> getAverageSum(MetaAudioFile& audio, std::vector<int> channelsToSum = {}) {
        audio.ensureAudioLoaded();

        std::vector<double> ret;
        ret.resize(audio.getNumEnabledFrames(), 0);

        int numChannels = 0;

        if (channelsToSum.empty()) {
            numChannels = audio.getNumEnabledChannels();

            for (auto& c : audio.enabledChildren)
                for (int i = 0; i < c->getNumFrames(); ++i)
                    ret[i] += c->getReadPointer()[i];
        } else {
            numChannels = toInt(channelsToSum.size());

            for (int channel = 0; channel < numChannels; ++channel) {
                int currentChannel = std::clamp<int>(channelsToSum[channel], 0, numChannels - 1);
                auto* c            = &audio.audioChildren[toSizeT(currentChannel)];

                for (int i = 0; i < c->getNumFrames(); ++i)
                    ret[i] += c->getReadPointer()[i];
            }
        }

        double div = audio.getNumEnabledChannels();

        std::for_each(std::begin(ret), std::end(ret), [&](double& v) { v /= div; });

        return ret;
    }

    // Get peak RMS value considering only enabled channels of source.
    static double getPeakRMS(MetaAudioFile& audio);

    static void normalize(MetaAudioFile& audio) {
        audio.ensureAudioLoaded();

        double gain = 1.0 / getPeakAmplitude(audio);

        for (auto& c : audio.enabledChildren) {
            auto ptr = c->getWritePointer();
            size_t n = c->getNumFrames();

            for (size_t s = 0; s < n; ++s)
                ptr[s] *= toFloat(gain);
        }
    }

    // Apply amplitude fade in only to enabled channels of source.
    static void applyFadeIn(MetaAudioFile& audio, double time, int shape = 0, double tension = 0);

    // Apply amplitude fade out only to enabled channels of source.
    static void applyFadeOut(MetaAudioFile& audio, double time, int shape = 0, double tension = 0);

    // Using RM-MET functions to detect and flatten pitch
    static void flattenPitch(MetaAudioFile& audio, double targetFrequency);

    // Continuously resamples the audio based on a rate envelope.
    static void applyRateEnvelope(MetaAudioFile& audio, Envelope rateEnvelope);

    // Remove audio content or add silence at start destructively to enabled channels.
    static void setStart(MetaAudioFile& audio, double time);

    // Remove audio content or add silence at end destructively to enabled channels.
    static void setEnd(MetaAudioFile& audio, double time);

    // Removes absolute silence content at the end of the audio on an individual channel basis
    static void trimZeros(MetaAudioFile& audio);

    // Removes content that dips below threshold_dB and fades audio out. Fades ends at threshold and begins at fadeTime minus threshold.
    static void trimSilenceViaDecibels(MetaAudioFile& audio, double threshold_dB, double fadeTime);

  private:
    static const int maxChannels = 64;
};

// Easy class to use to do simple audio functions, alternative to MetaAudioFile
class SimpleAudio : public AudioInfo {
  public:
    SimpleAudio()  = default;
    ~SimpleAudio() = default;

    // Loads audio from file path into memory
    SimpleAudio(const juce::File& path);

    // Creates empty initialized audio buffer
    SimpleAudio(int channels, int samples, int sampleRate, int bitDepth) {
        buffer.setSize(channels, samples, false, true, false);
        this->sampleRate  = sampleRate;
        this->bitDepth    = bitDepth;
        this->numFrames   = samples;
        this->numChannels = channels;
    }

    float* getWritePointer(int channel) {
        return buffer.getWritePointer(channel);
    }

    const float* getReadPointer(int channel) const {
        return buffer.getReadPointer(channel);
    }

    juce::AudioBuffer<float> buffer;
};

// Helper class to quickly go from audio classes to buffers or audio files.
class RenderBuffer {
  public:
    RenderBuffer()  = default;
    ~RenderBuffer() = default;

    static juce::AudioBuffer<float> buffer(const juce::File& path, int* sampleRateOut, int* bitDepthOut);

    static juce::AudioBuffer<float> buffer(WavFile& wavFile);

    // Writes enabled channels to buffer and returns buffer. Call getEnabled samplerate/bitdepth functions to get the relevant stats.
    static juce::AudioBuffer<float> buffer(MetaAudioFile& metaAudioFile);
};

class RenderWav {
  public:
    RenderWav()  = default;
    ~RenderWav() = default;

    // This is the actual final render function that all render functions refer to to create a file.
    // Sample Rate: 8000, 11025, 12000, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000, 352800, 384000
    // Bit Depth: 8, 16, 24, 32
    static bool file(const juce::AudioBuffer<float>& buffer, const juce::File& fileToWrite, int sampleRate, int bitDepth, const juce::StringPairArray& meta = {}) {
        if (fileToWrite == juce::File()) {
            jassertfalse;
            return false;
        }

        auto fs = new juce::FileOutputStream(fileToWrite);

        if (fs->failedToOpen()) // make sure folder exists
        {
            delete fs;
            jassertfalse;
            return false;
        }

        jassert(VectorHelper::contains(Convert::toVector(juce::WavAudioFormat().getPossibleSampleRates()), sampleRate));
        jassert(VectorHelper::contains(Convert::toVector(juce::WavAudioFormat().getPossibleBitDepths()), bitDepth));

        std::unique_ptr<juce::AudioFormatWriter> writer(juce::WavAudioFormat().createWriterFor(fs, sampleRate, buffer.getNumChannels(), bitDepth, meta, 0));

        if (!writer) {
            delete fs;
            jassertfalse;
            return false;
        }

        auto result = writer->writeFromAudioSampleBuffer(buffer, 0, buffer.getNumSamples());

        if (!result) {
            jassertfalse;
            return false;
        }

        return true;
    }

    static bool file(MetaAudioFile& metaAudioFile, const juce::File& fileToWrite) {
        metaAudioFile.wavFile->createMetaDataFromArrays();
        return file(RenderBuffer::buffer(metaAudioFile), fileToWrite, metaAudioFile.getSampleRate(), metaAudioFile.getBitDepth(), metaAudioFile.wavFile->getMetadata());
    }

    static bool file(SimpleAudio& simpleAudio, const juce::File& fileToWrite) {
        return file(simpleAudio.buffer, fileToWrite, simpleAudio.getSampleRate(), simpleAudio.getBitDepth());
    }
};

class RenderOgg {
  public:
    RenderOgg()  = default;
    ~RenderOgg() = default;

    // Quality Level  0 to 10: 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 500 kbps
    // Sample Rate: 8000, 11025, 12000, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000
    // Bit Depth: 32
    static bool file(const juce::AudioBuffer<float>& buffer, const juce::File& fileToWrite, int sampleRate, int qualityLevel = 6, const juce::StringPairArray& meta = {}) {
        if (fileToWrite == juce::File()) {
            jassertfalse;
            return false;
        }

        auto fs = new juce::FileOutputStream(fileToWrite);

        if (fs->failedToOpen()) // make sure folder exists
        {
            delete fs;
            jassertfalse;
            return false;
        }

        jassert(VectorHelper::contains(Convert::toVector(juce::OggVorbisAudioFormat().getPossibleSampleRates()), sampleRate));

        std::unique_ptr<juce::AudioFormatWriter> writer(juce::OggVorbisAudioFormat().createWriterFor(fs, sampleRate, buffer.getNumChannels(), 32, meta, qualityLevel));

        if (!writer) {
            delete fs;
            jassertfalse;
            return false;
        }

        auto result = writer->writeFromAudioSampleBuffer(buffer, 0, buffer.getNumSamples());

        if (!result) {
            jassertfalse;
            return false;
        }

        return true;
    }

    // Quality Level  0 to 10: 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 500 kbps
    // Sample Rate: 8000, 11025, 12000, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000
    // Bit Depth: 32
    static bool file(MetaAudioFile& metaAudioFile, const juce::File& fileToWrite, int qualityLevel = 6) {
        return file(RenderBuffer::buffer(metaAudioFile), fileToWrite, metaAudioFile.getSampleRate(), qualityLevel, metaAudioFile.wavFile->getMetadata());
    }

    // Quality Level  0 to 10: 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 500 kbps
    // Sample Rate: 8000, 11025, 12000, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000
    // Bit Depth: 32
    static bool file(SimpleAudio& simpleAudio, const juce::File& fileToWrite, int qualityLevel = 6) {
        return file(simpleAudio.buffer, fileToWrite, simpleAudio.getSampleRate(), qualityLevel);
    }
};

class RenderFlac {
  public:
    RenderFlac()  = default;
    ~RenderFlac() = default;

    // This is the actual final render function that all render functions refer to to create a file.
    // Compression Level: 0 (fastest), 1, 2, 3, 4, 5 (default), 6, 7, 8 (most compressed)
    // Sample Rate: 8000, 11025, 12000, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000, 352800, 384000
    // Bit Depth: 16 or 24
    static bool file(const juce::AudioBuffer<float>& buffer, const juce::File& fileToWrite, int sampleRate, int bitDepth, int compressionLevel = 5, const juce::StringPairArray& meta = {}) {
        if (fileToWrite == juce::File()) {
            jassertfalse;
            return false;
        }

        auto fs = new juce::FileOutputStream(fileToWrite);

        if (fs->failedToOpen()) // make sure folder exists
        {
            delete fs;
            jassertfalse;
            return false;
        }

        VectorHelper::contains(Convert::toVector(juce::FlacAudioFormat().getPossibleSampleRates()), sampleRate);
        VectorHelper::contains(Convert::toVector(juce::FlacAudioFormat().getPossibleBitDepths()), bitDepth);

        std::unique_ptr<juce::AudioFormatWriter> writer(juce::FlacAudioFormat().createWriterFor(fs, sampleRate, buffer.getNumChannels(), bitDepth, meta, compressionLevel));

        if (!writer) {
            delete fs;
            jassertfalse;
            return false;
        }

        auto result = writer->writeFromAudioSampleBuffer(buffer, 0, buffer.getNumSamples());

        if (!result) {
            jassertfalse;
            return false;
        }

        return true;
    }

    // Compression Level: 0 (fastest), 1, 2, 3, 4, 5 (default), 6, 7, 8 (most compressed)
    // Sample Rate: 8000, 11025, 12000, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000, 352800, 384000
    // Bit Depth: 16 or 24
    static bool file(MetaAudioFile& metaAudioFile, const juce::File& fileToWrite, int compressionLevel = 5) {
        return file(
          RenderBuffer::buffer(metaAudioFile), fileToWrite, metaAudioFile.getSampleRate(), juce::jlimit(16, 24, metaAudioFile.getBitDepth()), compressionLevel, metaAudioFile.wavFile->getMetadata());
    }

    // Compression Level: 0 (fastest), 1, 2, 3, 4, 5 (default), 6, 7, 8 (most compressed)
    // Sample Rate: 8000, 11025, 12000, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000, 352800, 384000
    // Bit Depth: 16 or 24
    static bool file(SimpleAudio& simpleAudio, const juce::File& fileToWrite, int compressionLevel = 5) {
        return file(simpleAudio.buffer, fileToWrite, simpleAudio.getSampleRate(), juce::jlimit(16, 24, simpleAudio.getBitDepth()), compressionLevel);
    }
};
