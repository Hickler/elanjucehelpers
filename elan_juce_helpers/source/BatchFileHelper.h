﻿#pragma once

#include "JuceHeader.h"
#include <map>
#include <vector>

class BatchFileChanger
{
public:
	BatchFileChanger() = default;
	~BatchFileChanger() = default;

	enum Operation
	{
		none,
		copy,
		move,
		rename,
		temporary,
		invalid
	};

	enum Padding
	{
		off,
		automatic
	};

	enum Order
	{
		forward,
		reverse,
		random
	};

	class Task
	{
	public:
		friend BatchFileChanger;

		// returns newFile or newFilewithDupeID based on what is relevant
		juce::String getNewPathStr()
		{
			return newFileWithDupID.isEmpty() ? newFile : newFileWithDupID;
		}

		int getDupNumber()
		{
			return dupNumber;
		}

	protected:
		juce::String origFile;
		juce::String userInput;
		juce::String random;
		juce::String newFile;
		juce::String newFileWithDupID;
		int dupNumber = 1;

		Operation operation = Operation::none;

		bool completedTask = false;
		bool completedRandomRemove = false;
		bool taskFailed = false;
		bool undoFailed = false;
	};

	/*
	OPERATIONS
	none: This is reserved for files not being manipulated.
	move: New path can be a directory or a file path.
	rename: New path can be a file name or file path.
	copy: New path can be empty if duplicate files are desired. If a name is provided, the copied file will share the same directory with a new name.
	temporary: Moves file to a new folder where the new path is the given new path plus the original path, essentially preserving its folder structure.
	*/
	Task* addTask(juce::String originalPath, juce::String newPath, Operation operation);

	// Customize the duplicate formatting with regex and tag script which is an extended regex.
	void setDuplicateFormat(int startingNumber, 
		juce::String regexMatch = ".*\\.", juce::String tagScriptReplace = "(<num>).", 
		int padFlag = Padding::automatic, int orderFlag = Order::forward, 
		bool onlyRenameDuplicates = true);

	// Avoid duplicate filenames before running tasks using this function. By default, it will use fileName(1).ext, fileName(2).ext style of formatting.
	void applyDuplicateFormat();

	// Returns tab and newline separated string displaying file conflicts that will occur if tasks are run. Returns empty string if no issues found.
	juce::String checkForConflicts();

	// Runs all tasks. Once tasks are run, tasks can no longer be changed. If fails, an be called again and continues where it left off.
	void runTasks();

	// Runs tasks in reverse attempting to restore files to a state before tasks were run. If fails, an be called again and continues where it left off.
	void undoTasks();

	// Returns comma separated array of strings of oldPath,newPath.
	std::vector<juce::String> getRenameList();

	std::vector<std::unique_ptr<Task>> TASKS;

protected:
	struct DuplicateFormat
	{
		int startingNumber = 1;
		std::regex regexMatch{ ".*\\." };
		juce::String tagScriptReplace = "(<num>).";
		int padFlag = Padding::automatic;
		int orderFlag = Order::forward;
		bool onlyRenameDuplicates = true;
	} duplicateFormat;

	juce::String getOperationName(Operation operation);

	juce::String generateRandom7CharString();

	// map of new path string and task inside a map of directories
	std::map<juce::String, std::map<juce::String, std::vector<Task*>>> taskMapNew;
	std::map<juce::String, std::map<juce::String, std::vector<Task*>>> taskMapOriginal;
	std::vector<Task*> orderOfOperations;
	
	// std::map of random strings generated to make sure no duplicates
	std::map<juce::String, int> randomStrings;

	juce::Random randomGen;
	bool runTasksWasCalled = false;
	bool didCheckForConflicts = false;
};

/*
Helps create a list of non-existing file paths, checks for duplicates, and adds a duplicate suffix number, and renames existing files with duplicate suffix numbers when needed.
Pass in an array of strings and BatchFileHelper will add those strings as files to a collection as well as add all existing files that are in folders of provided strings.
Call clearDupeNumbers() if you want non-existing and existing files to add dupe suffixes from scratch.
*/
class BatchFileHelper
{
public:
	enum AccessMethod
	{
		originalNameWithoutSuffix,
		originalNameWithSuffix,
		newNameWithoutSuffix,
		newNameWithSuffix
	};

	BatchFileHelper(const std::vector<juce::String>& filePathStrings);

	std::vector<juce::String> getFilesToBeCreated();

	/* 
	Recommend calling 'fileListContainsDuplicates(SearchMethod::newNameWithSuffix)' before
	running this action. If you want to solve duplicates, call 'addDuplicateSuffixToCollection()'
	*/
	bool createNewFiles();

	/* 
	This function will rename existing files so they are not overwritten.
	*/
	bool createNewFilesAndAddDupeSuffixToExistingFiles(bool doNotModifyNonDuplicates = false);

	/*
	Removes dupe numbers from rename schedule including existing files,
	useful when you need to remove the dupe suffix from existing files
	so that there are no non-consecutive dupe numbers.
	*/
	void clearDupeNumbers();

	std::vector<int> getUnusedDupeNumbers(std::vector<juce::File>& files);

	/* 
	Prints rename list as a string as oldname + seperator + newname + \n and each folder is separated by additional spaces.
	Duplicate file check is based on non-suffixed version of the file names.
	*/
	juce::String printRenameList(juce::String seperator = " -> ", bool printOnlyIfRenaming = false, bool printDuplicatesOnly = false);

	/*
	Prints rename schedule list as "newname + \n" and each folder is separated by additional spaces.
	Duplicate file check is based on non-suffixed version of the file names.
	*/
	juce::String printFileList();

	/*
	Checks whether BatchFileHelper contains duplicate file names.
	Duplicate file check is based on the method used, either with or without duplicate suffix, either original file names or scheduled new file names
	By default it will perform a duplicate check on files specifed at creation and against existing files.
	*/
	bool checkForDuplicates(AccessMethod method = originalNameWithoutSuffix);

	/*
	Checks the file rename schedule list for illegal characters in file name and path.
	Rename list is by default the original file path.
	*/
	bool fileListContainsIllegalChars(AccessMethod method = newNameWithSuffix);

	/*
	Replaces all illegal characters with _ and trims whitespace where needed. This overwrites all changes made with other functions! Call this first!
	NOTE: You may want to check for duplicates after running this action or call 'addDuplicateSuffixToCollection' to ensure no duplicates
	NOTE: This will reset the undo functionality.
	*/
	void solveIllegalCharacterIssues();

	/* 
	Adds a "duplicate" suffix of "(#)" to the string in order to prevent duplicate strings, e.g 'buiscuits' appearing 3 times will be 'buiscuits(1)', 'buiscuits(2)', 'biscuits(3)',
	'(1)' will be added to the first duplicate.
	Suffixes on existing files will be preserved. Existing files will be assigned suffix numbers above and below existing suffixes
	*/
	void addDuplicateSuffixToCollection(bool doNotModifyNonDuplicates = false);

	/*
	This cannot be undone unless the action fails at creating a temp folder. Undo will automatically be triggered if needed.
	files are moved to saltTempDelete folder (found in directory of given file) and then the folder is moved to trash.
	By default, files deleted are the ones specified when this object was created. Change method to delete rename schedule files.
	*/
	bool trashListedExistingFiles(AccessMethod method = originalNameWithSuffix);

	// Attempts to undo file changes. Returns false if something went wrong. Call this function again to retry the undo, it will pick up where it left off.
	bool undoFileChanges();

protected:

	struct fileNameChangesStruct
	{
		juce::File oldFile;
		juce::File newfile;
		bool fileWasCreated = false;
	};

	struct fileNameDupeStruct
	{
		fileNameDupeStruct();
		fileNameDupeStruct(const juce::File& s, int addIndex);
		fileNameDupeStruct(const juce::String& s, int addIndex);

		bool hasDupeNumber();

		void clearDupeNumber();

		void updateNewPath(const juce::String& s);

		juce::File originalFile;
		juce::String dir;
		juce::String originalNameNoSuffix;
		juce::String originalNameNoSuffixNoExt;
		juce::String ext;
		juce::String originalName;
		juce::String newName;
		juce::String newPath;
		int dupeNumber = -1;
		int idx = -1;

		static bool hasDupeSuffix(const juce::String& s, int& indexOfOpenParenOut, int& indexOfCloseParenOut, int& dupeNumberOut);

		// NOTE: Provide file name without extension!!!
		static juce::String removeDupeSuffix(const juce::String& fileNameNoExt, int* dupeNumberOut = nullptr);

		static juce::String addDupeSuffix(const juce::String& s, int dupeNumberToUse);
	};

	std::vector<fileNameChangesStruct> fileNameChanges;
	std::vector<juce::String> newFileList;
	std::map<juce::String, std::map<juce::String, std::vector<fileNameDupeStruct>>> folderFileMap;
	juce::Random random;
	int successfulUndoActions = 0;
	std::map<juce::String, juce::String> renameList;

	void collectFiles();

	void collectFiles(const std::vector<juce::String>& newFiles, std::map<juce::String, std::map<juce::String, std::vector<fileNameDupeStruct>>>& fileMapInput, AccessMethod methodForDuplicateCheck);

	void refreshFileFolderMap();

	void swapFileNames(juce::File& file1, juce::File& file2, std::vector<fileNameChangesStruct>& changes);

	juce::String generateRandom4CharString();

	bool attemptToUndoFileRenames();

	bool renameExistingFiles();
};