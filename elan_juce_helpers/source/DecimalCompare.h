#pragma once

bool isEqual(double a, double b, double accuracy = 1.e-6);

bool isNotEqual(double a, double b, double accuracy = 1.e-6);

bool isLessThan(double a, double b, double accuracy = 1.e-6);

bool isMoreThan(double a, double b, double accuracy = 1.e-6);

bool isLessThanOrEqual(double a, double b, double accuracy = 1.e-6);

bool isMoreThanOrEqual(double a, double b, double accuracy = 1.e-6);

bool isTouchingRange(double time, double start, double end, double accuracy = 1.e-6);

bool isInsideRange(double time, double start, double end, double accuracy = 1.e-6);

//template <typename t> bool isEqual(t a, t b, t accuracy = t(1.e-6))
//{
//	return abs(a - b) <= accuracy;
//}
//
//template <typename t> bool isNotEqual(t a, t b, t accuracy = t(1.e-6))
//{
//	return abs(a - b) > accuracy;
//}
//
//template <typename t> bool isLessThan(t a, t b, t accuracy = t(1.e-6))
//{
//	return isNotEqual(a, b, accuracy) && a < b;
//}
//
//template <typename t> bool isMoreThan(t a, t b, t accuracy = t(1.e-6))
//{
//	return isNotEqual(a, b, accuracy) && a > b;
//}
//
//template <typename t> bool isLessThanOrEqual(t a, t b, t accuracy = t(1.e-6))
//{
//	return isEqual(a, b, accuracy) || a <= b;
//}
//
//template <typename t> bool isMoreThanOrEqual(t a, t b, t accuracy = t(1.e-6))
//{
//	return isEqual(a, b, accuracy) || a >= b;
//}
//
//template <typename t> bool isTouchingRange(t time, t start, t end, t accuracy = t(1.e-6))
//{
//	return isLessThanOrEqual(time, end, accuracy) && isMoreThanOrEqual(time, start, accuracy);
//}
//
//template <typename t> bool isInsideRange(t time, t start, t end, t accuracy = t(1.e-6))
//{
//	return isLessThan(end, time, accuracy) && isMoreThan(start, time, accuracy);
//}
//
