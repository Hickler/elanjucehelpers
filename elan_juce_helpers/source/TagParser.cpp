#include "TagParser.h"

//TagParser tp_test("<articulation>,,,<vel>,yesfdgdfg");

TagParser::TagParser(const String& in_str) {
    StringIterator iter(in_str);

    auto p = iter.p1;

    bool inside_bracket = false;

    while (!iter.atEnd()) {
        switch (mode) {
        case Begin:
            prefix = iter.consumeToChar_IgnoreEscapes("<[", '\\');
            switch ((char)iter) {
            case '<':
                iter.inc();
                mode = InsideExpression;
                continue;
            case '[':
                if (prefix.isNotEmpty()) {
                    tags.push_back({ "", prefix, "", false, false });
                    prefix = "";
                }
                iter.inc();
                mode = InsideBracket;
                continue;
            }
            continue;

        case InsideExpression:
            property_name = iter.consumeToChar_IgnoreEscapes("#>", '\\');

            if (iter == '#') {
                use_value = true;
            }

            iter.skipPastChar('>');

            if (inside_bracket) {
                suffix = iter.consumeToChar_IgnoreEscapes("]", '\\');
                iter.move();
                inside_bracket = false;
            }

            mode = SaveResult;
            continue;

        case InsideBracket:
            inside_bracket = is_optional = true;

            prefix = iter.consumeToChar_IgnoreEscapes("<", '\\');
            iter.move();
            mode = InsideExpression;
            continue;

        case SaveResult:
            tags.push_back({ property_name, prefix, suffix, use_value, is_optional });
            property_name.clear();
            use_value   = false;
            is_optional = false;
            mode        = Begin;
            continue;
        }
    }
    if (mode == SaveResult || prefix.isNotEmpty())
        tags.push_back({ property_name, prefix, suffix, use_value, is_optional });
}

void Tagger::setup(String input)
{
    if (!isTagScript(input))
    {
        name = input;
        return;
    }

    tagmap.clear();

    vector<String> tempvect = STR::split(input, "`", 1, "");
    name = tempvect[0];

    for (int i = 1; i < tempvect.size(); ++i)
    {
        vector<String> temp = STR::splitToVector(tempvect[i], "=");

        if (temp.size() < 2) 
            continue;

        tagmap[temp[0]] = temp[1];
    }
}

Tagger::Tagger(String input)
{
    setup(input);
}

// Return full name and tags.
String Tagger::KeySort(map<String, String> m) const
{
    String s;

    for (const auto& it : order)
    {
        auto needle = m.find(it);
        if (needle != m.end())
        {
            s += "`" + needle->first + "=" + needle->second;
            m.erase(needle);
        }
    }

    for (auto&& it : m)
        s += "`" + it.first + "=" + it.second;

    return s;
}

String Tagger::getTagString() const
{
    return KeySort(tagmap);
}

String Tagger::fromTagScript(const String& tagScript) const
{
    vector<String> tagErrors;

    TagParser tp(tagScript);

    String result;
    for (auto& t : tp.tags)
    {
        if (t.property_name.isEmpty())
        {
            result += t.prefix;
            continue;
        }

        String tagValue = getTag(t.property_name);

        if (tagValue.isEmpty())
        {
            if (t.is_optional)
                continue;
            else
                tagErrors.push_back(t.property_name);
        }

        result += t.prefix + tagValue + t.suffix;
    }

    if (!tagErrors.empty())
    {
        string wasOrWere = tagErrors.size() > 1 ? "were" : "was";
        throw std::invalid_argument(Convert::toString(tagErrors, ",").toStdString() + " " + wasOrWere + " not marked as optional and value is empty. Surround each tag with [] brackets to prevent error.");
    }

    return result;
}
String Tagger::getString() const
{
    return getNameString() + getTagString();
}
String Tagger::getImportantTagString() const
{
    auto temp_map = tagmap;
    for (const auto& tag : uniques) temp_map.erase(tag);
    return name + KeySort(temp_map);
}

String Tagger::getNameString() const
{
    return name;
}
void Tagger::setString(const String& input)
{
    setup(input);
}

void Tagger::setNameString(const String& input)
{
    name = input;
}

// Set the full tag string.

void Tagger::setTagString(const String& input)
{
    setup(name + input);
}


/* Boolean */
// Check if string is properly formatted tag script

bool Tagger::isTagScript(const String& v)
{
    StringIterator iter(v);

    enum Mode { carat, equals };
    Mode mode = carat;

    for (;;)
    {
        switch (mode)
        {
        case carat:
            iter.skipToChar('`');

            if (iter.atEnd())
                return true;

            iter.inc();

            if (iter.atEnd() || iter.isAny("`=") )
                return false;

            mode = equals;
            continue;

        case equals:
            iter.skipToChar("=");

            if (iter.atEnd())
                return false;

            iter.inc();

            if (iter.atEnd() || iter.isAny("`=") )
                return false;

            mode = carat;
            continue;
        }
    }

    return true;
}

//returns "" on error
bool Tagger::hasTag(const String& tag) const
{
    return tagmap.find(tag) != tagmap.end();
}
bool Tagger::isTagDefined(const String& tag) const
{
    return tagmap.find(tag)->second.isNotEmpty();
}
String Tagger::getTag(const String& tag, char /*typechar*/) const
{
    auto it = tagmap.find(tag);      

    if (it == tagmap.end())
        return "";

    return it->second;
}

void Tagger::renameTag(const String& tag, const String& newName)
{
    auto it = tagmap.find(tag);

    if (it == tagmap.end())
        return;

    auto val = it->second;

    tagmap.erase(it);

    setTag(newName, val);
}

void Tagger::removeTag(const String& tag)
{
    tagmap.erase(tag);
}
void Tagger::removeAllTags()
{
    tagmap.clear();
}
void Tagger::removeTags(const vector<String>& TagList)
{
    for (const auto& tag : TagList)
        removeTag(tag);
}
