#pragma once

#include "JuceHeader.h"

#include <regex>
#include <vector>

class RegexHelper {
  public:
    static const juce::String charsThatNeedEscape;

    static std::regex exp(const juce::String& expression, bool caseSensitive = false);

    static juce::String literalize(juce::String input) {
        for (auto ptr = input.getCharPointer(); ptr.isNotEmpty(); ++ptr) {
            if (CHAr::isAny(*ptr, charsThatNeedEscape)) {
                --ptr;
                ptr.write('\\');
            }
        }

        return std::move(input);
    }

    /* Returns true if the entire expression matches input. */
    static bool match(const juce::String& input, const std::regex& expression);
    /* Returns true if the entire expression matches input. */
    static bool match(const juce::String& input, const juce::String& expression, bool caseSensitive = false);

    /* Returns true if part of the expression matches input. */
    static bool search(const juce::String& input, const std::regex& expression);
    /* Returns true if part of the expression matches input. */
    static bool search(const juce::String& input, const juce::String& expression, bool caseSensitive = false);

    /* Returns the input string where all matches are removed (replaced with an empty string). */
    static juce::String remove(const juce::String& input, const std::regex& expression);
    /* Returns the input string where all matches are removed (replaced with an empty string). */
    static juce::String remove(const juce::String& input, const juce::String& expression, bool caseSensitive = false);

    /* Returns the first match from the input string based on the regex and output expression. Use
    this to extract data from a string. */
    static juce::String extract(const juce::String& input, const std::regex& expression, const juce::String& outExpr);
    /* Returns the first match from the input string based on the regex and output expression. Use
    this to extract data from a string. */
    static juce::String extract(const juce::String& input, const juce::String& expression, const juce::String& outExpr, bool caseSensitive = false);

    /* Returns the input string based on the regex and output expression, but returns replacements
    with non-matches. Use this to manipulate or replaces matches with the output expression or
    remove matches from the string with a blank output expression. */
    static juce::String substitute(const juce::String& input, const std::regex& expression, const juce::String& outExpr);
    /* Returns the input string based on the regex and output expression, but returns replacements
    with non-matches. Use this to manipulate or replaces matches with the output expression or
    remove matches from the string with a blank output expression. */
    static juce::String substitute(const juce::String& input, const juce::String& expression, const juce::String& outExpr, bool caseSensitive = false);

    /* Returns an array via capture groups. Index 0 is all matched strings in or out of groups,
    index 1 is group 1, etc. */
    static std::vector<juce::String> iterateAll(const juce::String& input, const std::regex& expression);
    /* Returns an array via capture groups. Index 0 is all matched strings in or out of groups,
    index 1 is group 1, etc. */
    static std::vector<juce::String> iterateAll(const juce::String& input, const juce::String& expression, bool caseSensitive = false);

    /* Iterates through the string matching based on expression and capturing what is in a group. */
    static std::vector<juce::String> iterateSub(const juce::String& input, const std::regex& expression);
    /* Iterates through the string matching based on expression and capturing what is in a group. */
    static std::vector<juce::String> iterateSub(const juce::String& input, const juce::String& expression, bool caseSensitive = false);

    /* Splits a string based on the delimiter expression. Does not keep the matched delimiters.
    Groups don't affect result. */
    static std::vector<juce::String> split(const juce::String& input, const std::regex& expression);
    /* Splits a string based on the delimiter expression. Does not keep the matched delimiters.
    Groups don't affect result. */
    static std::vector<juce::String> split(const juce::String& input, const juce::String& expression, bool caseSensitive = false);

    /* Splits a string based on the delimiter expression and preserves the delimiter in each match.
    Groups don't affect result. Important: If the first delimiter is at the beginning of the
    string, delimiters will be placed at the beginning of each match, otherwise it will be placed
    at the end. */
    static std::vector<juce::String> splitGreedy(const juce::String& input, const std::regex& expression);
    /* Splits a string based on the delimiter expression and preserves the delimiter in each match.
    Groups don't affect result. Important: If the first delimiter is at the beginning of the
    string, delimiters will be placed at the beginning of each match, otherwise it will be placed
    at the end. */
    static std::vector<juce::String> splitGreedy(const juce::String& input, const juce::String& expression, bool caseSensitive = false);

  private:
    // add '{1}' after square bracket ranges unless there already is a quantifier or alternation such as '?' '*' '+' '{3,4}'
    static std::string temporaryBugFix(std::string exp) {
        enum State {
            start,
            skipNext,
            lookForEndBracket,
            foundEndBracket,
        };

        State state     = start;
        State prevState = start;

        int p = -1;
        std::vector<int> positionsToFix;

        for (auto c : exp) {
            ++p;

            switch (state) {
            case start:
                if (c == '\\') {
                    prevState = state;
                    state     = skipNext;
                } else if (c == '[')
                    state = lookForEndBracket;

                continue;

            case skipNext:
                state = prevState;
                continue;

            case lookForEndBracket:
                if (c == '\\') {
                    prevState = state;
                    state     = skipNext;
                } else if (c == ']') {
                    state = foundEndBracket;
                    if (p + 1 == exp.length()) {
                        positionsToFix.push_back(p + 1);
                    }
                }
                continue;

            case foundEndBracket:
                if (c != '+' && c != '*' && c != '?')
                    positionsToFix.push_back(p);
                state = start;
                continue;
            }
        }

        // check for valid curly brackets so we don't add an additional one
        std::string s = exp;
        std::smatch m;
        std::regex e("\\{\\d+,?\\d*?\\}");

        size_t offset = 0;
        std::vector<int> validCurlyBracketPositions;
        while (regex_search(s, m, e)) {
            validCurlyBracketPositions.push_back(m.position(0) + static_cast<int>(offset));
            offset += m.position(0) + m[0].length();
            s = m.suffix();
        }

        // remove valid curly bracket positions from the fix std::vector
        for (auto p : validCurlyBracketPositions) {
            positionsToFix.erase(std::remove(positionsToFix.begin(), positionsToFix.end(), p), positionsToFix.end());
        }

        // insert the fixes
        for (size_t i = positionsToFix.size(); i--;) {
            exp.insert(positionsToFix[i], "{1}");
        }

        return exp;
    }
};
