#include "RegexHelper.h"

const String RegexHelper::charsThatNeedEscape = "()[].*^$\\";

std::regex RegexHelper::exp(const String& expression, bool caseSensitive)
{	
	if (caseSensitive)
		return std::move(std::regex(expression.toStdString()));
	else
		return std::move(std::regex(temporaryBugFix(expression.toStdString()), std::regex_constants::icase));
}

bool RegexHelper::match(const String& input, const std::regex& expression)
{
	return regex_match(input.toStdString(), expression);
}

bool RegexHelper::match(const String& input, const String& expression, bool caseSensitive)
{
	return match(input, exp(expression, caseSensitive));
}

bool RegexHelper::search(const String& input, const std::regex& expression)
{
	return regex_search(input.toStdString(), expression);
}

bool RegexHelper::search(const String& input, const String& expression, bool caseSensitive)
{
	return search(input, exp(expression, caseSensitive));
}

String RegexHelper::remove(const String& input, const std::regex& expression)
{
	return regex_replace(input.toStdString(), expression, "");
}

String RegexHelper::remove(const String& input, const String& expression, bool caseSensitive)
{
	return remove(input, exp(expression, caseSensitive));
}

String RegexHelper::extract(const String& input, const std::regex& expression, const String& outExpr)
{
	auto flags = std::regex_constants::format_no_copy | std::regex_constants::format_first_only;

	return regex_replace(input.toStdString(), expression, outExpr.toStdString(), flags);
}

String RegexHelper::extract(const String& input, const String& expression, const String& outExpr, bool caseSensitive)
{
	return extract(input, exp(expression, caseSensitive), outExpr);
}

String RegexHelper::substitute(const String& input, const std::regex& expression, const String& outExpr)
{
	return regex_replace(input.toStdString(), expression, outExpr.toStdString());
}

String RegexHelper::substitute(const String& input, const String& expression, const String& outExpr, bool caseSensitive)
{
	return regex_replace(input.toStdString(), exp(expression, caseSensitive), outExpr.toStdString());
}

std::vector<String> RegexHelper::iterateAll(const String& input, const std::regex& expression)
{
	std::match_results<std::string::const_iterator> iterator;
	auto s = input.toStdString();
	regex_search(s, iterator, expression);

	vector<String> results;
	for (size_t i = 0; i < iterator.size(); ++i)
		results.push_back(std::move(iterator[i].str()));

	return results;
}

std::vector<String> RegexHelper::iterateAll(const String& input, const String& expression, bool caseSensitive)
{
	return iterateAll(input, exp(expression, caseSensitive));
}

std::vector<String> RegexHelper::iterateSub(const String& input, const std::regex& expression)
{
	auto s(input.toStdString());
	std::smatch m;
	std::vector<String> results;
	bool stop = false;

	while (std::regex_search(s, m, expression))
	{
		int i = 0;
		StringArray capture;
		for (auto x : m)
		{
			if (x.length() == 0)
				stop = true;

			if (i > 0)
				capture.add(std::string(x));

			++i;
		}
		results.push_back(capture.joinIntoString(""));
		s = m.suffix().str();

		if (stop)
			break;
	}

	return std::move(results);
}

std::vector<String> RegexHelper::iterateSub(const String& input, const String& expression, bool caseSensitive)
{
	return iterateSub(input, exp(expression, caseSensitive));
}

std::vector<String> RegexHelper::split(const String& input, const std::regex& expression)
{
	auto s = input.toStdString();
	std::vector<String> results;
	std::sregex_token_iterator iter(s.begin(), s.end(), expression, -1);
	std::sregex_token_iterator end;

	for (; iter != end; ++iter)
		if (iter->matched)
			results.push_back(String(*iter));

	return results;
}

std::vector<String> RegexHelper::split(const String& input, const String& expression, bool caseSensitive)
{
	return split(input, exp(expression, caseSensitive));
}

std::vector<String> RegexHelper::splitGreedy(const String& input, const std::regex& expression)
{
	auto s(input.toStdString());
	std::smatch m;
	std::vector<String> results;
	std::regex_search(s, m, expression);

	if (m.prefix().matched) // delimiter is post-fixed
	{
		std::sregex_token_iterator i(s.begin(), s.end(), expression, { -1, 0 });
		std::sregex_token_iterator j;
		while (i != j)
		{
			StringArray capture(String(*i++));

			if (i != j)
				capture.add(String(*i++));

			results.push_back(capture.joinIntoString(""));

		}
	}
	else // delimiter is pre-fixed
	{
		std::sregex_token_iterator i(s.begin(), s.end(), expression, { -1, 0 });
		std::sregex_token_iterator j;
		++i;
		while (i != j)
		{
			StringArray capture(String(*i++));

			if (i != j)
				capture.add(String(*i++));

			results.push_back(capture.joinIntoString(""));

		}
	}

	return results;
}

std::vector<String> RegexHelper::splitGreedy(const String& input, const String& expression, bool caseSensitive)
{
	return split(input, exp(expression, caseSensitive));
}
