#pragma once

#include "JuceHeader.h"

#include <map>

/*
A class for manipulating existing files with throw and no-throw functions.
This class is NOT for manipulating strings that are file-like, although
there are a few helper functions for manipulating strings as needed.

NOTE: There will not be an ending slash when retrieving folders/paths/directories


NOMENCLATURE
------------
Name = name of a folder or file
Path = full path to a file or folder
Dir  = full path to folder
Ext  = extension short hand

File     = file full path
FilePath = file full path
FileName = file name
FileDir  = path to file without its name

Folder     = folder full path
FolderPath = folder full path
FolderName = folder name
FolderDir  = path to folder without its name
*/

// Helper class for file functions where string or file are both valid inputs to a parameter.
class StringOrFile {
  public:
    StringOrFile(juce::String s);
    StringOrFile(juce::File f);
    StringOrFile(juce::StringRef s);

    operator juce::File();
    operator juce::String();
    operator juce::StringRef();

    juce::String getFullPathName() const;
    juce::String getFileNameWithoutExtension() const;

    juce::String s;
    juce::File f;
};

class FileHelper {
  public:
    FileHelper()  = default;
    ~FileHelper() = default;

    enum class Error {
        empty_string,
        path_is_relative,
        illegal_char,
        path_is_dir,
        begins_with_space,
        space_before_ext,
        ends_with_space,
        ends_with_dot,
        ends_or_begins_with_space,
        file_already_exists,
        folder_already_exists,
        file_already_exists_as_folder,
        directory_with_multi_slash,
        directory_already_exists_as_file,
        path_does_not_exist,
        file_does_not_exist,
        directory_does_not_exist,
        unknown_error_create_dir,
        unknown_error_create_file,
        unknown_error_file_move_or_rename,
        unknown_error_folder_move_or_rename,
        unknown_error_overwrite_file,
        unknown_error_file_copy,
        unknown_error_modify_file,
        unknown_error_file_trash,
        unknown_error_file_delete
    };

    static std::map<FileHelper::Error, juce::String> errorMessages;

    static bool isIllegalNameChar(const juce::juce_wchar& c);
    static bool isIllegalDirChar(const juce::juce_wchar& c);

    /* NO THROW FILE CHECKS */

    // True if path exists as a file. Does not throw exception.
    static bool doesFileExist(const juce::String& file);
    // True if path exists as a folder. Does not throw exception.
    static bool doesFolderExist(const juce::String& directory);
    // True if path exists as a file or folder. Does not throw exception.
    static bool doesPathExist(const juce::String& path);
    // Asks if path contains files. Does not throw exception. Returns false if directory does not exist, no files, or only folders are found.
    static bool doesDirContainFiles(const juce::File& directory, juce::String wildCard = "*");
    // Asks if path contains files. Does not throw exception. Returns false if directory does not exist, no folders, or only files are found.
    static bool doesDirContainFolders(const juce::File& directory, juce::String wildCard = "*");
    // Checks if given path is an absolute/full path. Does not throw.
    static bool isAbsolutePath(const juce::String& path);
    // Checks if given path is a relative/partial path. Does not throw.
    static bool isRelativePath(const juce::String& path);
    // Checks if given path is a file. Does not throw.
    static bool isFile(const juce::String& path);
    // Checks if given path is a folder. Does not throw.
    static bool isFolder(const juce::String& path);
    // Returns 0 if not valid. Returns 1 if path is a file path, 2 if a folder path. If path does not exist, assumes file if has extension, otherwise assumes folder.
    static int isFileOrFolder(const juce::String& path);

    /* NO THROW CHECK FOR VALID PATHS */

    // Asks if string is a path with a valid file name, does not throw exception.
    static bool isValidFilePath(StringOrFile file);
    // Asks if file is a valid file name without a path, does not throw exception.
    static bool isValidFileName(StringOrFile file);
    // Asks if file is a valid folder name without a path, does not throw exception.
    static bool isValidFolderName(StringOrFile folderName);
    // Asks if file is a path to a folder or file, does not throw exception.
    static bool isValidPath(StringOrFile file);
    // Asks if file is a folder path, does not throw exception.
    static bool isValidFolderPath(StringOrFile dir);

    /* VALID PATH CHECKS THAT THROW */

    // Throws exception if path is not a file path or badly formatted.
    static void ensureValidFilePath(const juce::String& path);
    // Path may be a file or folder. Throws exception if badly formatted.
    static void ensureValidPath(const juce::String& path);
    // Path must be a folder. Throws exception if path is badly formatted.
    static void ensureValidDir(const juce::String& directory);
    // Throws exception is badly formatted file name
    static void ensureValidFileName(const juce::String& fileName);
    // Throws exception is badly formatted folder name
    static void ensureValidFolderName(const juce::String& folderName);
    // Throws exception if badly formatted path or path does not exist.
    static void ensurePathExists(const juce::File& f);
    // Throws exception if badly formatted file path or file does not exist.
    static void ensureFileExists(const juce::File& f);
    // Throws excpetion if file cannot be be modified
    static bool ensureFileNotExist(const juce::File& f);
    // Throws exception if badly formatted directory or directory does not exist.
    static void ensureFolderExists(const juce::File& f);

    /* RENAME, MOVE, PATH INFO, ASSUME PATH */

    // Throws exception if file is invalid or if file is a folder or fails to modify file
    static juce::File setFileDir(const juce::File& file, StringOrFile fileDir, bool overwrite);
    // Affects the file name and its dot and extension. Throws exception if file is invalid or if file is a folder or fails to modify file
    static juce::File setFileName(const juce::File& file, StringOrFile fileName, bool overwrite);
    // Throws exception if folderName is invalid or fails to modify file's folder
    static juce::File setFolderName(const juce::File& folder, const juce::String& newName, bool overwrite);
    // Returns name of folder or file with dot and extension
    static juce::String getName(const juce::File& file);
    // Returns name of folder or file without dot and extension
    static juce::String getNameNoExt(const StringOrFile& file);
    // Returns file or folder after renaming
    static juce::File setName(const juce::File& path, const juce::String& name, bool overwrite);

    // Throws exception if fails to modify file
    static juce::File setFilePath(const juce::File& file, const juce::String& filePath, bool overwrite);
    // Throws exception if fails to modify file or folder
    static juce::File setPath(const juce::File& file, const juce::String& filePath, bool overwrite);

    // Replaces illegal charactres with '_' and removes duplicate slashes
    static juce::String createLegalString(const juce::String& filePathOrName);

    // Returns true of path has a file extension.
    static bool hasExt(const juce::String& filePath);
    // Returns the extension part of file with dot.
    static juce::String getExt(const juce::String& file);
    // Adds or changes the extension for file string and returns the new string.
    static juce::String setExt(const juce::String& file, juce::StringRef ext);
    // Return string for file name without an extension. Does not throw.
    static juce::String removeExt(StringOrFile fileName);

    // If file does not have an extension, set to given extension. Does not throw.
    static juce::String assumeExt(juce::StringRef file, juce::StringRef ext);
    // If file string is a relative path, a base folder is added to the returned string. Does not throw.
    static juce::String assumeFolder(juce::StringRef file, juce::StringRef folder);
    // Adds a base folder to a relative path and extension to string without an extension. Does not throw.
    static juce::String assume(juce::StringRef file, juce::StringRef folder, juce::StringRef ext);

    // Return string for file name with added prefix before the extension. Does not throw.
    static juce::String addSuffix(StringOrFile fileName, const juce::String& suffix);
    // Extracts the folder path from given file or folder. Does not throw.
    static juce::String getFolder(const juce::String& f) {
        return getParent(0, f);
    }
    // Extracts the folder path from given file or folder. Does not throw.
    static juce::String getDir(const juce::File& f);
    // Converts file to a string path. Does not throw.
    static juce::String toString(const juce::File& f);
    // Returns file parts: drive letter without punctuation, directory without drive letter, file name without extension, file extension without dot, via 3 output parameters.
    static void getFileParts(const juce::String& f, juce::String& driveLetterOut, juce::String& fileDirOut, juce::String& fileNameNoExtOut, juce::String& fileExtOut);
    // Returns file parts: directory, file name without extension, file extension without dot, via 3 output parameters.
    static void getFileParts(const juce::File& f, juce::String& fileDirOut, juce::String& fileNameNoExtOut, juce::String& fileExtOut);
    // Returns file parts: directory and file name with dot and extension via 2 output parameters.
    static void getFileParts(const juce::File& f, juce::String& fileDirOut, juce::String& fileNameOut);

    /* OPERATING SYSTEM OPEN or REVEAL */

    // Opens folder. Throws exception if badly formatted path, error creating folder, or folder does not exist.
    static void openFolder(const juce::String& directory, bool createIfNeeded);
    // Opens or runs file. Throws exception if badly formatted path, error creating file, or file does not exist.
    static void openFile(const juce::String& path, bool createIfNeeded);
    // Opens the folder that contains given folder. Throws exception if badly formatted path, error creating folder, or folder does not exist.
    static void revealFolder(const juce::String& path, bool createIfNeeded);
    // Opens the folder that contains file. Throws exception if badly formatted path, error creating folder, or file does not exist.
    static void revealFile(const juce::String& path, bool createIfNeeded);

    /* GET PATHS, READ, CREATE, DELETE FILE */

    /**
     * 1. Creates any parent directories.
     * 2. Throw if the file path appears to be invalid in some way
     * 3. Throw if file exists and overwrite is not true
     * 4. Throw if the file cannot be created
     * 5. Creates the file.
     */
    static juce::File createFile(const juce::String& filePath, bool overwrite);
    // Creates a file in operating system's temporary folder and returns the file.
    static juce::File createTempFile();
    // Creates text file. Throws exception for badly formatted path or error creating file.
    static void createTextFile(const juce::String& filePath, const juce::String& text, bool overwrite);
    // Returns a string for file. Throws exception if file does not exist
    static juce::String readFile(const juce::String& filePath);
    // Creates folder for path. Throws exception if badly formatted path or error creating folder.
    static void createFolder(const juce::String& fileDir);
    // Copies file to new path. Throws exception for badly formatted path or error creating file.
    static juce::File copyFile(const juce::File& origFile, const juce::String& filePath, bool overwrite);
    // Moves file or folder to trash. Throws exception on failure.
    static void moveToTrash(const juce::File& f);
    // Deletes a file or folder permanently. Throws exception if path is invalid or deletion failure.
    static void permanentDelete(const juce::File& f);
    // Recursively moves to trash folders containing folders with no files and removes itself if empty. Returns a list of removed folders.
    static vector<juce::String> removeEmptyFoldersRecursive(const juce::File& folder);
    // Recursively moves to trash files that are zero size. Throws exception if invalid path or failure to trash folders.
    static vector<juce::String> removeEmptyFilesRecursive(const juce::File& folder);
    // Throws for invalid path. Gets list of folders immediately inside a directory. If given path is a file, function will use its directory.
    static vector<juce::String> getFolders(const juce::File& f, const juce::String& wildCard = "*");
    // Throws for invalid path. Gets list of files immediately inside a directory. If given path is a file, function will use its directory.
    static vector<juce::String> getFiles(const juce::File& f, const juce::String& wildCard = "*");
    // Throws for invalid path. Gets list of folders and subfolders inside a directory. If given path is a file, function will use its directory.
    static vector<juce::String> getFoldersRecursive(const juce::File& f, const juce::String& wildCard = "*");
    // Throws for invalid path. Gets list of files in given directory and subfolders. If given path is a file, function will use its directory.
    static vector<juce::String> getFilesRecursive(const juce::File& f, const juce::String& wildCard = "*");

    /* DATE, TIME, AND SLASH CONVERSION */

    static juce::String getErrorString();

    // Get parent or 'up' or 'back' folders based on levels "up". Levels are clamped up to the source drive. NOTE: Removes the file from the path
    static juce::String getParent(int levels, StringOrFile path);
    // Gets the parent directory for folder or file.
    static juce::String getParent(StringOrFile path) {
        getParent(1, path);
    }

    // Convert all slashes to forward slashes.
    static juce::String toUniversalSlash(StringOrFile path);

    // Convert all slashes to system native slash.
    static juce::String toSystemSlash(StringOrFile path);

    // format is Year-Month-Day e.g. 2001-12-01
    static juce::String getDateStr(juce::String separator = "-");
    // format is HourMinute e.g. 0105
    static juce::String getTimeStr();

    // 2001-01-01_0101 -> YearMonth-Day HourMinute, separatorA is between date numbers, separatorB is between date and time.
    static juce::String getDateAndTimeStr(juce::String separatorA = "-", juce::String separatorB = "_");

    /* FILE COMPARISONS */

    // Adds (2) suffix to filename before extension, with number depending on what final name is available to avoid overwriting another file.
    static juce::String getUnusedFilePath(juce::String filePath, bool replaceIllegalChars);

    static bool isDataEqual(const juce::String& path1, const juce::String& path2) {
        return FileHelper::readFile(path1) == FileHelper::readFile(path2);
    }

  protected:
    static void throwError(const juce::String& path, FileHelper::Error v);

    static bool isSlashChar(const juce::juce_wchar& c);
};
