#include "DecimalCompare.h"

bool isEqual(double a, double b, double accuracy)
{
	return abs(a - b) <= accuracy;
}

bool isNotEqual(double a, double b, double accuracy)
{
	return abs(a - b) > accuracy;
}
bool isLessThan(double a, double b, double accuracy)
{
	return isNotEqual(a, b, accuracy) && a < b;
}
bool isMoreThan(double a, double b, double accuracy)
{
	return isNotEqual(a, b, accuracy) && a > b;
}
bool isLessThanOrEqual(double a, double b, double accuracy)
{
	return isEqual(a, b, accuracy) || a <= b;
}
bool isMoreThanOrEqual(double a, double b, double accuracy)
{
	return isEqual(a, b, accuracy) || a >= b;
}

bool isTouchingRange(double time, double start, double end, double accuracy)
{
	return isLessThanOrEqual(time, end, accuracy) && isMoreThanOrEqual(time, start, accuracy);
}

bool isInsideRange(double time, double start, double end, double accuracy)
{
	return isLessThan(time, end, accuracy) && isMoreThan(time, start, accuracy);
}
