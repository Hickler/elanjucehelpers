#pragma once

template<typename Type> class GetSetValue
{
public:
	GetSetValue(std::function<void()> updateFunc) : updateFunc(updateFunc)

    template <typename OtherType> bool operator== (const OtherType& other) const   { return cachedValue == other; }

    template <typename OtherType> bool operator!= (const OtherType& other) const   { return cachedValue != other; }

    void set(Type v)
	{
		value = v;
		updateFunc();
	}

	Type get()
	{
		return value;
	}


protected:

	Type value;

	std::function<void()> updateFunc = nullptr;
};