#include "BatchFileHelper.h"

BatchFileHelper::BatchFileHelper(const vector<String>& filePathStrings)
{
	newFileList = filePathStrings;

	collectFiles();
}

vector<String> BatchFileHelper::getFilesToBeCreated()
{
	std::map<size_t, String> iSM;

	for (auto& folder : folderFileMap)
		for (auto& obj : folder.second)
			for (auto& fND : obj.second)
				if (fND.originalFile == File())
					iSM[fND.idx] = fND.newPath;

	vector<String> sA;

	for (size_t i = 0; i < iSM.size(); ++i)
		sA.push_back(iSM[i]);

	return std::move(sA);
}

bool BatchFileHelper::createNewFiles()
{
	for (auto& folder : folderFileMap)
	{
		for (auto& obj : folder.second)
		{
			for (auto& fND : obj.second)
			{
				if (fND.originalFile != File()) // only creating listed files and not renaming existing files
					continue;

				try
				{
					FileHelper::createFile(fND.newPath, false);
					fileNameChanges.push_back({ File(), fND.newPath, true });
				}
				catch (std::exception&)
				{
					attemptToUndoFileRenames();
					return false;
				}
			}
		}
	}

	return true;
}

bool BatchFileHelper::createNewFilesAndAddDupeSuffixToExistingFiles(bool doNotModifyNonDuplicates)
{
	addDuplicateSuffixToCollection(doNotModifyNonDuplicates);

	renameExistingFiles(); // rename existing files in preparation to create new files

	for (auto& folder : folderFileMap)
	{
		for (auto& obj : folder.second)
		{
			for (auto& fND : obj.second)
			{
				if (fND.originalFile == File()) // only create non-existent files here
				{
					try
					{
						FileHelper::createFile(fND.newPath, false);
						fileNameChanges.push_back({ {}, fND.newPath, true });
					}
					catch (std::exception&)
					{
						attemptToUndoFileRenames();
						return false;
					}
				}
			}
		}
	}

	return true;
}

void BatchFileHelper::clearDupeNumbers()
{
	for (auto& folder : folderFileMap)
		for (auto& obj : folder.second)
			for (auto& fND : obj.second)
				fND.clearDupeNumber();
}

vector<int> BatchFileHelper::getUnusedDupeNumbers(vector<File>& files)
{
	int numberToCheck = 1;

	vector<int> ret;

	for (auto& f : files)
	{
		int openParenIndex;
		int closeParenIndex;
		int dupeNumber;

		if (fileNameDupeStruct::hasDupeSuffix(f.getFileName(), openParenIndex, closeParenIndex, dupeNumber))
		{
			while (numberToCheck == dupeNumber)
				++numberToCheck;
		}

		ret.push_back(numberToCheck);
	}

	return std::move(ret);
}

String BatchFileHelper::printRenameList(String seperator, bool printOnlyIfRenaming, bool printDuplicatesOnly)
{
	vector<String> sA;

	int arrayIndex = 0;

	for (auto& folder : folderFileMap)
	{
		for (auto& obj : folder.second)
		{
			if (!printDuplicatesOnly || obj.second.size() > 1)
			{
				for (auto& fND : obj.second)
					if (!printOnlyIfRenaming || fND.originalName == fND.newName)
						sA.push_back(fND.originalName + seperator + fND.newName + "\n");

				sA.push_back("\n\n\n");

				++arrayIndex;
			}
		}
	}

	return Convert::toString(sA);
}

String BatchFileHelper::printFileList()
{
	vector<String> sA;

	int arrayIndex = 0;

	for (auto& folder : folderFileMap)
	{
		for (auto& obj : folder.second)
		{
			for (auto& fND : obj.second)
				if (fND.originalFile == File())
					sA.push_back(fND.newName + "\n");

			sA.push_back("\n\n\n");

			++arrayIndex;
		}
	}

	return Convert::toString(sA);
}

bool BatchFileHelper::checkForDuplicates(AccessMethod method) {
    if (method == originalNameWithoutSuffix) {
        for (auto& folder : folderFileMap) {
            for (auto& obj : folder.second) {
                return obj.second.size() > 1;
            }
        }
    } else {
        std::map<String, std::map<String, vector<fileNameDupeStruct>>> tempFileMap;
        collectFiles(newFileList, tempFileMap, method);

        for (auto& folder : folderFileMap) {
            for (auto& obj : folder.second){
                return obj.second.size() > 1;
            }
        }
    }
    return {};
}

bool BatchFileHelper::fileListContainsIllegalChars(AccessMethod method) {
    for (auto& folder : folderFileMap) {
        if (FileHelper::isValidFolderPath(folder.first)) {
            return false;
        }

        for (auto& obj : folder.second) {
            for (auto& fND : obj.second) {
                String pathToCheck;

                switch (method) {
                case originalNameWithoutSuffix:
                    pathToCheck = fND.originalNameNoSuffix;
                    break;
                case originalNameWithSuffix:
                    pathToCheck = fND.originalName;
                    break;
                case newNameWithoutSuffix:
                    pathToCheck = fileNameDupeStruct::removeDupeSuffix(FileHelper::removeExt(fND.newName)) + fND.ext;
                    break;
                case newNameWithSuffix:
                    pathToCheck = fND.newName;
                    break;
                }

                if (!FileHelper::isValidFilePath(pathToCheck)) {
                    return false;
                }
            }
        }
    }
    return false;
}

void BatchFileHelper::solveIllegalCharacterIssues()
{
	successfulUndoActions = 0;
	fileNameChanges.clear();

	for (auto& s : newFileList)
		s = FileHelper::createLegalString(s);

	collectFiles();
}

void BatchFileHelper::addDuplicateSuffixToCollection(bool doNotModifyNonDuplicates)
{
	for (auto& folder : folderFileMap)
	{
		for (auto& obj : folder.second)
		{
			if (obj.second.size() == 1) // no duplicate found, remove duplicate prefix if desired
			{
				obj.second[0].newName = doNotModifyNonDuplicates ? obj.second[0].originalName : obj.second[0].originalNameNoSuffix;
				continue;
			}

			vector<int> usedDupeNumbers;

			for (auto& fND : obj.second)
				if (fND.dupeNumber != -1)
					VectorHelper::pushUnique(usedDupeNumbers, fND.dupeNumber);

			int i = 1;
			for (auto& fND : obj.second)
			{
				if (fND.originalFile == File() || !fND.hasDupeNumber())
				{
					while (VectorHelper::indexOf(usedDupeNumbers, i) != -1)
						++i;

					fND.newName = fND.originalNameNoSuffixNoExt + "(" + String(i) + ")" + fND.ext;
					fND.newPath = fND.dir + "/" + fND.newName;
					++i;
				}
				else
				{
					fND.newName = fND.originalNameNoSuffixNoExt + "(" + String(fND.dupeNumber) + ")" + fND.ext;
					fND.newPath = fND.dir + "/" + fND.newName;
				}
			}
		}
	}

	refreshFileFolderMap();
}

bool BatchFileHelper::trashListedExistingFiles(AccessMethod method)
{
	vector<String> foldersToDelete;

	for (auto& folder : folderFileMap)
	{
		String tempFolderForDeletion = folder.first + "/saltTempDelete";

		for (auto& obj : folder.second)
		{
			for (auto& fND : obj.second)
			{
				if (fND.originalFile != File()) // we are only deleting files specified at object creation
					continue;

				String pathToDelete;

				switch (method)
				{
				case originalNameWithoutSuffix:
					pathToDelete = folder.first + "/" + fND.originalNameNoSuffix;
					break;
				case originalNameWithSuffix:
					pathToDelete = folder.first + "/" + fND.originalName;
					break;
				case newNameWithoutSuffix:
					pathToDelete = folder.first + "/" + fileNameDupeStruct::removeDupeSuffix(FileHelper::removeExt(fND.newName)) + fND.ext;
					break;
				case newNameWithSuffix:
					pathToDelete = folder.first + "/" + fND.newName;
					break;
				}

				File f(pathToDelete);

				if (FileHelper::doesFileExist(pathToDelete))
				{
					try
					{
						auto fileMoved = FileHelper::setFileDir(f, tempFolderForDeletion, false);
						fileNameChanges.push_back({ pathToDelete, fileMoved.getFullPathName() });
					}
					catch (std::exception&)
					{
						attemptToUndoFileRenames();
						return false;
					}
				}
			}
		}

		if (File(tempFolderForDeletion).getNumberOfChildFiles(File::TypesOfFileToFind::findFiles) != 0)
			foldersToDelete.push_back(tempFolderForDeletion);
	}

	for (const auto& s : foldersToDelete)
		if (!File(s).moveToTrash())
			return false;

	fileNameChanges.clear();
	newFileList.clear();
	successfulUndoActions = 0;

	return true;
}

bool BatchFileHelper::undoFileChanges()
{
	return attemptToUndoFileRenames();
}

void BatchFileHelper::collectFiles()
{
	folderFileMap.clear();
	fileNameChanges.clear();
	successfulUndoActions = 0;

	collectFiles(newFileList, folderFileMap, originalNameWithoutSuffix);
}

void BatchFileHelper::collectFiles(const vector<String>& /*newFiles*/, std::map<String, std::map<String, vector<fileNameDupeStruct>>>& fileMapInput, AccessMethod methodForDuplicateCheck)
{
	int i;

	// Populate folderFileMap with new files to be created
	i = 0;
	for (const auto& s : newFileList)
	{
		auto folder = FileHelper::getDir(s);
		auto name = FileHelper::getName(s);
		fileMapInput[folder][name].push_back(fileNameDupeStruct(s, i));
		++i;
	}

	// Go through each folder of corresponding new files to be created and populate with existing files
	i = 0;
	for (auto& folder : fileMapInput)
	{
		vector<String> files = FileHelper::getFiles(folder.first);

		for (auto& f : files)
		{
			fileNameDupeStruct fND(f, i);
			String path;
			switch (methodForDuplicateCheck)
			{
			case originalNameWithoutSuffix:
				path = fND.originalNameNoSuffix;
				break;
			case originalNameWithSuffix:
				path = fND.originalName;
				break;
			case newNameWithoutSuffix:
				path = fND.newName;
				break;
			case newNameWithSuffix:
				path = fileNameDupeStruct::removeDupeSuffix(FileHelper::removeExt(fND.newName)) + fND.ext;
				break;
			}

			folder.second[path].push_back(fND);
			++i;
		}
	}
}

void BatchFileHelper::refreshFileFolderMap()
{
	std::map<String, std::map<String, vector<fileNameDupeStruct>>> tempFileFolderMap;

	for (auto& folder : folderFileMap)
	{
		for (auto& obj : folder.second)
		{
			for (auto& fND : obj.second)
			{
				String dir, name, ext;
				FileHelper::getFileParts(fND.newPath, dir, name, ext);
				tempFileFolderMap[dir][name].push_back(fND);
			}
		}
	}

	folderFileMap = std::move(tempFileFolderMap);
}

void BatchFileHelper::swapFileNames(File& file1, File& file2, vector<fileNameChangesStruct>& changes)
{
	String initialName = FileHelper::getName(file2);

	String s;
	s = initialName + "_" + generateRandom4CharString();
	FileHelper::setFileName(file2, s, false);
	changes.push_back({ file2, s });

	s = initialName;
	FileHelper::setFileName(file1, s, false);
	changes.push_back({ file1, s });

	s = FileHelper::getName(file1);
	FileHelper::setFileName(file2, s, false);
	changes.push_back({ file2, s });
}

String BatchFileHelper::generateRandom4CharString()
{
	vector<String> ret;
	ret.push_back((String)char(random.nextFloat() * 26 + 41));
	ret.push_back((String)char(random.nextFloat() * 26 + 41));
	ret.push_back((String)char(random.nextFloat() * 26 + 41));
	ret.push_back((String)char(random.nextFloat() * 26 + 41));
	return Convert::toString(ret);
}

bool BatchFileHelper::attemptToUndoFileRenames()
{
	for (size_t i = fileNameChanges.size() - successfulUndoActions; i-- > 0;)
	{
		// attempt to undo name changes
		try
		{
			if (fileNameChanges[i].fileWasCreated)
			{
				if (!File(fileNameChanges[i].newfile).deleteFile())
					return false;

				++successfulUndoActions;
			}
			if (fileNameChanges[i].newfile.existsAsFile())
			{
				FileHelper::setFileName(fileNameChanges[i].newfile, fileNameChanges[i].oldFile, false);

				++successfulUndoActions;
			}
			else
			{
				return false;
			}
		}
		catch (std::exception&)
		{
			return false;
		}
	}
	return true;
}

bool BatchFileHelper::renameExistingFiles()
{
	for (auto& folder : folderFileMap)
	{
		for (auto& obj : folder.second)
		{
			for (auto& fileObj : obj.second)
			{
				if (fileObj.originalFile == File()) // file does not yet exist so do not attempt to rename
					continue;

				// attempt to rename files
				try
				{
					String newFilePath = fileObj.dir + "/" + fileObj.newName;

					if (FileHelper::doesFileExist(newFilePath))
					{
						File newfilePathFile(newFilePath);
						swapFileNames(fileObj.originalFile, newfilePathFile, fileNameChanges);
					}
					else
					{
						FileHelper::setFileName(fileObj.originalFile, fileObj.newName, false);
						fileObj.newPath = newFilePath;
						fileNameChanges.push_back({ fileObj.originalFile, newFilePath });
					}
				}
				catch (std::exception&)
				{
					attemptToUndoFileRenames();
					return false;
				}
			}
		}
	}

	return true;
}

BatchFileHelper::fileNameDupeStruct::fileNameDupeStruct() {}

BatchFileHelper::fileNameDupeStruct::fileNameDupeStruct(const File& s, int addIndex)
{
	jassert(s.existsAsFile()); // file must exist or use string version of this function

	File f(s);
	originalFile = s;

	FileHelper::getFileParts(f, dir, originalNameNoSuffixNoExt, ext);
	originalNameNoSuffixNoExt = removeDupeSuffix(originalNameNoSuffixNoExt, &dupeNumber);
	originalNameNoSuffix = originalNameNoSuffixNoExt + ext;
	originalName = FileHelper::getName(f);
	newName = originalName;
	newPath = dir + "/" + newName;
	idx = addIndex;
}

BatchFileHelper::fileNameDupeStruct::fileNameDupeStruct(const String& s, int addIndex)
{
	File f(s);
	FileHelper::getFileParts(f, dir, originalNameNoSuffixNoExt, ext);
	originalNameNoSuffixNoExt = removeDupeSuffix(FileHelper::removeExt(originalNameNoSuffixNoExt), &dupeNumber);
	originalNameNoSuffix = originalNameNoSuffixNoExt + ext;
	originalName = FileHelper::getName(f);
	newName = originalName;
	newPath = dir + "/" + newName;
	idx = addIndex;
}

bool BatchFileHelper::fileNameDupeStruct::hasDupeNumber()
{
	return dupeNumber != -1;
}

void BatchFileHelper::fileNameDupeStruct::clearDupeNumber()
{
	dupeNumber = -1;
}

void BatchFileHelper::fileNameDupeStruct::updateNewPath(const String& s)
{
	dir = FileHelper::getDir(s);
	newName = FileHelper::getName(s);
	ext = FileHelper::getExt(s);
	newPath = dir + "/" + newName;
}

bool BatchFileHelper::fileNameDupeStruct::hasDupeSuffix(const String& s, int& indexOfOpenParenOut, int& indexOfCloseParenOut, int& dupeNumberOut)
{
	indexOfOpenParenOut = s.lastIndexOf("(");
	indexOfCloseParenOut = s.lastIndexOf(")");

	String dupeNumberStr = s.substring(indexOfOpenParenOut + 1, indexOfCloseParenOut - 1);

	if (STR::isInt(dupeNumberStr))
		dupeNumberOut = dupeNumberStr.getIntValue();
	else
		dupeNumberOut = -1;

	return dupeNumberOut != -1 && !(indexOfOpenParenOut == -1 || indexOfCloseParenOut == -1);
}

String BatchFileHelper::fileNameDupeStruct::removeDupeSuffix(const String& fileNameNoExt, int* dupeNumberOut)
{
	int indexOfOpenParen;
	int indexOfCloseParen;
	int dupeNumber;

	bool hasDupe = hasDupeSuffix(fileNameNoExt, indexOfOpenParen, indexOfCloseParen, dupeNumber);

	if (dupeNumberOut != nullptr)
		*dupeNumberOut = dupeNumber;

	if (hasDupe)
		return fileNameNoExt.substring(0, indexOfOpenParen - 1);

	return fileNameNoExt;
}

String BatchFileHelper::fileNameDupeStruct::addDupeSuffix(const String& s, int dupeNumberToUse)
{
	int indexOfOpenParen;
	int indexOfCloseParen;
	int dupeNumber;

	if (hasDupeSuffix(s, indexOfOpenParen, indexOfCloseParen, dupeNumber))
		return s;

	return FileHelper::removeExt(s) + "(" + String(dupeNumberToUse) + ")" + FileHelper::getExt(s);
}

BatchFileChanger::Task* BatchFileChanger::addTask(String originalPath, String newPath, Operation operation)
{
	if (runTasksWasCalled)
		throw std::invalid_argument("In order to protect file integrity, tasks cannot be added once tasks have been run. Create a new object in order to make more tasks.");	

	if (operation == Operation::none)
	{
		jassertfalse;
		return nullptr;
	}

	didCheckForConflicts = false;

	TASKS.push_back(std::make_unique<Task>());
	auto& t = *TASKS.back();
	t.operation = operation;
	t.origFile  = originalPath;
	t.userInput = newPath;
	t.random    = generateRandom7CharString();

	switch (operation)
	{
	case invalid:
	case none:
		break;
	case Operation::move:
		if (FileHelper::isValidFolderPath(newPath)) // if folder path
			t.newFile = newPath + "/" + File(originalPath).getFileName();
		else if (FileHelper::isValidFilePath(newPath))
			t.newFile = newPath;
		else
			throw std::invalid_argument("MOVE task ->: " + newPath.toStdString() + " Path is not a valid folder path or file path.");
		break;
	case Operation::rename:
		if (FileHelper::isValidFilePath(newPath))
			t.newFile = newPath;
		else if (FileHelper::isValidFileName(newPath))
			t.newFile = File(originalPath).getParentDirectory().getChildFile(newPath).getFullPathName();
		else
			throw std::invalid_argument("RENAME task ->: " + newPath.toStdString() + " Path is not a valid file path or file name.");
		break;
	case Operation::copy:
		if (newPath.isEmpty())
			t.newFile = t.origFile;
		else if (FileHelper::isValidFolderPath(newPath))
			t.newFile = newPath + "/" + FileHelper::getName(t.origFile);
		else if (FileHelper::isValidFilePath(newPath))
			t.newFile = newPath;
		else
			throw std::invalid_argument("COPY task: " + newPath.toStdString() + " Path is not a valid folder path or file path.");
		break;
	case Operation::temporary:
	{
		if (!FileHelper::isValidFolderPath(newPath))
			throw std::invalid_argument("TEMPORARY task: " + newPath.toStdString() + " Path is not a valid folder path.");
#if JUCE_WINDOWS
		// deal with drive letter
		String driveLetter = File(originalPath).getVolumeLabel();

		if (driveLetter.isNotEmpty())
			t.newFile = newPath + "/" + driveLetter + "/" + originalPath.substring(2);
#else
		t.newFile = newPath + "/" + originalPath;
#endif
		break;
	}
	}

	t.newFile = FileHelper::toUniversalSlash(File(t.newFile).getFullPathName());
	t.origFile = FileHelper::toUniversalSlash(File(t.origFile).getFullPathName());

	if (taskMapOriginal[FileHelper::getDir(t.origFile)][t.origFile].size() == 0)
		taskMapOriginal[FileHelper::getDir(t.origFile)][t.origFile].push_back(&t);
	else
		throw std::invalid_argument("There cannot be more than one task for a file.");

	FileHelper::ensureValidFilePath(t.origFile);
	FileHelper::ensureValidFilePath(t.newFile);

	taskMapNew[FileHelper::getDir(t.newFile)][t.newFile].push_back(&t);

	return &*TASKS.back();
}

void BatchFileChanger::setDuplicateFormat(int startingNumber, String regexMatch, String tagScriptReplace, int padFlag, int orderFlag, bool onlyRenameDuplicates)
{
	duplicateFormat.startingNumber = startingNumber;
	duplicateFormat.tagScriptReplace = tagScriptReplace;
	duplicateFormat.padFlag = padFlag;
	duplicateFormat.orderFlag = orderFlag;
	duplicateFormat.onlyRenameDuplicates = onlyRenameDuplicates;

	if (!tagScriptReplace.contains("<num>"))
		throw std::invalid_argument("tagScriptReplace parameter must contain '<num>' in order to place duplicate number.");

	try
	{
		duplicateFormat.regexMatch = std::regex(regexMatch.toStdString(), std::regex_constants::icase);
	}
	catch (exception & e)
	{
		throw std::invalid_argument("Regex expression failed: " + string(e.what()));
	}
}

void BatchFileChanger::applyDuplicateFormat()
{
	map<String, map<String, vector<Task*>>> taskMapNewReplacement;

	if (runTasksWasCalled)
		throw std::invalid_argument("In order to protect file integrity, editing tasks is not allowed after tasks have been run. Create a new object in order to make more tasks.");

	for (auto& directory : taskMapNew)
	{
		for (auto& fullpath : directory.second)
		{
			size_t size = fullpath.second.size();
			int padlength = numDigitsInInteger(static_cast<int>(size));

			if (size > 0 || !duplicateFormat.onlyRenameDuplicates)
			{
				vector<int> numbers(size);
				std::iota(numbers.begin(), numbers.end(), 0);
				switch (duplicateFormat.orderFlag)
				{
				case Order::forward:
					break;
				case Order::reverse:
					std::reverse(numbers.begin(), numbers.end());
				case Order::random:
					VectorHelper::shuffle(numbers);
					break;
				}

				//int dupeNumber = duplicateFormat.startingNumber;
				for (int i = 0; i < size; ++i)
				{
					auto& t = *fullpath.second[numbers[i]];

					int dupNumber = i + duplicateFormat.startingNumber;
					String dupNumberStr(dupNumber);
					if (duplicateFormat.padFlag == Padding::automatic)
						dupNumberStr = dupNumberStr.paddedLeft('0', padlength);
					else if (duplicateFormat.padFlag >= 2)
						dupNumberStr = dupNumberStr.paddedLeft('0', duplicateFormat.padFlag);

					Tagger tagger;
					tagger.setTag("num", dupNumberStr);
					String dir, name;
					FileHelper::getFileParts(t.newFile, dir, name);
					String newName = tagger.fromTagScript(RegexHelper::substitute(name, duplicateFormat.regexMatch, duplicateFormat.tagScriptReplace));			

					try
					{
						FileHelper::ensureValidFileName(newName);
						FileHelper::ensureValidFilePath(dir + "/" + newName);
					}
					catch (std::exception & e)
					{
						throw std::invalid_argument("Bad path after appyling duplicate formatting. Check format parameters -> " + string(e.what()));
					}
					
					t.newFileWithDupID = dir + "/" + newName;
					t.dupNumber = dupNumber;
					taskMapNewReplacement[FileHelper::getDir(t.getNewPathStr())][t.getNewPathStr()].push_back(&t);
				}
			}
			else
			{
				for (int i = 0; i < size; ++i)
				{
					auto& t = *fullpath.second[i];
					taskMapNewReplacement[FileHelper::getDir(t.getNewPathStr())][t.getNewPathStr()].push_back(&t);
					t.dupNumber = duplicateFormat.startingNumber;
				}
			}
		}
	}

	taskMapNew = std::move(taskMapNewReplacement);
}

String BatchFileChanger::checkForConflicts()
{
	vector<String> ret;
	ret.push_back("ErrorType\tOperation\tOriginalFile\tNewFile\tOriginalUserInput");

	// create temp task map for checking for errors that includes all files that exist in all folders that have associated tasks
	map<String, map<String, vector<Task>>> tempTaskMap;

	for (auto& directory : taskMapNew)
		for (auto& fullpath : directory.second)
			for (auto& t : fullpath.second)
				tempTaskMap[directory.first][t->getNewPathStr()].push_back(*t);

	// add files to temp task map, this will increase the size to above 1 (the number of tasks for a given full path) if there is a file conflict
	for (auto& directory : tempTaskMap)
	{
		vector<String> files;

		if (FileHelper::doesFolderExist(directory.first))
			files = FileHelper::getFiles(directory.first);

		for (auto& f : files)
		{
			Task t;
			t.origFile = f;
			t.newFile = f;
			t.operation = Operation::none;

			auto it = taskMapOriginal[directory.first].find(t.origFile);
			if (it == taskMapOriginal[directory.first].end())
				tempTaskMap[directory.first][t.getNewPathStr()].push_back(t); // the size is increased here
		}
	}

	// check size for full path (number of tasks for given path)
	for (auto& directory : tempTaskMap)
	{
		for (auto& fullpath : directory.second)
		{
			if (fullpath.second.size() > 1)
			{
				for (auto& t : fullpath.second)
				{
					if (t.operation == Operation::none)
						ret.push_back("File already exists\t" + getOperationName(t.operation) + "\t" + t.origFile);
					else
						ret.push_back("File shares new name with another file targeted for manipulated\t" + getOperationName(t.operation) + "\t" + t.origFile + "\t" + t.getNewPathStr() + "\t" + t.userInput);
				}
			}
		}
	}

	didCheckForConflicts = true;

	if (ret.size() >= 2)
		return Convert::toString(ret, "\n");

	return {};
}

void BatchFileChanger::runTasks()
{
	if (!didCheckForConflicts && checkForConflicts().isNotEmpty())
		throw std::invalid_argument("Cannot run tasks, there are file conflicts. Call checkForConflicts() to review the issues.");

	// initial rename with random characters
	for (auto& directory : taskMapNew)
	{
		for (auto& fullpath : directory.second)
		{
			for (auto& t : fullpath.second)
			{
				if (t->completedTask)
					continue;

				const String& o = t->origFile;
				const String& n = t->getNewPathStr();
				const String& r = t->random;

				try
				{
					switch (t->operation)
					{
					case Operation::invalid:
					case Operation::none:
						break;
					case Operation::move:
						FileHelper::setFilePath(o, n + r, false);
						break;
					case Operation::rename:
						FileHelper::setFilePath(o, n + r, false);
						break;
					case Operation::copy:
						FileHelper::copyFile(o, n + r, false);
						break;
					case Operation::temporary:
						FileHelper::setFilePath(o, n + r, false);
						break;
					}
				}
				catch (std::exception & e)
				{
					throw std::invalid_argument("Failed at completing a task. You can re-run this function to continue where it left off. -> " + string(e.what()));
				}
				t->completedTask = true;
				runTasksWasCalled = true;
				orderOfOperations.push_back(t);
			}
		}
	}

	// remove random characters
	for (auto& directory : taskMapNew)
	{
		for (auto& fullpath : directory.second)
		{
			for (auto& t : fullpath.second)
			{
				if (t->completedRandomRemove)
					continue;

				//const String& o = t->origFile;
				const String& n = t->getNewPathStr();
				const String& r = t->random;
				try
				{
					switch (t->operation)
					{
					case Operation::rename:
					case Operation::move:
					case Operation::copy:
					case Operation::temporary:
						FileHelper::setFilePath(n + r, n, false);
						break;
					case Operation::invalid:
					case Operation::none:
						break;
					}
				}
				catch (std::exception & e)
				{
					throw std::invalid_argument("Failed at running tasks / removing random characters. You can re-run this function to continue where it left off. -> " + string(e.what()));
				}
				t->completedRandomRemove = true;
				orderOfOperations.push_back(t);
			}
		}
	}
}

void BatchFileChanger::undoTasks()
{
	for (auto& t : orderOfOperations)
	{
		if (!t->completedRandomRemove)
			continue;

		try
		{
			switch (t->operation)
			{
			case Operation::rename:
			case Operation::move:
			case Operation::copy:
				FileHelper::moveToTrash(t->getNewPathStr());
				break;
			case Operation::temporary:
				FileHelper::setFilePath(t->getNewPathStr(), t->getNewPathStr() + t->random, false);
				break;
			case Operation::invalid:
			case Operation::none:
				break;
			}
		}
		catch (std::exception & e)
		{
			throw std::invalid_argument("Failed at undoing / removing the random string. You can re-run the undo function to continue where it left off. -> " + string(e.what()));
		}

		t->completedRandomRemove = false;
	}

	for (auto& t : orderOfOperations)
	{
		if (!t->completedTask)
			continue;

		try
		{
			switch (t->operation)
			{
			case Operation::rename:
			case Operation::move:
			case Operation::temporary:
				FileHelper::setFilePath(t->getNewPathStr() + t->random, t->origFile, false);
				break;
			case Operation::copy:
				FileHelper::moveToTrash(t->getNewPathStr() + t->random);
				break;
			case Operation::invalid:
			case Operation::none:
				break;
			}
		}
		catch (std::exception & e)
		{
			throw std::invalid_argument("Failed at undoing a task. You can re-run the undo function to continue where it left off. -> " + string(e.what()));
		}

		t->completedTask = false;
	}
}

vector<String> BatchFileChanger::getRenameList()
{
	vector<String> ret;

	//// organized order
	//for (auto& directory : taskMapNew)
	//	for (auto& fullpath : directory.second)
	//		for (auto& t : fullpath.second)
	//			ret.push_back(t->origFile + "," + t->getNewPathStr());

	// as-added order
	for (auto& t : TASKS)
		ret.push_back(t->origFile + "," + t->getNewPathStr());

	return std::move(ret);
}

String BatchFileChanger::getOperationName(Operation operation)
{
	switch (operation)
	{
	case Operation::none:
		return "none";
	case Operation::move:
		return "move";
	case Operation::rename:
		return "rename";
	case Operation::copy:
		return "copy";
	case Operation::temporary:
		return "temporary";
	case Operation::invalid:
		return "invalid";
	}
	return {};
}

String BatchFileChanger::generateRandom7CharString()
{
	// 8,031,810,176 possible combinations
	String str = 
		String::charToString(randomGen.nextFloat() * 25 + 65) +
		String::charToString(randomGen.nextFloat() * 25 + 65) +
		String::charToString(randomGen.nextFloat() * 25 + 65) +
		String::charToString(randomGen.nextFloat() * 25 + 65) +
		String::charToString(randomGen.nextFloat() * 25 + 65) +
		String::charToString(randomGen.nextFloat() * 25 + 65) +
		String::charToString(randomGen.nextFloat() * 25 + 65);

	randomStrings[str]++;
	
	if (randomStrings[str] > 1)
	{
		jassertfalse; // how could this happen in a 1 in eight billion chances?
		generateRandom7CharString();
	}

	return str;
}
