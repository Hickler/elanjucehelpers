#pragma once

#include "JuceHeader.h"
#include <vector>

namespace VectorHelper
{
	template <typename t> void insert(std::vector<t>& v, size_t index, t element)
	{
		v.insert(v.begin() + index, element);
	}

	template <typename t> void move(std::vector<t>& v, size_t oldIndex, size_t newIndex)
	{
		if (oldIndex > newIndex)
			std::rotate(v.rend() - oldIndex - 1, v.rend() - oldIndex, v.rend() - newIndex);
		else
			std::rotate(v.begin() + oldIndex, v.begin() + oldIndex + 1, v.begin() + newIndex + 1);
	}

	template <typename t> void erase(std::vector<t>& v, size_t index)
	{
		v.erase(v.begin() + index);
	}

	template <typename t> void eraseIf(std::vector<t>& v, std::function<bool(const t&)> f/* = [](const t& obj) { return true; }*/)
	{
		v.erase(std::remove_if(v.begin(), v.end(), f), v.end());
	}

	template <typename t> void eraseObject(std::vector<t>& v, t objectToRemove)
	{
		auto it = std::find(v.begin(), v.end(), objectToRemove);
		if (it != v.end())
			v.erase(it);
	}

	template <typename t> t popFront(std::vector<t>& v)
	{
		auto ret = std::move(v[0]);
		v.erase(v.begin());
		return std::move(ret);
	}

	// Appends a onto the end of b
	template <typename t> void append(std::vector<t>& a, const std::vector<t>& b)
	{
		a.insert(a.end(), b.begin(), b.end());
	}

	template<class t> bool contains(const std::vector<t>& v, t elementToCheckFor)
	{
		return std::any_of(v.begin(), v.end(), [&](t e) { return e == elementToCheckFor; });
	}

	// Only pushes if element does not already exist.
	template<class t> void pushUnique(std::vector<t>& v, t newElement)
	{
		if (!contains(v, newElement))
			v.push_back(newElement);
	}

	// Only pushes if element does not already exist.
	template <typename t> size_t indexOf(const std::vector<t>& vecOfElements, const t& element)
	{
		auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);
		if (it != vecOfElements.end())
			return distance(vecOfElements.begin(), it);

		return std::numeric_limits<size_t>::max(); // Return maximum size_t value if not found
	}

	template<class t> void sort(std::vector<t>& v, std::function<bool(const t&, const t&)> f = [](const t& a, const t& b) { return a < b; })
	{
		std::stable_sort(v.begin(), v.end(), f);
	}

	// Finds nearest element in std::vector. Uses higher value when two elements are valid. Requires operator-, operator<, operator<=
	template<class t> t nearest(std::vector<t>& v, t element)
	{
		if (v.size() == 0)
			return {};

		const auto it = std::lower_bound(v.begin(), v.end(), element);

		if (it == v.begin())
			return v[0];

		if (abs(*it - element) <= abs(*(it - 1) - element))
			return *it;

		return *(it - 1);
	}

	// Returns a new std::vector of random elements from given std::vector attempting not to repeat indexes if num elements is larger than size.
	template <class t> std::vector<t> sample(const std::vector<t> v, int elements)
	{
		std::vector<t> ret;
		ret.reserve(elements);

		while (ret.size() < elements)
		{
			std::shuffle(v.begin(), v.end(), getRandomEngine());
			ret.insert(ret.end(), v.begin(), v.begin() + std:::min(v.size(), elements - ret.size()))
		}

		return std::move(ret);
	}

	// Shuffles the given std::vector in place.
	template <class t> void shuffle(std::vector<t>& v)
	{
		std::shuffle(v.begin(), v.end(), getRandomEngine());
	}

	// Get a default_random_engine for use in std::algorithm that require a randomizer object using system time as a seed
	std::default_random_engine getRandomEngine();	
};

namespace Convert
{
	template <typename t> std::vector<t> toVector(const juce::Array<t>& x)
	{
		std::vector<t> ret;
		ret.reserve(x.size());
		for (const auto& obj : x)
			ret.push_back(obj);

		return ret;
	}

	template <typename tA, typename tB> std::vector<tA> toVector(const std::vector<tB>& x)
	{
		std::vector<tA> ret;
		ret.reserve(x.size());
		for (const auto& obj : x)
			ret.push_back(obj);

		return ret;
	}

	template <typename t> std::vector<t> toVector(const t* x, size_t indexes)
	{
		return std::vector<t>(x, x + indexes);
	}

	template <typename tA, typename tB> std::vector<tA> toVector(const tB* x, size_t indexes)
	{
		std::vector<tA> ret;
		ret.reserve(indexes);
		for (int i = 0; i < indexes; ++i)
			ret.push_back(tA(x[i]));

		return std::move(ret);
	}

	std::vector<juce::String> toVector(const juce::StringArray& x);
	std::vector<juce::String> toVector(const juce::String& x, const juce::juce_wchar& c);
	std::vector<juce::String> fromLines(const juce::String& x);
	void appendFromLines(std::vector<juce::String>& v, const juce::String& x);

	juce::Array<juce::var> toVar(const juce::Array<juce::String>& x);
	juce::Array<juce::var> toVar(const juce::Array<juce::File>& x);
	juce::Array<juce::var> toVar(const std::vector<juce::String>& x);
	juce::Array<juce::var> toVar(const std::vector<double>& x);

	juce::StringArray toStringArray(const std::vector<juce::String>& x);
	juce::StringArray toStringArray(const juce::var& x);

	juce::String toString(const std::vector<juce::String>& x, juce::StringRef separator = "", int start = 0, int numberToJoin = -1);
	juce::String toString(const std::vector<juce::juce_wchar>& x);
	juce::String toString(const juce::Array<int>& x, juce::StringRef separator = "");
	juce::String toString(const juce::StringArray& x, juce::StringRef separator = "", int start = 0, int numberToJoin = -1);
};

void DEBUG_PRINT(const juce::ValueTree& x);
void DEBUG_PRINT(const std::vector<juce::String>& x);
void DEBUG_PRINT(const juce::var& x);
void DEBUG_PRINT(const juce::Array<juce::var>* x);
void DEBUG_PRINT(const juce::DynamicObject* x);

template <typename t> juce::String valueToTimestamp(t seconds, int zeroPadding = 2)
{
	double time = double(seconds);
	int h = floor(time / 60.0 / 60.0);
	time -= h * 60 * 60;
	int m = floor(time / 60.0);
	time -= m * 60;
	int s = floor(time);
	juce::String ms = juce::String(time - s).replaceFirstOccurrenceOf("0.", "");
	return juce::String(h).paddedLeft('0', zeroPadding) + ":" + juce::String(m).paddedLeft('0', zeroPadding) + ":" + juce::String(s).paddedLeft('0', zeroPadding) + "." + ms;
}
