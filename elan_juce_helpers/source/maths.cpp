#include "maths.h"

#include "JuceHeader.h"
#include "sehelper.hpp"

namespace elan
{
	void sinCos(double v, double * sinOut, double * cosOut)
	{
		RAPT::rsSinCos(v, sinOut, cosOut);
	}
	double freqToPitch(double freq, double masterTuneA4)
	{
		return RAPT::rsFreqToPitch(freq, masterTuneA4);
	}
	double pitchToFreq(double v)
	{
		return RAPT::rsPitchToFreq(v);
	}
	double pitchOffsetToFreqFactor(double v)
	{
		return RAPT::rsPitchOffsetToFreqFactor(v);
	}
} // namespace elan

constexpr double kPI    = 3.1415926535897932384626433832795;
double const TAU        = kPI * 2;
double const TAU_INV    = 1 / (kPI * 2);
double const PI_INV_x_2 = (1 / kPI) * 2;
double const PI_z_2     = kPI / 2;
double const PI_INV_z_2 = PI_INV/2;
double const PI_z_4     = kPI / 4;
double const PI_z_6     = kPI / 6;
double const PI_x_2     = kPI * 2;
double const PI_x_1p5   = kPI * 1.5;
double const PI_x_4     = kPI * 4;
double const INV_180 = 1.0/180.0;
double const TWOTHIRDS = 2.0 / 3.0;
double const ONETHIRD = 1.0 / 3.0;
const double INV_127 = 1 / 127.0;
const double INV_8192 = 1 / 8192.0;

double squared(double v) { return v*v; }

double approachValue(double inputValueNormalized, double valueToModify, double valueToApproach)
{
	double delta = abs(valueToModify - valueToApproach) * inputValueNormalized;

	return valueToModify > valueToApproach ? valueToModify - delta : valueToModify + delta;
}

const vector<double> powers_of_two{
	pow(2,-20),pow(2,-19),pow(2,-18),pow(2,-17),pow(2,-16),
	pow(2,-15),pow(2,-14),pow(2,-13),pow(2,-12),pow(2,-11),
	pow(2,-10),pow(2,-9),pow(2,-8),pow(2,-7),pow(2,-6),
	pow(2,-5),pow(2,-4),pow(2,-3),pow(2,-2),pow(2,-1),
	pow(2,0),pow(2,1),pow(2,2),pow(2,3),pow(2,4),
	pow(2,5),pow(2,6),pow(2,7),pow(2,8),pow(2,9),
	pow(2,10),pow(2,11),pow(2,12),pow(2,13),pow(2,14),
	pow(2,15),pow(2,16),pow(2,17),pow(2,18),pow(2,19),
	pow(2,20)
};

double powerOfTwo(int power)
{
	jassert(power >= -20 && power <= 20);
	return powers_of_two[power + 20];
}

const vector<double> powers_of_ten{
	pow(10,-20),pow(10,-19),pow(10,-18),pow(10,-17),pow(10,-16),
	pow(10,-15),pow(10,-14),pow(10,-13),pow(10,-12),pow(10,-11),
	pow(10,-10),pow(10,-9),pow(10,-8),pow(10,-7),pow(10,-6),
	pow(10,-5),pow(10,-4),pow(10,-3),pow(10,-2),pow(10,-1),
	pow(10,0),pow(10,1),pow(10,2),pow(10,3),pow(10,4),
	pow(10,5),pow(10,6),pow(10,7),pow(10,8),pow(10,9),
	pow(10,10),pow(10,11),pow(10,12),pow(10,13),pow(10,14),
	pow(10,15),pow(10,16),pow(10,17),pow(10,18),pow(10,19),
	pow(10,20)
};

double powerOfTen(int power)
{
	jassert(power >= -20 && power <= 20);
	return powers_of_ten[power+20];
}

double roundTo(double v, int decimals)
{
	double modifier = powerOfTen(decimals);
	return round(v * modifier) / modifier;
}

double roundPrecise(double v, double precision)
{
	double numberOfDivisions = round(v / precision);
	return numberOfDivisions * precision;
}

int numDigitsBeforeDecimal(double v)
{
	return toInt(log10(std::max(abs(v), 1.0)));
}

int numDigitsInInteger(int v)
{
	return 1 + toInt(floor(log10(v)));
}

double tent(double x)
{
	x = fabs(x);
	if (x > 1.0)
		return 0.0;
	return 1.0 - x;
}

double getDistanceFromLine(double x1, double y1, double x2, double y2, double xp, double yp)
{
	auto yDistance = y2 - y1;
	auto xDistance = x2 - x1;
	return std::abs((y2 - y1) * xp - (x2 - x1) * yp + x2 * y1 - y2 * x1) /
		std::sqrt(yDistance * yDistance + xDistance * xDistance);
}

double bipolarToUnipolar(double v) { return v * .5 + .5; }
double unipolarToBipolar(double v) { return v * 2 - 1; }

double curve(double value, double tension)
{
	double t = tension;
	double v = value;
	return (t*v-v)/(2*t*v-t-1);
}

double s_curve(double value, double tension)
{
	double t = tension;
	double v = value;

	if (v < 0.5)
		return (t*v-v) / (4*t*v-t-1);

	v += -0.5;
	t *= -0.5;
	return (t*v-v*0.5) / (4*t*v-t-0.5) + 0.5;
}

void phaseIncrement(double * phase, double increment)
{
	*phase = fmod(*phase+increment, 1);
}

void phaseIncrementTAU(double * phase, double increment)
{
	////original method
	*phase = fmod(*phase+increment, TAU);
	
	// another method
	//*phase += increment;
	//wrapPhaseTAU(phase);

	// another method
	//phase += increment; 
	//phase -= floor(phase);

	//method 1 for negative phase
	//*phase = *phase - TAU * floor(phase / TAU);

	//method 2 for negative phase
	//*phase += increment;
	//while (*phase > TAU)
	//	*phase -= TAU;
	//while (*phase < 0.0)
	//	*phase += TAU;
}

double degreesToPhase(double degrees)
{
	return PI * degrees * INV_180;
}

double valueToRotation(double v)
{
	return v * TAU;
}

double dbToAmpAccurate(double v)
{
  return pow(10, v/20);
}

double beatsToFrequency(double BPM, double beats)
{
	return 1 / beatsToTime(BPM, beats);
}

double barsToFrequency(double BPM, double bars)
{
	return 1 / beatsToTime(BPM, bars * 4.0);
}

double beatsToTime(double BPM, double beats)
{
	return (60.0 / BPM) * beats;
}

double AmplitudeToRatio(double a)
{
	return (a > 0.0 ? a + 1.0 : 1.0 - a / -2.0);
}

double RatioToAmplitude(double r)
{
	return r > 1.0 ? r - 1.0 : r * 2.0 - 2.0;
}

void wrapPhase(double * phase) 
{
	*phase -= floor(*phase);
}

double wrapPhase(double phase)
{
	return phase - floor(phase);
}

void wrapPhaseTAU(double * phase)
{
	//method 1:
	*phase -= TAU * floor(*phase * TAU_INV);

	////method 2 for negative phase
	//while (*phase > TAU)
	//	*phase -= TAU;
	//while (*phase < 0.0)
	//	*phase += TAU;
}

double wrapPhaseTAU(double phase)
{
	return phase - TAU * floor(phase * TAU_INV);

	//while (phase > TAU)
	//	phase -= TAU;
	//while (phase < 0.0)
	//	phase += TAU;
	//return phase;
}

int wrapSingleOctave_Positive(int index, int size)
{
	jassert(size != 0); // causes infinite hang
	jassert(index > -1); // must be index above -1

	while (index >= size)
		index -= size;
	return index;
}

int wrapSingleOctave_Negative(int index, int size)
{
	jassert(size != 0); // causes infinite hang
	jassert(index < size); // must be index below size

	while (index < 0)
		index += size;
	return index;
}

int wrapSingleOctave_Bipolar(int index, int size)
{
	jassert(size != 0); // causes infinite hang

	while (index >= size)
		index -= size;
	while (index < 0)
		index += size;
	return index;
}

int wrapMultiOctave_Bipolar(int index, int size, int* indexOctave)
{
	jassert(size != 0); // causes infinite hang

	int wrappedIndex;
	int octave;

	if (index >= 0)
	{
		octave = +(int)floor((double)index / (double)size);
		wrappedIndex = index % size;
	}
	else
	{
		index = abs(index);
		octave = -(int)ceil((double)index / (double)size);
		wrappedIndex = size * abs(octave) - index;
	}

	if (indexOctave != nullptr)
		*indexOctave = octave;

	return wrappedIndex;
}

void rotate2D(double sin, double cos, double * X, double * Y)
{
	double x = *X, y = *Y;
	*X = x * cos - y * sin;
	*Y = x * sin + y * cos;
}

void rotate3D(double inX, double inY, double inZ, 
	double rotX_sin, double rotX_cos, 
	double rotY_sin, double rotY_cos, 
	double * outL, double * outR)
{
	double x1 = inX*rotX_cos - inY*rotX_sin;
	double y1 = inX*rotX_sin + inY*rotX_cos;

	double x2 = x1*rotY_cos - inZ*rotY_sin;
	//double y2 = x1*rotY_sin + inZ*rotY_cos;

	*outL = x2;
	*outR = y1;
}

bool isErroneousNumber(vector<double> arrayOfNumbersToCheck)
{
  for (size_t i = 0; i < arrayOfNumbersToCheck.size(); ++i)
  {
    double n = arrayOfNumbersToCheck[i];

    if (std::isnan(n))
      return true;
    if (std::isinf(n))
      return true;
  }

  return false;
}
