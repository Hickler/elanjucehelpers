#include "FileHelper.h"

#include <exception>
#include <filesystem>

using juce::File;
using juce::String;
using juce::StringRef;
using juce::juce_wchar;
using std::invalid_argument;
using std::exception;
using std::string;
using namespace filesystem;

String illegal_filename_chars  = "*?|<>\"\\/:";
String illegal_directory_chars = "*?|<>\"";

#ifdef JUCE_WINDOWS
String systemSlash = "\\";
#else
String systemSlash = "/";
#endif

/* Unkown errors could be a permissions error. */

map<FileHelper::Error, String> FileHelper::errorMessages = {
  // Formatting errors
    {                       FileHelper::Error::empty_string,                                                                                                          "Input string is empty."},
    {                       FileHelper::Error::illegal_char,                                                                                                        "Illegal character found."},
    {                   FileHelper::Error::path_is_relative,                                                                                                   "File path cannot be relative."},
    {                        FileHelper::Error::path_is_dir,                                                                                                "File path cannot be a directory."},
    {                   FileHelper::Error::space_before_ext,                                                                                                         "Space before extension."},
    {                  FileHelper::Error::begins_with_space,                                                                                                              "Begins with space."},
    {                    FileHelper::Error::ends_with_space,                                                                                                                "Ends with space."},
    {                      FileHelper::Error::ends_with_dot,                                                                                                                  "Ends with dot."},
    {          FileHelper::Error::ends_or_begins_with_space,                                                                                                      "Ends or begins with space."},
 // Existence errors
    {                FileHelper::Error::file_already_exists,                                                                                                            "File already exists."},
    {              FileHelper::Error::folder_already_exists,                                                                                                          "Folder already exists."},
    {      FileHelper::Error::file_already_exists_as_folder,                                                                                                "File already exists as a folder."},
    {         FileHelper::Error::directory_with_multi_slash,                                                                                   "Directory path has multiple slashes in a row."},
    {   FileHelper::Error::directory_already_exists_as_file,                                                                                               "Directory already exists as file."},
    {                FileHelper::Error::path_does_not_exist,                                                                                        "Path does not exist as a file or folder."},
    {                FileHelper::Error::file_does_not_exist,                                                                                                            "File does not exist."},
    {           FileHelper::Error::directory_does_not_exist,                                                                                                       "Directory does not exist."},
 // Creation errors
    {           FileHelper::Error::unknown_error_create_dir,                            "Unknown error creating directory, check that path is valid, drive letter, or file write permissions."},
    {  FileHelper::Error::unknown_error_file_move_or_rename,                "Unknown error moving or renaming file, may be in use by another program or check file permissions or disk space."},
    {FileHelper::Error::unknown_error_folder_move_or_rename, "Unknown error moving or renaming folder, files inside may be in use by another program or check file permissions or disk space."},
    {           FileHelper::Error::unknown_error_file_trash,                   "Unknown error moving file to trash, may be in use by another program or check file permissions or disk space."},
    {          FileHelper::Error::unknown_error_file_delete,                                        "Unknown error deleting file, may be in use by another program or check file permissions."},
    {          FileHelper::Error::unknown_error_create_file,                           "Unknown error creating file, check that path is valid, drive letter, file permissions, or disk space."},
    {       FileHelper::Error::unknown_error_overwrite_file,                        "Unkown error overwriting file, may be in use by another program or check file permissions or disk space."},
    {            FileHelper::Error::unknown_error_file_copy,                            "Unknown error copying file, check that path is valid, drive letter, file permissions, or disk space."},
    {          FileHelper::Error::unknown_error_modify_file,                         "Unknown error modifying file, may be in use by another program or check file permissions or disk space."},
};

StringOrFile::StringOrFile(String s)
  : s(s) {
    f = File::createFileWithoutCheckingPath(s);
}

StringOrFile::StringOrFile(File f)
  : s(f.getFullPathName())
  , f(f) {}

StringOrFile::StringOrFile(StringRef s)
  : s(s) {
    f = File::createFileWithoutCheckingPath(s);
}

StringOrFile::operator File() {
    return f;
}
StringOrFile::operator String() {
    return s;
}
StringOrFile::operator StringRef() {
    return s;
}

String StringOrFile::getFullPathName() const {
    return s;
}
String StringOrFile::getFileNameWithoutExtension() const {
    return f.getFileNameWithoutExtension();
}

void FileHelper::throwError(const String& path, FileHelper::Error v) {
    throw invalid_argument(path.toStdString() + " -> " + errorMessages[v].toStdString());
}

bool FileHelper::isSlashChar(const juce_wchar& c) {
    return c == '\\' || c == '/';
}

bool FileHelper::isIllegalNameChar(const juce_wchar& c) {
    return c <= 31 || CHAr::isAny(c, illegal_filename_chars);
}

bool FileHelper::isIllegalDirChar(const juce_wchar& c) {
    return c <= 31 || CHAr::isAny(c, illegal_directory_chars);
}

String FileHelper::createLegalString(const String& filePathOrName) {
    String fileFolder = getDir(filePathOrName).trim();
    String fileName   = getName(filePathOrName).trim();

    StringArray chars;

    enum State {
        FINDSLASH,
        REMOVESLASH
    } state = FINDSLASH;

    for (int i = 0; i < fileFolder.length(); ++i) {
        switch (state) {
        case FINDSLASH:
            if (isSlashChar(fileFolder[i])) {
                state = REMOVESLASH;
                chars.add("/");
                continue;
            }
            break;
        case REMOVESLASH:
            if (isSlashChar(fileFolder[i])) {
                continue;
            } else {
                state = FINDSLASH;
            }
            break;
        }

        chars.add(String::charToString(isIllegalDirChar(fileFolder[i]) ? '_' : fileFolder[i]));
    }

    if (fileName.isNotEmpty()) {
        if (!isSlashChar(chars[chars.size() - 1][0])) {
            chars.add("/");
        }
        for (int i = 0; i < fileName.length(); ++i) {
            chars.add(String::charToString(isIllegalNameChar(fileName[i]) ? '_' : fileName[i]));
        }
    }

    return chars.joinIntoString("");
}

File FileHelper::setFilePath(const File& originalFile, const String& filePath, bool overwrite) {
    if (originalFile == filePath) {
        return originalFile;
    }

    ensureValidFilePath(filePath);

    File newFile = filePath;

    if (newFile.existsAsFile()) {
        if (!overwrite) {
            throwError(filePath, FileHelper::Error::file_already_exists);
        } else if (!newFile.deleteFile()) {
            throwError(filePath, FileHelper::Error::unknown_error_overwrite_file);
        }
    }

    if (!newFile.getParentDirectory().createDirectory()) {
        throwError(FileHelper::getDir(filePath), FileHelper::Error::unknown_error_create_dir);
    }

    if (!originalFile.moveFileTo(newFile)) {
        throwError(originalFile.getFullPathName() + " -> " + newFile.getFullPathName(), FileHelper::Error::unknown_error_file_move_or_rename);
    }

    return newFile;
}

File FileHelper::setPath(const File& originalPath, const String& path, bool overwrite) {
    if (originalPath == path) {
        return originalPath;
    }

    ensureValidPath(path);

    File newFile = path;

    if (newFile.existsAsFile()) {
        if (!overwrite) {
            throwError(path, FileHelper::Error::file_already_exists);
        } else if (!newFile.deleteFile()) {
            throwError(path, FileHelper::Error::unknown_error_overwrite_file);
        }
    }

    if (!newFile.getParentDirectory().createDirectory()) {
        throwError(FileHelper::getDir(path), FileHelper::Error::unknown_error_create_dir);
    }

    if (!originalPath.moveFileTo(newFile)) {
        throwError(originalPath.getFullPathName() + " -> " + newFile.getFullPathName(), FileHelper::Error::unknown_error_file_move_or_rename);
    }

    return newFile;
}

String FileHelper::getUnusedFilePath(String filePath, bool replaceIllegalChars) {
    if (replaceIllegalChars) {
        filePath = createLegalString(filePath);
    }

    String fileDir, fileName, fileExt;
    getFileParts(filePath, fileDir, fileName, fileExt);

    return juce::File(fileDir).getNonexistentChildFile(fileName, "." + fileExt).getFullPathName();
}

bool FileHelper::hasExt(const String& filePath) {
    int indexOfDot = filePath.lastIndexOf(".");
    // dot is found && is not first char && is not last char
    return indexOfDot != -1 && indexOfDot != 0 && indexOfDot != (filePath.length() - 1);
}

String FileHelper::getExt(const String& f) {
    StringIterator it(f);       
    it.setMoveReverse();
    it.skipToEnd();    
    it.start();
    it.skipToChar(".");
    it.swap();
    return it.getSubstring();
}

String FileHelper::setExt(const String& file, StringRef ext) {
    return file.upToLastOccurrenceOf(".", false, false) + "." + ext;
}

String FileHelper::assumeExt(StringRef file, StringRef ext) {
    return hasExt(file) ? file : setExt(file, ext);
}

String FileHelper::assumeFolder(StringRef file, StringRef folder) {
    return isRelativePath(file) ? folder + "/" + file : file;
}

String FileHelper::assume(StringRef file, StringRef folder, StringRef ext) {
    return assumeFolder(assumeExt(file, ext), folder);
}

String FileHelper::removeExt(StringOrFile file) {
    return file.getFileNameWithoutExtension();
}

String FileHelper::addSuffix(StringOrFile fileName, const String& suffix) {
    int lastSlashIndex = fileName.s.lastIndexOfAnyOf("\\/");
    int lastDotIndex   = fileName.s.lastIndexOf(".");

    auto beforeDotStr = fileName.s.substring(lastSlashIndex, lastDotIndex);
    auto afterDotStr  = fileName.s.substring(lastDotIndex, fileName.s.length());

    return beforeDotStr + suffix + afterDotStr;
}

String FileHelper::getDir(const File& f) {
    return f.getParentDirectory().getFullPathName();
}

String FileHelper::toString(const File& f) {
    return f.getFullPathName();
}

void FileHelper::getFileParts(const String& f, String& driveLetterOut, String& fileDirOut, String& fileNameNoExtOut, String& fileExtOut) {
    if (FileHelper::isValidFileName(f)) {
        driveLetterOut   = "";
        fileDirOut       = "";
        fileNameNoExtOut = FileHelper::getNameNoExt(f);
        fileExtOut       = FileHelper::getExt(f).substring(1);
        return;
    }

    if (isRelativePath(f)) {
        vector<String> splits = STR::splitToVector(f, "\\/");
        if (isValidFileName(splits.back())) {
            String fileName  = splits.back();
            driveLetterOut   = "";
            fileDirOut       = Convert::toString(splits, systemSlash, 0, static_cast<int>(splits.size() - 1));
            fileNameNoExtOut = FileHelper::getNameNoExt(fileName);
            fileExtOut       = FileHelper::getExt(fileName).substring(1);
        } else {
            fileDirOut = Convert::toString(splits, systemSlash, 0, static_cast<int>(splits.size()));
        }
        return;
    }

    File file{ f };

#ifdef JUCE_WINDOWS
    driveLetterOut = f.substring(0, 1);
    fileDirOut     = file.getParentDirectory().getFullPathName().substring(3);
#else
    driveLetterOut = systemSlash;
    fileDirOut     = file.getParentDirectory().getFullPathName().substring(1);
#endif
    fileNameNoExtOut = file.getFileNameWithoutExtension();
    fileExtOut       = file.getFileExtension().substring(1);
}

void FileHelper::getFileParts(const File& f, String& fileDirOut, String& fileNameNoExtOut, String& fileExtOut) {
    fileDirOut       = FileHelper::isValidFilePath(f) ? f.getParentDirectory().getFullPathName() : f.getFullPathName();
    fileNameNoExtOut = f.getFileNameWithoutExtension();
    fileExtOut       = f.getFileExtension().substring(1);
}

void FileHelper::getFileParts(const File& f, String& fileDirOut, String& fileNameOut) {
    fileDirOut  = FileHelper::isValidFilePath(f) ? f.getParentDirectory().getFullPathName() : f.getFullPathName();
    fileNameOut = f.getFileName();
}

void FileHelper::openFolder(const String& directory, bool createIfNeeded) {
    ensureValidDir(directory);

    if (!doesFolderExist(directory)) {
        if (createIfNeeded) {
            createFolder(directory);
        } else {
            throwError(directory, FileHelper::Error::directory_does_not_exist);
        }
    }

    File(directory).startAsProcess();
}

void FileHelper::openFile(const String& path, bool createIfNeeded) {
    ensureValidFilePath(path);

    if (!doesFileExist(path)) {
        if (createIfNeeded) {
            createFolder(path);
        } else {
            throwError(path, FileHelper::Error::file_does_not_exist);
        }
    }

    File(path).startAsProcess();
}

void FileHelper::revealFolder(const String& directory, bool createIfNeeded) {
    ensureValidDir(directory);

    if (!doesFolderExist(directory)) {
        if (createIfNeeded) {
            createFolder(directory);
        } else {
            throwError(directory, FileHelper::Error::directory_does_not_exist);
        }
    }

    File(directory).revealToUser();
}

void FileHelper::revealFile(const String& path, bool createIfNeeded) {
    ensureValidFilePath(path);

    if (!doesFileExist(path)) {
        if (createIfNeeded) {
            createFolder(path);
        } else {
            throwError(path, FileHelper::Error::file_does_not_exist);
        }
    }

    File(path).revealToUser();
}

bool FileHelper::isValidFolderName(StringOrFile folderName) {
    try {
        ensureValidFolderName(folderName);
        return true;
    } catch (exception&) {
        return false;
    }
}

bool FileHelper::isValidPath(StringOrFile file) {
    try {
        ensureValidPath(file);
        return true;
    } catch (exception&) {
        return false;
    }
}

bool FileHelper::isValidFolderPath(StringOrFile dir) {
    try {
        ensureValidDir(dir);
        return true;
    } catch (exception&) {
        return false;
    }
}

File FileHelper::setFileDir(const File& file, StringOrFile fileDir, bool overwrite) {
    if (file.getParentDirectory() == fileDir) {
        return fileDir;
    }

    ensureValidDir(fileDir);

    if (!File(fileDir).createDirectory()) {
        throwError(fileDir, FileHelper::Error::unknown_error_create_dir);
    }

    File newFile = fileDir.s + "/" + file.getFileName();

    if (newFile.existsAsFile()) {
        if (!overwrite) {
            throwError(fileDir, FileHelper::Error::file_already_exists);
        } else if (!newFile.deleteFile()) {
            throwError(fileDir, FileHelper::Error::unknown_error_overwrite_file);
        }
    }

    if (!file.moveFileTo(newFile)) {
        throwError(file.getFullPathName(), FileHelper::Error::unknown_error_file_move_or_rename);
    }

    return newFile;
}

File FileHelper::setFileName(const File& file, StringOrFile fileName, bool overwrite) {
    if (file.getFileName() == fileName.s)
        return fileName;

    ensureValidFileName(fileName);

    File newFile = file.getParentDirectory().getChildFile(fileName);

    if (newFile.existsAsFile()) {
        if (!overwrite) {
            throwError(newFile.getFullPathName(), FileHelper::Error::file_already_exists);
        } else if (!newFile.deleteFile()) {
            throwError(newFile.getFullPathName(), FileHelper::Error::unknown_error_overwrite_file);
        }
    }

    if (!file.moveFileTo(newFile)) {
        throwError(file.getFullPathName() + " :: " + newFile.getFullPathName(), FileHelper::Error::unknown_error_file_move_or_rename);
    }

    return newFile;
}

File FileHelper::setFolderName(const File& folder, const String& newName, bool /*overwrite*/) {
    if (FileHelper::getName(folder) == newName) {
        return folder;
    }

    ensureValidFolderName(newName);

    File newFolder = folder.getParentDirectory().getChildFile(newName);

    ensureFileNotExist(newFolder);

    if (newFolder.exists()) {
        throwError(newFolder.getFullPathName(), FileHelper::Error::folder_already_exists);
    }

    if (!folder.moveFileTo(newFolder)) {
        throwError(folder.getFullPathName() + " :: " + newFolder.getFullPathName(), FileHelper::Error::unknown_error_file_move_or_rename);
    }

    return newFolder;
}

String FileHelper::getName(const File& file) {
    return file.getFileName();
}

String FileHelper::getNameNoExt(const StringOrFile& file) {
    return file.getFileNameWithoutExtension();
}

File FileHelper::setName(const File& file, const String& name, bool overwrite) {
    if (FileHelper::getName(file) == name) {
        return name;
    }

    File newFile;

    if (file.existsAsFile()) {
        newFile = setFileName(file, name, overwrite);
    } else {
        newFile = setFolderName(file, name, overwrite);
    }

    return newFile;
}

File FileHelper::createFile(const String& filePath, bool overwrite) {
    ensureValidFilePath(filePath);

    File newFile = filePath;

    if (newFile.existsAsFile()) {
        if (!overwrite) {
            throwError(filePath, FileHelper::Error::file_already_exists);
        } else if (!newFile.deleteFile()) {
            throwError(filePath, FileHelper::Error::unknown_error_overwrite_file);
        }
    }

    else if (newFile.exists()) {
        throwError(filePath, FileHelper::Error::file_already_exists_as_folder);
    }

    if (!newFile.create()) {
        throwError(filePath, FileHelper::Error::unknown_error_create_file);
    }

    return newFile;
}

// Creates a file in operating system's temporary folder and returns the file.

File FileHelper::createTempFile() {
    auto tempFolder = File::getSpecialLocation(File::SpecialLocationType::tempDirectory);
    String tempPath;

    do {
        tempPath = tempFolder.getFullPathName() + "/temp_" + STR::getRandomAlphaNumericString(8) + ".tmp";
    } while (!doesFileExist(tempPath));

    createFile(tempPath, false);

    return tempPath;
}

void FileHelper::createTextFile(const String& filePath, const String& text, bool overwrite) {
    createFile(filePath, overwrite);

    if (!File(filePath).appendText(text)) {
        throwError(filePath, FileHelper::Error::unknown_error_modify_file);
    }
}

String FileHelper::readFile(const String& filePath) {
    ensureFileExists(filePath);

    return File(filePath).loadFileAsString();
}

void FileHelper::createFolder(const String& fileDir) {
    ensureValidDir(fileDir);

    if (!File(fileDir).createDirectory()) {
        throwError(fileDir, FileHelper::Error::unknown_error_create_dir);
    }
}

File FileHelper::copyFile(const File& f, const String& filePath, bool overwrite) {
    ensureValidFilePath(filePath);

    File newFile = filePath;

    if (newFile.existsAsFile()) {
        if (!overwrite) {
            throwError(newFile.getFullPathName(), FileHelper::Error::file_already_exists);
        } else if (!newFile.deleteFile()) {
            throwError(newFile.getFullPathName(), FileHelper::Error::unknown_error_overwrite_file);
        }
    }

    if (!newFile.getParentDirectory().createDirectory()) {
        throwError(newFile.getParentDirectory().getFullPathName(), FileHelper::Error::unknown_error_create_dir);
    }

    if (!f.copyFileTo(newFile)) {
        throwError(f.getFullPathName() + " :: " + newFile.getFullPathName(), FileHelper::Error::unknown_error_file_copy);
    }

    return File();
}

void FileHelper::moveToTrash(const File& f) {
    ensureValidPath(f.getFullPathName());

    if (!f.moveToTrash()) {
        throwError(f.getFullPathName(), FileHelper::Error::unknown_error_file_trash);
    }
}

void FileHelper::permanentDelete(const File& f) {
    ensureValidPath(f.getFullPathName());

    // if (!std::filesystem::remove(f.getFullPathName().toStdString()))
    //	throwError(f.getFullPathName(), FileHelper::Error::unknown_error_overwrite_file);
}

vector<String> FileHelper::removeEmptyFoldersRecursive(const File& folder) {
    vector<String> folders = getFoldersRecursive(folder);
    folders.push_back(folder.getFullPathName());

    std::sort(folders.begin(), folders.end(), [](const String& a, const String& b) { return a.length() > b.length(); });

    vector<String> removedPaths;
    for (const auto& f : folders) {
        if (!FileHelper::doesDirContainFolders(f) && !FileHelper::doesDirContainFiles(f)) {
            FileHelper::permanentDelete(f);
            removedPaths.push_back(f);
        }
    }

    return removedPaths;
}

vector<String> FileHelper::removeEmptyFilesRecursive(const File& folder) {
    auto files = getFilesRecursive(folder);

    vector<String> removedPaths;
    for (auto& f : files) {
        if (File(f).getSize() == 0) {
            FileHelper::permanentDelete(f);
            removedPaths.push_back(f);
        }
    }

    return removedPaths;
}

vector<String> FileHelper::getFolders(const File& f, const String& wildCard) {
    File path;

    if (doesFileExist(f.getFullPathName())) {
        path = f.getParentDirectory();
    } else if (isValidFolderPath(f)) {
        path = f;
    }

    ensureFolderExists(path);

    juce::DirectoryIterator iter(path, false, wildCard, File::TypesOfFileToFind::findDirectories);

    vector<String> v;

    while (iter.next()) {
        v.push_back(iter.getFile().getFullPathName().replace("\\", "/"));
    }

    return v;
}

vector<String> FileHelper::getFiles(const File& f, const String& wildCard) {
    File path;

    if (doesFileExist(f.getFullPathName())) {
        path = f.getParentDirectory();
    } else if (isValidFolderPath(f)) {
        path = f;
    }

    ensureFolderExists(path);

    juce::DirectoryIterator iter(path, false, wildCard, File::TypesOfFileToFind::findFiles);

    vector<String> v;

    while (iter.next()) {
        v.push_back(iter.getFile().getFullPathName().replace("\\", "/"));
    }

    return v;
}

vector<String> FileHelper::getFoldersRecursive(const File& f, const String& wildCard) {
    File path = isFolder(f.getFullPathName()) ? f : f.getParentDirectory();

    ensureFolderExists(path);

    juce::DirectoryIterator iter(path, true, wildCard, File::TypesOfFileToFind::findDirectories);

    vector<String> v;

    while (iter.next()) {
        v.push_back(iter.getFile().getFullPathName().replace("\\", "/"));
    }

    return v;
}

vector<String> FileHelper::getFilesRecursive(const File& f, const String& wildCard) {
    File path = isFile(f.getFullPathName()) ? f : f.getParentDirectory();

    ensureFolderExists(path);

    juce::DirectoryIterator iter(path, true, wildCard, File::TypesOfFileToFind::findFiles);

    vector<String> v;

    while (iter.next()) {
        v.push_back(iter.getFile().getFullPathName().replace("\\", "/"));
    }

    return v;
}

bool FileHelper::doesFileExist(const String& path) {
    return File(path).existsAsFile();
}

bool FileHelper::doesFolderExist(const String& directory) {
    return File(directory).isDirectory();
}

bool FileHelper::doesPathExist(const String& path) {
    return doesFileExist(path) || doesFolderExist(path);
}

bool FileHelper::doesDirContainFiles(const File& directory, String wildCard) {
    if (!isValidFolderPath(directory)) {
        return false;
    }

    if (!doesFolderExist(directory.getFullPathName())) {
        return false;
    }

    DirectoryIterator iter(directory, false, wildCard, File::TypesOfFileToFind::findFiles);

    return iter.next();
}

bool FileHelper::doesDirContainFolders(const File& directory, String wildCard) {
    ensureFolderExists(directory);

    DirectoryIterator iter(directory, false, wildCard, File::TypesOfFileToFind::findDirectories);

    return iter.next();
}

bool FileHelper::isAbsolutePath(const String& path) {
    return File::isAbsolutePath(path);
}

bool FileHelper::isRelativePath(const String& path) {
    return !File::isAbsolutePath(path);
}

bool FileHelper::isFile(const String& path) {
    return doesFileExist(path) || isValidFilePath(path);
}

bool FileHelper::isFolder(const String& path) {
    return doesFolderExist(path) || isValidFolderPath(path);
}

int FileHelper::isFileOrFolder(const String& path) {
    if (doesFileExist(path)) {
        return 1;
    }
    if (doesFolderExist(path)) {
        return 2;
    }

    if (isValidPath(path)) {
        if (File(path).getFileExtension().isNotEmpty()) {
            return 1;
        }

        return 2;
    }

    return 0;
}

void FileHelper::ensureValidFilePath(const String& path) {
    if (isRelativePath(path)) {
        throwError(path, FileHelper::Error::path_is_relative);
    }

    if (CHAr::isAny(path.getLastCharacter(), "\\/")) {
        throwError(path, FileHelper::Error::path_is_dir);
    }

    ensureValidPath(path);
}

void FileHelper::ensureValidPath(const String& path) {
    if (path.isEmpty()) {
        throwError(path, FileHelper::Error::empty_string);
    }

    String dir = File(path).getParentDirectory().getFullPathName();
    ensureValidDir(dir);

    String name = File(path).getFileName();
    if (name.isNotEmpty()) {
        ensureValidFileName(name);
    }
}

void FileHelper::ensureValidDir(const String& directory) {
    if (directory.isEmpty()) {
        throwError(directory, FileHelper::Error::empty_string);
    }

    if (directory.trim() != directory) {
        throwError(directory, FileHelper::Error::ends_or_begins_with_space);
    }

    if (directory.removeCharacters(illegal_directory_chars) != directory) {
        throwError(directory, FileHelper::Error::illegal_char);
    }

    if (directory.contains("\\\\") || directory.contains("//")) {
        throwError(directory, FileHelper::Error::directory_with_multi_slash);
    }
}

void FileHelper::ensureValidFileName(const String& fileName) {
    if (fileName.isEmpty()) {
        throwError({}, FileHelper::Error::empty_string);
    }

    enum MODE {
        begin,
        middle,
        end
    };
    MODE mode = begin;

    int last_dot_pos = 0;

    StringIterator it(fileName);

    while (!it.atEnd()) {
        switch (mode) {
        case begin:
            if (FileHelper::isIllegalNameChar(it)) {
                throwError(fileName, FileHelper::Error::illegal_char);
            }
            if (it == ' ') {
                throwError(fileName, FileHelper::Error::begins_with_space);
            }
            if (it == '.') {
                last_dot_pos = it;
            }
            if (it.atLast()) {
                mode = end;
                continue;
            }
            mode = middle;
            ++it;
            continue;
        case middle:
            if (FileHelper::isIllegalNameChar(it)) {
                throwError(fileName, FileHelper::Error::illegal_char);
            }
            if (it == '.') {
                last_dot_pos = it;
            }
            if (it.atLast()) {
                mode = end;
                continue;
            }
            ++it;
            continue;
        case end:
            if (FileHelper::isIllegalNameChar(it)) {
                throwError(fileName, FileHelper::Error::illegal_char);
            }
            if (it == ' ') {
                throwError(fileName, FileHelper::Error::ends_with_space);
            }
            if (it == '.') {
                throwError(fileName, FileHelper::Error::ends_with_dot);
            }
            ++it;
            continue;
        }
    }

    // make sure no space before extension
    if (it[max(last_dot_pos - 1, 0)] == ' ') {
        throwError(fileName, FileHelper::Error::space_before_ext);
    }
}

void FileHelper::ensureValidFolderName(const String& folderName) {
    if (folderName.isEmpty()) {
        throwError(folderName, FileHelper::Error::empty_string);
    }

    if (folderName.trim() != folderName) {
        throwError(folderName, FileHelper::Error::ends_or_begins_with_space);
    }

    if (folderName.removeCharacters(illegal_directory_chars) != folderName) {
        throwError(folderName, FileHelper::Error::illegal_char);
    }

    int slashLocation = folderName.indexOfAnyOf("\\/");

    if (!(slashLocation == -1 || slashLocation == folderName.length() - 1)) {
        throwError(folderName, FileHelper::Error::directory_with_multi_slash);
    }
}

void FileHelper::ensurePathExists(const File& f) {
    ensureValidPath(f.getFullPathName());

    if (!f.exists()) {
        throwError(f.getFullPathName(), FileHelper::Error::path_does_not_exist);
    }
}

void FileHelper::ensureFileExists(const File& f) {
    ensureValidFilePath(f.getFullPathName());

    if (!f.existsAsFile()) {
        throwError(f.getFullPathName(), FileHelper::Error::file_does_not_exist);
    }
}

bool FileHelper::ensureFileNotExist(const File& f) {
    ensureValidFilePath(f.getFullPathName());

    if (f.existsAsFile()) {
        throwError(f.getFullPathName(), FileHelper::Error::file_already_exists);
    }

    return true;
}

void FileHelper::ensureFolderExists(const File& f) {
    ensureValidDir(f.getFullPathName());

    if (f.existsAsFile()) {
        throwError(f.getFullPathName(), FileHelper::Error::directory_already_exists_as_file);
    }

    if (!f.exists()) {
        throwError(f.getFullPathName(), FileHelper::Error::directory_does_not_exist);
    }
}

bool FileHelper::isValidFilePath(StringOrFile file) {
    try {
        ensureValidFilePath(file);
        return true;
    } catch (exception&) {
        return false;
    }
}

bool FileHelper::isValidFileName(StringOrFile file) {
    try {
        ensureValidFileName(file);
        return true;
    } catch (exception&) {
        return false;
    }
}

String FileHelper::getParent(int levels, StringOrFile path) {
    File newDir(FileHelper::toSystemSlash(path));

    if (doesFileExist(path) || hasExt(path)) {
        newDir = newDir.getParentDirectory();
    }

    for (int i = 0; i < levels; ++i) {
        newDir = File(newDir).getParentDirectory();

        string s           = newDir.getFullPathName().toStdString();
        filesystem::path p = newDir.getFullPathName().toStdString();
        if (s == p.root_path().string())
            break;
    }

    return newDir.getFullPathName() + systemSlash;
}

String FileHelper::toUniversalSlash(StringOrFile path) {
    return path.s.replaceCharacter('\\', '/');
}

String FileHelper::toSystemSlash(StringOrFile path) {
    return path.s.replaceCharacters("\\/", systemSlash + systemSlash);
}

String FileHelper::getDateStr(String separator) {
    auto t = juce::Time::getCurrentTime();
    return String(t.getYear()) + separator + String(t.getMonth() + 1).paddedLeft('0', 2) + separator + String(t.getDayOfMonth()).paddedLeft('0', 2);
}

String FileHelper::getTimeStr() {
    auto t = juce::Time::getCurrentTime();
    return String(t.getHours()).paddedLeft('0', 2) + String(t.getMinutes()).paddedLeft('0', 2);
}

String FileHelper::getDateAndTimeStr(String separatorA, String separatorB) {
    return getDateStr(separatorA) + separatorB + getTimeStr();
}
