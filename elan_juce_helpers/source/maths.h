#pragma once

#include <map>
#include <set>
#include <random>
#include <vector>
#include <array>
#include <cmath>

using std::vector;
using juce::String;
//using jura::INF;
//using jura::Parameter;
//using jura::ModulatableParameter2;
//using jura::ModulationConnection;
//using jura::MetaControlledParameter;

//typedef RAPT::rsEllipticSubBandFilter<double, double> EllipticSubBandFilter;
//typedef RAPT::rsOnePoleFilter<double, double> OnePoleFilter;
//typedef rosic::rsOnePoleFilterStereo OnePoleFilterStereo;
//typedef RAPT::rsLadderFilter<double, double> LadderFilter;
//typedef RAPT::rsStateVariableFilter<double, double> StateVariableFilter;
//typedef rosic::BreakpointModulator BreakpointModulator;
//typedef RAPT::rsNormalizedSigmoids<double> NormalizedSigmoids;
//typedef RAPT::rsScaledAndShiftedSigmoid<double> ScaledAndShiftedSigmoid;
//typedef RAPT::rsPositiveBellFunctions<double> PositiveBellFunctions;
//typedef RAPT::rsParametricBellFunction<double> ParametricBellFunction;

namespace elan
{
	void sinCos(double v, double * sinOut, double * cosOut);
	double freqToPitch(double freq, double masterTuneA4 = 440);
	double pitchToFreq(double v);
	template <typename t> t dbToAmp(t v)
	{
		return RAPT::rsDbToAmp<t>(v);
	}
	double pitchOffsetToFreqFactor(double v);
}



/* CONSTANTS */
extern double const TAU;
extern double const TAU_INV;
extern double const PI_INV_x_2;
extern double const PI_z_2;
extern double const PI_z_4;
extern double const PI_z_6;
extern double const PI_x_2;
extern double const PI_x_1p5;
extern double const PI_x_4;
extern double const INV_180;
extern double const TWOTHIRDS;
extern double const ONETHIRD;
extern const double INV_127;
extern const double INV_8192;
extern double const phase45degrees;
extern double const phase90degrees;

/* ROUNDING */
// Rounds value to have a fixed number of decimal places.
double roundTo(double v, int decimals);
// Rounds value to the nearest multiple of precision. For example, if precision is 0.6, value is rounded to the nearest multiple of 0.6.
double roundPrecise(double v, double precision);
template<class t> int roundPow2(t value)
{
	return (int)pow(2, round(log(value) / log(2)));
}

template<class t> int roundUpPow2(t value)
{
	return (int)pow(2, ceil(log(value) / log(2)));
}

template<class t> int roundDownPow2(t value)
{
	return (int)pow(2, floor(log(value) / log(2)));
}
int numDigitsBeforeDecimal(double v);
int numDigitsInInteger(int v);

/* SIMPLIFY */

double tent(double x);

double getDistanceFromLine(double x1, double y1, double x2, double y2, double xp, double yp);

template<class t> vector<std::pair<t, t>> simplifyByAverage(const vector<std::pair<t, t>>& p, t width)
{
	std::vector<std::pair<t, t>> avg;
	double w2 = width; // = 0.5 * width; // using tent function requires double width
	double w2r = 1 / w2;
	double dist;
	int k;
	int N = static_cast<int>(p.size());
	for (int n = 0; n < p.size(); n++)
	{
		double wgt = tent(0);
		double sw = wgt;
		double swv = wgt * p[n].second;
		k = n - 1;
		while (k >= 0 && (dist = p[n].first - p[k].first) <= w2)
		{ // left side loop
			wgt = tent(dist * w2r);
			sw += wgt;
			swv += wgt * p[k].second;
			k--;
		}
		k = n + 1;
		while (k < N && (dist = p[k].first - p[n].first) <= w2)
		{ // right side loop
			wgt = tent(dist * w2r);
			sw += wgt;
			swv += wgt * p[k].second;
			k++;
		}
		avg.push_back({ p[n].first, swv / sw });
	}

	return avg;
}

template<class t> vector<std::pair<t, t>> simplifyByDifference(const vector<std::pair<t, t>>& points, double maxError)
{
	t maxPointDistance = 0.0;
	t maxPointIndex = -1;

	if (points.size() > 1)
	{
		t x1 = points[0].first;
		t y1 = points[0].second;
		t x2 = points.back().first;
		t y2 = points.back().second;

		for (size_t i = 1; i < points.size() - 1; ++i)
		{
			t distance = getDistanceFromLine(x1, y1, x2, y2, points[i].first, points[i].second);

			if (distance > maxPointDistance)
			{
				maxPointIndex = static_cast<int>(i);
				maxPointDistance = distance;
			}
		}
	}

	vector<std::pair<t, t>> result;

	if (maxPointDistance > maxError)
	{
		vector<std::pair<t, t>> lp;
		lp.insert(lp.begin(), points.begin(), points.begin() + maxPointIndex + 1);

		vector<pair<t,t>> line1 = simplifyByDifference<t>(lp, maxError);

		vector<std::pair<t, t>> lq;
		lq.insert(lq.begin(), points.begin() + maxPointIndex, points.end());

		vector<pair<t, t>> line2 = simplifyByDifference<t>(lq, maxError);

		result = line1;
		result.insert(result.end(), line2.begin() + 1, line2.end());
	}
	else
	{
		result.push_back(points.front());
		result.push_back(points.back());
	}

	return result;
}

template <typename t> vector<t> discreteToContinuous(RAPT::rsNodeBasedFunction<t> nodes, int numSamples, int sampleRate)
{
	size_t frames = ceil(nodes.getMaxX() * t(sampleRate));
	vector<t> graph(numSamples, 0);

	for (int i = 0; i < numSamples; ++i)
		graph[i] = nodes.getValue(t(i) / t(sampleRate));

	return std::move(graph);
}

template <typename t> vector<t> discreteToContinuous(vector<t> values, vector<t> positions, int numSamples, int sampleRate)
{
	RAPT::rsNodeBasedFunction<t> nodes;
	for (int i = 0; i < values.size(); ++i)
		nodes.addNode(positions[i], values[i]);

	size_t frames = ceil(nodes.getMaxX() * t(sampleRate));
	vector<t> graph(numSamples, 0);

	for (int i = 0; i < numSamples; ++i)
		graph[i] = nodes.getValue(t(i) / t(sampleRate));

	return std::move(graph);
}

/* CONVERSION */
// -1 to +1 => 0 to 1
double bipolarToUnipolar(double v);
// 0 to 1  => -1 to +1
double unipolarToBipolar(double v);
// -360 to 360 => -1 to 1
double degreesToPhase(double degrees);
// -1 to +1 => -TAU to +TAU, usually used with elan::sinCos function for converting 0 to 1 sin/cos phase to generate a sinewave of -1 to +1 ampliude.
double valueToRotation(double v);
// -inf to +inf => 0 to inf
double dbToAmpAccurate(double v);
// beat value of 1 occurs 4 times in one bar, 2 times (2hz) in 1 second given 120 BPM
double beatsToFrequency(double BPM, double beats);
// bar value of 1 occurs 1/2 times (0.5 hz) in one second given 120 BPM.
double barsToFrequency(double BPM, double bars);
double beatsToTime(double BPM, double beats);
double RatioToAmplitude(double r);
double AmplitudeToRatio(double a);

/* CURVES */
/*
a = -.99 to 99
(x + x * a) / (1 + x * a)

a = -1 to 1
(a + 1) * x  / ((2 * x - 1) * a + 1)
(a * x  + x) /  (2 * a * x - a + 1)
*/

// value is 0 to 1, tension is -1 to +1, negative tension is rise fast, positive is rise slow
double curve(double value, double tension); 
// value is 0 to 1, tension is -1 to +1, negative tension is rise slow-fast-slow, positive is rise fast-slow-fast
double s_curve(double value, double tension);

/* UTILITY */
void phaseIncrement(double * phase, double increment);
void phaseIncrementTAU(double * phase, double increment);
void wrapPhase(double * phase);
double wrapPhase(double phase);
void wrapPhaseTAU(double * phase);
double wrapPhaseTAU(double phase);
extern const vector<double> powers_of_two;
double powerOfTwo(int power);
extern const vector<double> powers_of_ten;
double powerOfTen(int power);
double squared(double v);

// returns valueToModify to be closer to valueToApproach based on a 0 to 1 value of inputValueNormalized.
// This is for when you want to slowly get closer to the desired value where given value may be higher or lower than desired value.
double approachValue(double inputValueNormalized, double valueToModify, double valueToApproach);

// index should not exceed size * 2 - 1. If it does, see if using wrapMultiOctave_Bipolar is faster
// given a size of 3 indexes, the wrap pattern is 0 1 2 for positive values and does not wrap for negative values
int wrapSingleOctave_Positive(int index, int size);

// index should not exceed size * -2 + 1. If it does, see if using wrapMultiOctave_Bipolar is faster
// given a size of 3 indexes, the wrap pattern is 0 1 2 for negative values and does not wrap for positive values
int wrapSingleOctave_Negative(int index, int size);

// index should not exceed size * +/- 2 -/+ 1. If it does, see if using wrapMultiOctave_Bipolar is faster
// given a size of 3 indexes, the wrap pattern is 0 1 2 for positive values and 0 2 1 for negative values
int wrapSingleOctave_Bipolar(int index, int size);

// given a size of 3 indexes, the wrap pattern is 0 1 2 for positive values and 0 2 1 for negative values,
// indexOctave is a bipolar integer indicating how many times the index would have had to wrap
int wrapMultiOctave_Bipolar(int index, int size, int * indexOctave = nullptr);

/* VECTOR */
void rotate2D(double sin, double cos, double * X, double * Y);
void rotate3D(double inX, double inY, double inZ, 
	double rotX_sin, double rotX_cos, 
	double rotY_sin, double rotY_cos, 
	double * outL, double * outR);

bool isErroneousNumber(vector<double> arrayOfNumbersToCheck);
