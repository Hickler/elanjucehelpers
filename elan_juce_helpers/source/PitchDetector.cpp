#include "HelmholtzPitchDetector.cpp"

double PitchDetector::freqToPitch(double freq, double masterTuneA4)
{
	return 12.0 * 1 / log(2) * log(freq / masterTuneA4) + 69.0;
}

double PitchDetector::pitchToFreq(double v)
{
	return 440.0 * (pow(2.0, (v - 69.0) / 12.0));
}

double PitchDetector::detectFrequency(vector<double>* signal, int sampleRate, double fidelity)
{
        HelmholtzPitchDetector PD(signal->data(), static_cast<int>(signal->size()), sampleRate);
	PD.framesize = roundPow2<double>(sampleRate / 10.0);
	PD.overlap = 4;
	PD.fidelity = fidelity;
	return PD.GetAvgFreq();
}

double PitchDetector::frequencyOffsetToSemitones(double targetFreq, double measuredFreq)
{
	return 12.0 * log2(targetFreq / measuredFreq);
}

double PitchDetector::semitoneOffsetToFrequency(double targetFreq, double semitoneOffset)
{
	return targetFreq * pow(2, semitoneOffset / 12.0);
}

double PitchDetector::semitoneOffsetToRate(double bipolarOffset)
{
	return pow(2.0, bipolarOffset / 12.0);
}

void PitchDetector::setup(vector<double>*signal, int sampleRate, double targetPitch, double semitoneRange)
{
	m_signal = signal;
	m_frames = static_cast<int>(signal->size());
	m_sampleRate = sampleRate;
	m_targetPitch = targetPitch;
	m_semitoneRange = semitoneRange;

	m_baseFreq = pitchToFreq(m_targetPitch);
	m_freqRangeLow = pitchToFreq(targetPitch - semitoneRange);
	m_freqRangeHigh = pitchToFreq(targetPitch + semitoneRange);
}

void PitchDetector::analyzeUsingHelmholtz(double fidelity, int periods, int overlap)
{
	double detectedFrequency = detectFrequency(m_signal, m_sampleRate, fidelity);
	if (m_targetPitch < 0)
	{
		m_targetPitch = round(freqToPitch(detectedFrequency));
	}

	HelmholtzPitchDetector PD(m_signal->data(), m_frames, m_sampleRate);
	PD.userfreq = pitchToFreq(m_targetPitch);
	PD.framesize = m_framesize = roundPow2((1.0 / detectedFrequency * periods) / (1.0 / m_sampleRate));
	PD.overlap = overlap;
	PD.fidelity = fidelity;
	PD.pitchRange = .05;
	PD.GetFreqData();

	windowsize = PD.framesize / overlap;
	numEnvPtsToMake = m_frames / PD.framesize;

	if (!PD.FreqData.s.size())
	{
		error = true;
		return;
	}

	// prevent harmonic jump
	for (auto& freq : PD.FreqData.f) 
	{
		if (freq > detectedFrequency)
			freq /= round(freq / detectedFrequency);
		else
			freq *= round(detectedFrequency / freq);
	}	
	
	avgFreq = PD.GetAvgFreq();
}

void PitchDetector::analyzeUsingRsmet()
{
	if (m_targetPitch < 0)
		m_targetPitch = round(freqToPitch(detectFrequency(m_signal, m_sampleRate)));

	vector<double> reliability(m_frames, 0);
	frequencies.resize(m_frames, 0);
	//int CYCLE_CORRELATION = 1;
	RAPT::rsInstantaneousFundamentalEstimator<double>::measureInstantaneousFundamental(m_signal->data(), frequencies.data(), m_frames, m_sampleRate, m_freqRangeLow, m_freqRangeHigh/*, reliability.data(), CYCLE_CORRELATION*/);

	windowsize = 1;
	numEnvPtsToMake = m_frames;

	if (frequencies.empty())
	{
		frequencies.resize(m_frames, m_baseFreq);
		error = true;
	}
}

bool PitchDetector::hasError() { return error; }

double PitchDetector::getLowFreqRange() { return m_freqRangeLow; }
double PitchDetector::getHighFreqRange() { return m_freqRangeHigh; }