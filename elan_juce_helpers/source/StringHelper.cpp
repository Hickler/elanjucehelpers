#include "StringHelper.h"

#include "JuceHeader.h"

using juce::CharacterFunctions;
using juce::juce_wchar;
using juce::String;
using std::vector;

String toS(const juce_wchar& c) {
    return String::charToString(c);
}
String toS(int i) {
    return String(i);
}

bool CHAr::is(const juce_wchar& c1, const juce_wchar& c2) {
    return CharacterFunctions::compare(c1, c2) == 0;
}
bool CHAr::is_i(const juce_wchar& c1, const juce_wchar& c2) {
    return CharacterFunctions::compareIgnoreCase(c1, c2) == 0;
}
bool CHAr::isDigit(const juce_wchar& c) {
    return CharacterFunctions::isDigit(c);
}

bool CHAr::isAlphabet(const juce_wchar& c) {
    return CHAr::isAny_i(c, "abcdefghijklmnopqrstuvwxyz");
}
bool CHAr::isWhiteSpace(const juce_wchar& c) {
    return CharacterFunctions::isWhitespace(c);
}
bool CHAr::isNewLine(const juce_wchar& c) {
    return CHAr::isAny(c, "\r\n");
}
bool CHAr::isAny(const juce_wchar& c, const String& list) {
    return list.containsChar(c);
}
bool CHAr::isNotAny(const juce_wchar& c, const String& list) {
    return !isAny(c, list);
}
bool CHAr::isAny_i(const juce_wchar& c, const String& list) {
    for (auto p = list.getCharPointer(); !p.isEmpty(); ++p)
        if (CHAr::is_i(c, *p))
            return true;
    return false;
}
bool CHAr::isNotAny_i(const juce_wchar& c, const String& list) {
    return !isAny_i(c, list);
}
bool CHAr::isUpper(const juce_wchar& c) {
    return CharacterFunctions::isUpperCase(c);
}
bool CHAr::isLower(const juce_wchar& c) {
    return CharacterFunctions::isLowerCase(c);
}
juce_wchar CHAr::toUpper(const juce_wchar& c) {
    return CharacterFunctions::toUpperCase(c);
}
juce_wchar CHAr::toLower(const juce_wchar& c) {
    return CharacterFunctions::toLowerCase(c);
}

int CHAr::getValue(const juce_wchar& c) {
    if (!isDigit(c))
        return 0;

    return int(c) - '0';
}

bool STR::isInt(const String& s) {
    if (s.isEmpty())
        return false;

    auto p = s.getCharPointer();

    if (*p == '-' || *p == '+')
        ++p;

    while (!p.isEmpty() && CHAr::isDigit(*p))
        ++p;

    return p.isEmpty();
}

int STR::isIntOrFloat(const String& s, int* dotPosition, int* signPosition) {
    if (dotPosition)
        *dotPosition = -1;
    if (signPosition)
        *signPosition = -1;

    if (s.isEmpty())
        return 0;

    bool dotFound = false;

    auto p = s.getCharPointer();

    int i = 0;

    if (*p == '-' || *p == '+') {
        ++p;
        ++i;

        if (signPosition)
            *signPosition = 0;
    }

    while (!p.isEmpty()) {
        if (*p == '.') {
            if (!dotFound) {
                dotFound = true;
                if (dotPosition)
                    *dotPosition = i;
            } else // more than two dots is not a number, end early
            {
                break;
            }

            ++p;
            continue;
        }

        if (!CHAr::isDigit(*p))
            break;

        ++p;
        ++i;
    }

    if (p.isEmpty())
        if (dotFound)
            if (s.length() > 1)
                return 2;
            else
                return 0; // this means string was only a dot.
        else
            return 1;

    return 0;
}

bool STR::isFloat(const String& s) {
    return isIntOrFloat(s) == 2;
}

String STR::limitDecimals(const String& s, int decimalPlaces) {
    decimalPlaces = std::max(0, decimalPlaces);

    int dotIndex, signIndex;

    // string is not decimal
    if (isIntOrFloat(s, &dotIndex, &signIndex) != 2)
        return s;

    int roundDigitIdx = std::min(dotIndex + decimalPlaces, s.length() - 1);            // get digit for rounding
    int mod           = roundDigitIdx + 1 < s.length() && s[roundDigitIdx + 1] >= '5'; // round up or down

    if (mod) // when rounding up, check for adjacent 9s
        while (roundDigitIdx != signIndex && (s[roundDigitIdx] == '9' || roundDigitIdx == dotIndex))
            --roundDigitIdx;
    else if (roundDigitIdx == dotIndex) // ensure rounded digit does not refer to dot char
        --roundDigitIdx;

    // get value string
    String result = s.substring(signIndex + 1, roundDigitIdx) + String((roundDigitIdx == signIndex ? 0 : s[roundDigitIdx] - '0') + mod) + STR::repeat("0", dotIndex - roundDigitIdx - 1);

    if (result == "0")
        signIndex = -1;

    // add leading zero or trailing dot zero if desired
    if (decimalPlaces > 0)
        result = (result[0] == '.' ? "0" : "") + result + (dotIndex > roundDigitIdx ? ".0" : "");

    // return string with sign
    return (signIndex == 0 ? String::charToString(s[0]) : "") + result;
}

String STR::limitDecimals(double v, int decimalPlaces) {
    return limitDecimals(String(v, decimalPlaces + 1), decimalPlaces);
}

vector<String> STR::splitToVector(const String& s, const String& delim) {
    StringIterator iter(s);
    vector<String> tokens;

    while (!iter.atEnd()) {
        tokens.push_back(iter.consumeToChar(delim));
        iter.move();
    }

    return tokens;
}

Array<var> STR::splitToVarArray(const String& s, const String& delim) {
    StringIterator iter(s);
    Array<var> tokens;

    while (!iter.atEnd()) {
        tokens.add(iter.consumeToChar(delim));
        iter.move();
    }

    return std::move(tokens);
}

vector<String> STR::splitPast(const String& s, const String& delim) {
    StringIterator iter(s);
    vector<String> tokens;

    while (!iter.atEnd())
        tokens.push_back(iter.consumePastChar(delim));

    return tokens;
}

// split by any delimiter
vector<String> STR::split(const String& s, const String& delim, size_t min_size, const String& defval) {
    vector<String> elems = splitToVector(s, delim);
    elems.resize(std::max<size_t>(elems.size(), min_size), defval);
    return elems;
}

String STR::repeat(const String& s, int numRepeats) {
    vector<String> sr;

    for (int i = 0; i < numRepeats; ++i)
        sr.push_back(s);

    return Convert::toString(sr);
}

String STR::getRandomAlphaNumericString(int numChars) {
    vector<juce_wchar> v = alphanumeric;

    String ret;
    ret.preallocateBytes(v.size() - sizeof(String::CharPointerType::CharType));

    auto dest = ret.getCharPointer();

    size_t sz = 0;
    while (sz < numChars) {
        std::shuffle(v.begin(), v.end(), VectorHelper::getRandomEngine());
        std::for_each(v.begin(), v.end(), [&](juce_wchar c) { ret += c; });
        sz += v.size();
    }

    dest.writeNull();

    return std::move(ret);
}

String StringIterator::convertToWhiteSpace(const String& s) {
    StringIterator si(s);

    enum {
        newline2,
        newline1,
        tab
    };
    vector<String> sequences({ "\r\n", "\n", "\t" });
    vector<String> found;

    while (!si.atEnd()) {
        int sequenceFound;
        found.push_back(replaceAll(si.consumeToSequence(sequences, &sequenceFound), ' '));
        switch (sequenceFound) {
        case newline2:
            found.push_back("\r\n");
            si.move(2);
            break;
        case newline1:
            found.push_back("\n");
            si.move();
            break;
        case tab:
            found.push_back("\t");
            si.move();
            break;
        }
    }

    return Convert::toString(found);
}

String StringIterator::replaceAll(const String& s, juce_wchar c) {
    return String::repeatedString(String::charToString(c), s.length());
}

void StringIterator::remember() {
    if (!p1.isEmpty())
        memory.push_back({ p1, p2 });
}

void StringIterator::rememberAndSkipOver() {
    remember();
    move();
    start();
}

String StringIterator::getMemory() {
    vector<String> str;
    vector<String> strVec;

    for (const auto& mem : memory) {
        str.push_back({ mem.a, mem.b });
        strVec.push_back({ mem.a, mem.b });
    }

    memory.clear();

    return Convert::toString(str);
}

void StringIterator::restart() {
    pos = 0;
    len = in_str.length();
    p1  = in_str.getCharPointer();
    p2  = p1;
}

void StringIterator::skipNewLines() {
    while (!atEnd() && CHAr::isNewLine(*this)) {
        move();
    }
}

void StringIterator::skipToNewLine() {
    while (!atEnd() && !CHAr::isNewLine(*this)) {
        move();
    }
}

void StringIterator::skipToChar(const juce_wchar& c) {
    while (!atEnd() && *this != c) {
        move();
    }
}

void StringIterator::skipToChar(const String& s) {
    while (!atEnd() && !CHAr::isAny(*this, s)) {
        move();
    }
}

void StringIterator::skipPastChar(const juce_wchar& c) {
    skipToChar(c);
    while (!atEnd() && *this == c) {
        move();
    }
}

void StringIterator::skipToSpace() {
    while (!atEnd() && !CHAr::isWhiteSpace(*this)) {
        move();
    }
}

void StringIterator::skipToSpaceOrChar(const juce_wchar& c) {
    while (!atEnd() && *this != c && !CHAr::isWhiteSpace(*this)) {
        move();
    }
}

void StringIterator::skipSpaces() {
    while (!atEnd() && CHAr::isWhiteSpace(*this)) {
        move();
    }
}

void StringIterator::skipChar(const juce_wchar& c) {
    while (!atEnd() && *p2 == c) {
        move();
    }
}

void StringIterator::skipChar(const String& s) {
    while (!atEnd() && CHAr::isAny(*this, s)) {
        move();
    }
}

String StringIterator::consumeToNewLine() {
    start();
    skipToNewLine();
    return get();
}

// increments past the next full new line (\r\n or just \n)

String StringIterator::consumePastNewLine() {
    start();

    skipPastSingleNewLine();

    return get();
}

String StringIterator::consumePastWhiteSpace() {
    start();
    skipToSpace();
    return get();
}

String StringIterator::consumeToNonSpace() {
    start();
    skipSpaces();
    return get();
}

String StringIterator::consumeInt() {
    start();
    enum MODE {
        begin,
        end
    };
    MODE mode = begin;
    while (!this->atEnd()) {
        switch (mode) {
        case begin:
            if (isNotAny("-+") || !CHAr::isDigit(*this)) {
                return get();
            }
            move();
            mode = end;
            continue;
        case end:
            if (!CHAr::isDigit(*this)) {
                return get();
            }
            move();
            continue;
        }
    }
    return get();
}

String StringIterator::consumeToSequenceAndSkip(const vector<String>& sequences, int* sequenceFound) {
    while (!atEnd()) {
        for (int i = 0; i < sequences.size(); ++i) {
            if (lookAheadForSequence(sequences[i])) {
                remember();
                move(sequences[i].length());
                if (sequenceFound != nullptr) {
                    *sequenceFound = i;
                }
                start();
                return getMemory();
            }
        }
        move();
    }
    return {};
}

String StringIterator::consumeToSequence(const vector<String>& sequences, int* sequenceFound) {
    start();

    while (!atEnd()) {
        for (int i = 0; i < sequences.size(); ++i) {
            if (lookAheadForSequence(sequences[i])) {
                if (sequenceFound != nullptr) {
                    *sequenceFound = i;
                }

                return get();
            }
        }
        move();
    }

    if (sequenceFound != nullptr) {
        *sequenceFound = -1;
    }
    return get();
}

String StringIterator::consumePastSequence(const vector<String>& sequences, int* sequenceFound) {
    while (!atEnd()) {
        for (int i = 0; i < sequences.size(); ++i) {
            if (lookAheadForSequence(sequences[i])) {
                if (sequenceFound != nullptr) {
                    *sequenceFound = i;
                }

                move(sequences[i].length());

                return get();
            }
        }
        move();
    }

    return get();
}

String StringIterator::consumePastSequence(const String& sequence) {
    return consumePastSequence(vector<String>{ sequence });
}

bool StringIterator::lookAheadForSequence(const String& sequence) {
    int l        = sequence.length();
    int posInSeq = 0;

    auto p = p2;

    while (posInSeq < l && p.isNotEmpty() && *p == sequence[posInSeq]) {
        ++posInSeq;
        ++p;
    }

    return posInSeq == l;
}

String StringIterator::prepareForNaturalSort() {
    enum MODE {
        skipSpaces,
        zeroFound,
        consume
    };
    MODE mode = skipSpaces;

    while (!atEnd()) {
        switch (mode) {
        case skipSpaces:
            if (isAny(" _")) {
                move();
            } else if (is('0')) {
                move();
                mode = zeroFound;
            } else {
                start();
                move();
                mode = consume;
            }
            continue;
        case zeroFound:
            if (is('0')) {
                move();
            } else if (isDigit()) {
                start();
                move();
                mode = consume;
            } else {
                start();
                dec();
                if (isAny("_ ")) {
                    remember();
                    move();
                    start();
                    mode = skipSpaces;
                } else {
                    move();
                    mode = consume;
                }
            }
            continue;
        case consume:
            if (isNotAny(" _")) {
                move();
            } else {
                remember();
                move();
                start();
                mode = skipSpaces;
            }
            continue;
        }
    }
    remember();
    return getMemory();
}

String StringIterator::consumeToChar(const juce_wchar& c) {
    start();
    while (!atEnd() && *this != c) {
        move();
    }
    return get();
}

String StringIterator::consumeToChar(const String& s) {
    start();
    while (!atEnd() && isNotAny(s)) {
        move();
    }
    return get();
}

String StringIterator::consumePastChar(const String& s) {
    start();
    while (!atEnd() && isNotAny(s)) {
        move();
    }
    while (!atEnd() && isAny(s)) {
        move();
    }
    return get();
}

String StringIterator::consumeToSpaceOrChar(const juce_wchar& c) {
    start();
    skipToSpaceOrChar(c);
    return get();
}

String StringIterator::consumeToEnd() {
    start();
    while (!atEnd()) {
        move();
    }
    return get();
}

String StringIterator::consumeToNewLineTRIM() {
    skipSpaces();
    auto p = p1 = p2;
    while (!atEnd() && !CHAr::isNewLine(*this)) {
        skipToSpace();
        p = p2;
        skipChar(' ');
        if (CHAr::isNewLine(*this))
            break;
    }
    return String(p1, p);
}

String StringIterator::consumeToCharTRIM(const juce_wchar& c) {
    skipSpaces();
    auto p = p1 = p2;
    while (!atEnd() && *this != c) {
        skipToSpaceOrChar(c);
        p = p2;
        skipSpaces();
        if (*this == c)
            break;
    }
    return String(p1, p);
}

String StringIterator::consumeToNewlineOrCharTRIM(const juce_wchar& c) {
    skipSpaces();
    auto p = p1 = p2;
    while (!atEnd() && *this != c) {
        skipToSpaceOrChar(c);
        p = p2;
        skipChar(' ');
        if (*this == c || CHAr::isNewLine(*this))
            break;
    }
    return String(p1, p);
}

String StringIterator::consumeToChar_IgnoreEscapes(const String& s, const juce_wchar& escape) {
    start();

    while (!atEnd() && !CHAr::isAny(*this, s)) {
        if (*this == escape) {
            rememberAndSkipOver();
            move();
            continue;
        }
        move();
    }

    remember();

    return getMemory();
}
