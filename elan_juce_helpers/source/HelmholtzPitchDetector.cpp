#include "3rdParty/Helmholtz/Helmholtz.cpp"

#include "HelmholtzPitchDetector.h"

HelmholtzPitchDetector::HelmholtzPitchDetector(DATA* RawWav, int nsamples, int samplerate) :
	nsamples(nsamples),
	samplerate(samplerate)
{
	// normalize incoming audio
	double peakValue = 0.0;

	for (int n = 0; n < nsamples; ++n)
	{
		peakValue = std::max(peakValue, std::abs(RawWav[n]));
		if (peakValue >= 1.0)
			break;
	}

	normalizedAudio.reserve(nsamples);
	double normalizeMultiplier = 1.0 / peakValue;

	for (int n = 0; n < nsamples; ++n)
		normalizedAudio.push_back(normalizeMultiplier * RawWav[n]);
}

vector<double> HelmholtzPitchDetector::getFrequencies()
{
	if (frequencies.size())
		return frequencies;

	if (!FreqData.s.size())
		GetFreqData(); // populates FreqData.

	if (!FreqData.s.size())
		return frequencies; //error has occured, no frequency data

	size_t sz = FreqData.s.size();

	frequencies.resize(nsamples, 0);

	int s_start, s_end, s_total;
	double f1, f2;

	for (size_t x = 0; x < sz - 1; ++x) // linear interpolate data points
	{
		s_start = FreqData.s[x];
		s_end = FreqData.s[x + 1];
		s_total = s_end - s_start;
		f1 = FreqData.f[x];
		f2 = FreqData.f[x + 1];

		for (int s = 0; s < s_total; ++s)
			frequencies[s + s_start] = f1 + (double)s / s_total * (f2 - f1);
	}

	for (int s = 0; s < FreqData.s[0]; ++s) // pre-padding frequency
		frequencies[s] = FreqData.f[0];

	for (int s = FreqData.s[sz - 1]; s < nsamples; ++s) // post-padding frequency
		frequencies[s] = FreqData.f[sz - 1];

	return frequencies;
}

HelmholtzPitchDetector::samp_freq HelmholtzPitchDetector::GetFreqData()
{
	if (!framesize)
		solve_framesize();

	if (!FreqData.s.size())
		process();

	return FreqData;
}

FREQ HelmholtzPitchDetector::GetAvgFreq()
{
	GetFreqData();

	if (basefreq)
	{
		return basefreq;
	}
	else {
		basefreq = appear_most();
	}

	//double lowest = std::max<double>(8.0,      basefreq * pow(2.0, -pitchRange / 12.0));
	//double highest = std::min<double>(15000.0, basefreq * pow(2.0, +pitchRange / 12.0));
	double lowest = std::max<double>(8.0, basefreq - 1.0);
	double highest = std::min<double>(15000.0, basefreq + 1.0);
	double sum = 0;
	int    times = 0;

	if (highest == 0.0)
	{
		lowest = 8.0;
		highest = 15000;
	}

	for (int i = 0; i < FreqData.s.size(); i++)
	{
		if (FreqData.f[i] > highest || FreqData.f[i] < lowest)
			continue;

		++times;
		sum += FreqData.f[i];
	}

	if (!times)
		return 0;

	return basefreq = sum / double(times);
}

void HelmholtzPitchDetector::process()
{
	int rsv = overlap * (int)ceil((double)nsamples / framesize);
	FreqData.s.reserve(rsv);
	FreqData.f.reserve(rsv);
	hel = new Helmholtz(framesize);
	hhout = new double[framesize];

	int inc = framesize / overlap;

	for (int s = 0; s < nsamples - framesize; s += inc)
	{
		int samplesInBlock = framesize;

		if (s + framesize > nsamples)
			samplesInBlock = nsamples - s;

		if (samplesInBlock)
			processBlock(&normalizedAudio.data()[s], samplesInBlock, s);
	}
}

void HelmholtzPitchDetector::processBlock(DATA* dataBlock, int blocksize, int samp_index_for_freq)
{
	DATA* data = dataBlock;
	if (blocksize < framesize)
	{//zero padding, should be done in helmholtz
		data = new DATA[framesize];
		memset(data, 0, sizeof(DATA) * framesize);
		memcpy(data, dataBlock, sizeof(DATA) * blocksize);
	}

	hel->iosamples(data, hhout, blocksize);

	if (hel->getfidelity() <= fidelity) // if measured frequency is not reliable
		return;

	FreqData.s.push_back(samp_index_for_freq);
	FreqData.f.push_back(samplerate / hel->getperiod());

	if (blocksize < framesize)
		delete[] data;
}

//todo: send rounded double instead of int
int HelmholtzPitchDetector::appear_most()
{
	if (!FreqData.s.size())
		return 0;

	int hold = 0;
	int max = 0;

	vector<int> tmp; 
	tmp.reserve(FreqData.s.size());

	// Create temp vector that contains frequency rounded
	for (size_t i = 0; i < FreqData.s.size(); ++i)
		tmp.push_back(round(FreqData.f[i]));

	std::sort(tmp.begin(), tmp.end(), [](int a, int b) { return a > b; });

	// Get Appear_most frequency.
	size_t halfsize = tmp.size() / 2;
	for (size_t i = 0; i < tmp.size() - 1;)
	{
		int count = 0;

 		while (i < tmp.size() - 1 && tmp[i] == tmp[i + 1])
		{
			++i;
			if (count++ > halfsize)
				return tmp[i];
		}

		if (max < count)
		{
			max = count;
			hold = tmp[std::max<size_t>(i-1, 0)];
		}

 		++i;
	}

	return hold;
}

double HelmholtzPitchDetector::LinearInterpolate(double a, double b, size_t size, size_t index)
{
	return ((a - b) / (double)size) * (double)index;
}

void HelmholtzPitchDetector::solve_framesize()
{
	double b = userfreq ? userfreq : basefreq;
	if (!b || !framesize)
		framesize = samplerate / 10;
	else
		framesize = ((1.0 / b) * 8.0) / (1.0 / samplerate);
}

HelmholtzPitchDetector::~HelmholtzPitchDetector()
{
	if (hhout)
		delete[] hhout;

	if (hel)
		delete hel;
}
