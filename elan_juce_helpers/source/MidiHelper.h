#pragma once

#include "JuceHeader.h"

#include <vector>
#include <map>

struct char_int {
    juce::juce_wchar c;
    int i;
};
struct str_int {
    juce::String s;
    int i;
};

class MIDI {
  public:
    static double toFrequency(double pitch) {
        return pitchToFreq(pitch);
    }

    static double toPitch(double frequency) {
        return freqToPitch(frequency);
    }

    static int frequencyToIntegerPitch(double frequency) {
        return static_cast<int>(round(freqToPitch(frequency)));
    }

    static double quantizeFrequency(double frequency) {
        return toFrequency(round(freqToPitch(frequency)));
    }

    MIDI();
    MIDI(juce::String s, int transpose = 0);
    MIDI(int i, int transpose = 0);
    MIDI(juce::var v, int transpose = 0);

    void read(int i, int transpose = 0);
    void read(juce::String s, int transpose = 0);

    void setMidOct(int i);
    juce::String applyOffset(int i);
    void format(const juce::String& s);

    // C#3 returns 61
    int getNum();
    // C#3 returns C#3
    juce::String getName();
    // C#3 returns C
    juce::String getClass();
    // C#3 returns 1
    int getClassValue();
    // C#3 returns 3
    juce::String getOctaveString();
    // C#3 returns 6
    int getOctave();

    // D3 returns 1
    int getNaturalIndex() {
        jassert(class_to_natural_index[getClassValue()] != -1);
        return class_to_natural_index[getClassValue()];
    }
    // D#3 returns 1
    int getSharpIndex() {
        jassert(class_to_sharp_index[getClassValue()] != -1);
        return class_to_sharp_index[getClassValue()];
    }

    bool isNatural();
    bool isSharp();
    double toFrequency();

  private:
    int lowest_octave = -2;

    int midi_number = -1;
    juce::String midi_name;

    // value representations
    int root{};
    int accidental{};
    int multiplier{};
    int sign{};
    int octave{};

    int modulo{};

    bool use_flat{ false };
    bool use_capital_root{ true };
    bool use_capital_accidental{ false };
    bool always_show_sign{ false };

    static const std::vector<int> sharpKeyNumbers;
    static const std::vector<int> naturalKeyNumbers;
    static const std::vector<int> class_to_natural_index;
    static const std::vector<int> class_to_sharp_index;

    static std::map<juce::juce_wchar, int> root_to_value;
    static std::map<int, juce::juce_wchar> value_to_root;
    static std::map<juce::juce_wchar, int> accidental_to_value;
    static std::map<int, juce::juce_wchar> value_to_accidental;

    static double pitchToFreq(double pitch, double masterTuneA4 = 440.0) {
        return masterTuneA4 * (pow(2.0, (pitch - 69.0) / 12.0));
    }

    static double freqToPitch(double freq, double masterTuneA4 = 440.0) {
        return 12.0 * LN2_INV * log(freq / masterTuneA4) + 69.0;
    }
};

class Sequence {
  public:
    struct Pitch {
        Pitch(double pitch, double startTime, double endTime, int velocity = 127)
          : pitch(pitch)
          , startTime(startTime)
          , endTime(endTime)
          , velocity(velocity) {}

        double getLength() {
            return endTime - startTime;
        }

        double pitch, startTime, endTime;
        int velocity;
    };

    static std::vector<double> convertFrequencyDataToPitch(std::vector<double> frequencies) {
        std::vector<double> ret;
        ret.reserve(frequencies.size());

        for (auto& f : frequencies)
            ret.push_back(MIDI::toPitch(f));

        return ret;
    }

    Sequence(std::vector<Pitch> vectorOfPitches, double precision = 1) {
        for (auto& p : vectorOfPitches)
            p.pitch = roundPrecise(p.pitch, precision);

        m_sortByStartTime(vectorOfPitches);
        m_glueOverlappingNotes(vectorOfPitches);

        pitches = std::move(vectorOfPitches);
    }

    // Pass in a std::vector of linear data such as semitones, timePerIndex is usually 1.0 / sampleRate.
    Sequence(std::vector<double> pitchData, double timePerIndex, double precision = 1)
      : precision(precision) {
        double mn, mx;
        mn = mx = pitchData[0];
        for (auto& p : pitchData) {
            mn = std::min(mn, p);
            mx = std::max(mx, p);
        }

        if (pitchData.size() < 2) {
            pitches.push_back({ pitchData[0], 0, timePerIndex });
            return;
        }

        for (auto& p : pitchData)
            // p = roundPrecise(p, precision);
            p = roundPrecise(juce::jmap<double>(p, mn, mx, 0.0, 127.0), precision);

        for (int i = 0; i < pitchData.size(); ++i) {
            double p1 = pitchData[i];
            double p2 = pitchData[i + 1];

            double st = i;
            double et = i + 1;

            while (i < pitchData.size() && p1 == p2) {
                p2 = pitchData[i];
                et = i++;
            }

            --i;

            pitches.push_back({ p1, st * timePerIndex, et * timePerIndex });
        }

        sortByStartTime();
        m_glueOverlappingNotes(pitches);
    }
    ~Sequence() = default;

    void removeNotesBasedOnLength(double minTimeToKeep);

    // returns the first midpoint time between the end of one note and the start of a new note
    double getFirstTransitionTime();

    void connectNoteGaps();

    std::vector<Pitch> pitches;

  protected:
    void sortByStartTime();

    void m_quantizePitches(std::vector<Pitch>& v, double precision = 1) {
        for (auto& n : v) {
            n.pitch = roundPrecise(n.pitch, precision);
        }
    }

    void m_connectNoteGaps(std::vector<Pitch>& v) {
        if (pitches.size() < 2) {
            return;
        }

        sortByStartTime();

        double firstPitch;
        double secondPitch = pitches[0].pitch;
        std::vector<Pitch> newNotes;
        for (int i1 = 0, i2 = 1; i1 + 1 < pitches.size(); i1 = i2++) {
            firstPitch = v[i1].pitch;

            while (i2 < v.size() && firstPitch == secondPitch) {
                secondPitch = v[i2++].pitch;
            }

            --i2;

            newNotes.push_back({ v[i1].pitch, v[i1].startTime, v[i2].endTime });
        }

        v = std::move(newNotes);
    }

    void m_glueOverlappingNotes(std::vector<Pitch>& v) {
        if (v.size() < 2)
            return;

        auto pitchMap = m_splitPitchesIntoPitchMap(v);

        std::vector<Pitch> newNotes;

        for (auto it = pitchMap.begin(); it != pitchMap.end(); ++it) {
            m_sortByStartTime(it->second);

            double firstStart, firstEnd, secondStart/*, velocity*/;

            int i1, i2;
            for (i1 = 0, i2 = 1; i1 + 1 < it->second.size(); i1 = i2++) {
                firstStart  = v[i1].startTime;
                firstEnd    = v[i1].endTime;
                secondStart = v[i2].startTime;

                while (i2++ < v.size() && firstEnd > secondStart) {
                    firstEnd    = std::max(v[i1].endTime, v[i2 - 1].endTime);
                    secondStart = v[i2 - 1].startTime;
                }

                --i2;

                newNotes.push_back({ v[i1].pitch, v[i1].startTime, firstEnd, v[i1].velocity });
            }

            if (it->second.size() < 2) // if at beginning
                newNotes.push_back({ it->second[0].pitch, it->second[0].startTime, it->second[0].endTime, it->second[0].velocity });

            else if (i1 == v.size() - 1) // if at end
                newNotes.push_back({ v.back().pitch, v.back().startTime, v.back().endTime, v.back().velocity });
        }

        v = std::move(newNotes);
        m_sortByStartTime(v);
    }

    void m_sortByStartTime(std::vector<Pitch>& v) {
        std::sort(std::begin(v), std::end(v), [](Pitch& a, Pitch& b) { return a.startTime < b.startTime; });
    }

    std::map<double, std::vector<Pitch>> m_splitPitchesIntoPitchMap(std::vector<Pitch> v) {
        m_quantizePitches(v);

        std::map<double, std::vector<Pitch>> ret;

        for (auto& n : v) {
            n.pitch = roundPrecise(n.pitch, precision);
            ret[n.pitch].push_back(n);
        }

        return ret;
    }

    std::vector<Pitch> m_pitches;
    double precision = 1;
};
