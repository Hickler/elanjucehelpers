#pragma once

#include "HelmholtzPitchDetector.h"

class PitchDetector
{
public:
	enum Algorithm
	{
		HELMHOLTZ,
		RSMET
	};

	static double freqToPitch(double freq, double masterTuneA4 = 440.0);
	static double pitchToFreq(double v);
	static double detectFrequency(std::vector<double>* signal, int sampleRate, double fidelity = 0.8);
	static double frequencyOffsetToSemitones(double targetFreq, double measuredFreq);
	static double semitoneOffsetToFrequency(double targetFreq, double semitoneOffset);
	static double semitoneOffsetToRate(double bipolarOffset);

	/* Call setup and then one of the analyze algorithms */
	PitchDetector() = default;
	~PitchDetector() = default;

	void setup(std::vector<double>* signal, int sampleRate, double targetPitch = -1.0, double semitoneRange = 2.0);
	void analyzeUsingHelmholtz(double fidelity = 0.95, int periods = 4, int overlap = 4);
	void analyzeUsingRsmet();

	bool hasError();

	double avgFreq = 0.0;
	double getLowFreqRange();
	double getHighFreqRange();

	int getFrameSize()
	{
		jassert(m_framesize != 0); // pitch detector must be run first
		return m_framesize;
	}

	std::vector<double> frequencies;

protected:

	Algorithm algorithm = Algorithm::HELMHOLTZ;
	
	bool error = false;

	std::vector<double>* m_signal = nullptr;
        int m_frames{};
	
	double m_targetPitch{ -1.0 };
        double m_semitoneRange{ 0.0 };
        int m_sampleRate{ 44100 };
        double m_baseFreq{};
        double m_freqRangeLow{};
        double m_freqRangeHigh{};

	struct samp_freq { std::vector<int> s; std::vector<double> f; };
	std::vector<std::pair<double, double>> pts;
	int windowsize      = 0;
	int numEnvPtsToMake = 0;
	int m_framesize = 0;
};
