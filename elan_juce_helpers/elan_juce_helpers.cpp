#ifdef ELAN_JUCE_HELPERS_H_INCLUDED
 /* When you add this cpp file to your project, you mustn't include it in a file where you've
    already included any other headers - just put it inside a file on its own, possibly with your config
    flags preceding it, but don't include anything else. That also includes avoiding any automatic prefix
    header files that the compiler may be using.
 */
 #error "Incorrect use of JUCE cpp file"
#endif

#include "elan_juce_helpers.h"

#include "source/maths.cpp"

#include "source/ValueTreeObjectList.cpp"
#include "source/DecimalCompare.cpp"
#include "source/TypeConversion.cpp"
#include "source/StringHelper.cpp"
#include "source/RegexHelper.cpp"
#include "source/FileHelper.cpp"
#include "source/PluginFileHelper.cpp"
#include "source/AudioFile.cpp"
#include "source/MidiHelper.cpp"
#include "source/PitchDetector.cpp"
#include "source/TagParser.cpp"
#include "source/Translator.cpp"
#include "source/BatchFileHelper.cpp"
#include "source/DrawableSymbols.cpp"

