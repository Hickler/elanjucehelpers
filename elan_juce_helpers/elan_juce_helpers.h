/*******************************************************************************
 The block below describes the properties of this module, and is read by
 the Projucer to automatically generate project code that uses it.
 For details about the syntax and how to create or use a module, see the
 JUCE Module Format.txt file.


 BEGIN_JUCE_MODULE_DECLARATION

  ID:               elan_juce_helpers
  vendor:           Soundemote
  version:          0.0.1
  name:             elan_juce_helpers
  description:      JUCE helpers provided free by Soundemote programmed by Elan Hickler
  website:          http://www.soundemote.com

  dependencies:     juce_core

 END_JUCE_MODULE_DECLARATION

*******************************************************************************/


#ifndef ELAN_JUCE_HELPERS_H_INCLUDED
#define ELAN_JUCE_HELPERS_H_INCLUDED

#include <juce_core/juce_core.h>
#include <juce_graphics/juce_graphics.h>
#include <juce_data_structures/juce_data_structures.h>
#include <rapt/rapt.h>

#include <algorithm>
#include <vector>
#include <map>
#include <regex>
#include <random>
#include <chrono>
#include <numeric>
#include <filesystem>

#include "source/maths.h"

#include "source/ValueTreeObjectList.h"
#include "source/DecimalCompare.h"
#include "source/StringHelper.h"
#include "source/TypeConversion.h"
#include "source/RegexHelper.h"
#include "source/FileHelper.h"
#include "source/PluginFileHelper.h"
#include "source/AudioFile.h"
#include "source/MidiHelper.h"
#include "source/PitchDetector.h"
#include "source/TagParser.h"
#include "source/Translator.h"
#include "source/BatchFileHelper.h"
#include "source/DrawableSymbols.h"

#endif
